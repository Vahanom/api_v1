<?php

namespace App\Http\Controllers\partner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
class APIController extends Controller
{

    /**g
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    public function partnerregisteration(Request $request){
        $validator = Validator::make($request->all(), [
            'phoneno' => 'required|max:10|min:10',
            'name' => 'required',
            'email' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 200);
            }else{
            if ($request->hasFile('image')) {
                $original_filename = $request->file('image')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                if($request->user_type=='Supplier'){
                    $destination_path = '../../admin/assets/images/supplier';
                }
                else{
                        $destination_path = '../../admin/assets/images/technician';
                }
                $image = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image')->move($destination_path, $image)) {
                    $image =  $image;
              
                } else {
                    $image=null;
                }
            } else {
                $image=null;
            }
            
            //   if ($request->hasFile('idproof')) {
            //     $original_filename = $request->file('idproof')->getClientOriginalName();
            //     $original_filename_arr = explode('.', $original_filename);
            //     $file_ext = end($original_filename_arr);
            //     if($request->user_type=='Supplier'){
            //         $destination_path = '../../admin/assets/images/supplier';
            //     }
            //     else{
            //             $destination_path = '../../admin/assets/images/technician';
            //     }
            //     $idproof = 'U-' . rand() . '.' . $file_ext;
            //     if ($request->file('idproof')->move($destination_path, $idproof)) {
            //         $idproof =  $idproof;
              
            //     } else {
            //         $idproof=null;
            //     }
            // } else {
            //     $idproof=null;
            // }
        
            if ($request->hasFile('uploadcv')) {
                $original_filename = $request->file('uploadcv')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $uploadcv = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('uploadcv')->move($destination_path, $uploadcv)) {
                    $uploadcv =  $uploadcv;
              
                } else {
                    $uploadcv=null;
                }
            } else {
                $uploadcv=null;
            }
            
            if ($request->hasFile('bank_image')) {
                $original_filename = $request->file('bank_image')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $bank_image	= 'U-' . rand() . '.' . $file_ext;
                if ($request->file('bank_image')->move($destination_path, $bank_image)) {
                    $bank_image = $bank_image;
              
                } else {
                    $bank_image	= null;
                }
            } else {
                $bank_image	= null;
            }
            
            if ($request->hasFile('adhar_front_img')) {
                $original_filename = $request->file('adhar_front_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $adhar_front_img = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('adhar_front_img')->move($destination_path, $adhar_front_img)) {
                    $adhar_front_img = $adhar_front_img;
              
                } else {
                    $adhar_front_img	= null;
                }
            } else {
                $adhar_front_img	= null;
            }
            
             if ($request->hasFile('adhar_back_img')) {
                $original_filename = $request->file('adhar_back_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $adhar_back_img	= 'U-' . rand() . '.' . $file_ext;
                if ($request->file('adhar_back_img')->move($destination_path, $adhar_back_img)) {
                    $adhar_back_img = $adhar_back_img;
              
                } else {
                    $adhar_back_img	= null;
                }
            } else {
                $adhar_back_img	= null;
            }
            
             if ($request->hasFile('driving_front_img')) {
                $original_filename = $request->file('driving_front_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $driving_front_img = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('driving_front_img')->move($destination_path, $driving_front_img)) {
                    $driving_front_img = $driving_front_img;
              
                } else {
                    $driving_front_img	= null;
                }
            } else {
                $driving_front_img	= null;
            }
            
            if ($request->hasFile('driving_back_img')) {
                $original_filename = $request->file('driving_back_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $driving_back_img = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('driving_back_img')->move($destination_path, $driving_back_img)) {
                    $driving_back_img = $driving_back_img;
              
                } else {
                    $driving_back_img	= null;
                }
            } else {
                $driving_back_img	= null;
            }
             
            $sql1 = "SELECT * FROM tbl_p_users where puser_email='$request->email' or puser_phno='$request->phoneno'";
            $results1 = app('db')->select($sql1);
            
            
            if(empty($results1)){
                $sql = "INSERT INTO `tbl_p_users`(puser_account_status,`puser_name`, `puser_address`, `puser_lat_long`, `puser_phno`, `puser_email`, `puser_adhar_number`,`puser_dl_number`, `puser_pan_number`, `puser_GST_number`, `puser_bankacc_number`, `puser_bankacc_ifsc`, `puser_profile_image`,`puser_type`
                , `puser_long`, `bank_holder`, `found`, `puser_document_url`,`puser_created_date`,`puser_created_by`,`year_of_experience`,`idproof`,`expertise`,`date_of_birth`,`gender`,`bank_image`,`adhar_front_img`,`adhar_back_img`,`driving_front_img`,`driving_back_img`,interest) 
                VALUES ('Active','$request->name','$request->address','$request->lat_long','$request->phoneno','$request->email','$request->aadharno','$request->drivingno','$request->panno','$request->gstno','$request->bankaccountno','$request->ifsccode','$image','$request->user_type','$request->long',
                '$request->holdername','$request->found','$uploadcv', CURRENT_TIMESTAMP,'user app','$request->year_of_experience','$request->idproof','$request->expertise','$request->date_of_birth','$request->gender','$bank_image','$adhar_front_img','$adhar_back_img','$driving_front_img','$driving_back_img','$request->interest')";
               
                $results = app('db')->insert($sql);
                $id = DB::getPdo()->lastInsertId();
               
               // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
                $ch = curl_init();
            
                curl_setopt($ch, CURLOPT_URL, 'https://api.sendgrid.com/v3/mail/send');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"personalizations\": [{\"to\": [{\"email\": \"$request->email\"}]}],\"from\": {\"email\": \"no-reply@vahanom.com\"},\"subject\":\"Great news! Your registration is confirmed!!\",\"content\": [{\"type\": \"text/html\",\"value\": \"Dear Dear $request->name,<br><p>You’ve successfully completed registration for Vahanom as a user. It’s time to check your Vehicle health. 
                    <br>Thank you for registering!<br>Team Vahanom\"}]}");
   
                $headers = array();
                $headers[] = 'Authorization: Bearer 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';
                // $headers[] = 'Authorization: Bearer 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';
                $headers[] = 'Content-Type: application/json';
                 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
             
                if (curl_errno($ch)){
                    echo 'Error:' . curl_error($ch);
                   // dd($ch);exit;
                }
                curl_close($ch);
                //dd($request->name);
               
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "User Created Successfully.!";
                $output['puser_id'] =  $id;
                
                }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "User already Created.!";
                $output['puser_id']=$results1[0]->puser_id;
                    }
        }
            return response()->json($output);
    }
    public function partnerupdateaddress(Request $request){
        $uid = $request->header('userid');
        $sql = "UPDATE `tbl_p_users` SET puser_add = '$request->pincode', `puser_address`= '$request->address' , `puser_lat_long`= '$request->lat' , `puser_long`= '$request->log' WHERE puser_id = '$uid'";
        // echo $sql;exit;
        $results = app('db')->update($sql); 
            if(!empty($results)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Address updated Successfully";
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] = "Something went wrong!!";
            }
        return response()->json($output);
    }
    
    public function getpartneraddress(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT puser_address,puser_add,puser_lat_long,puser_long from tbl_p_users where puser_id='$uid'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "address";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No address found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}


public function getpartnernotification(Request $request){
    $uid = $request->header('userid');
    $month=date("Y-m-d h:i:s",strtotime("-7 day"));
    $sql = "SELECT nt.*,m.muser_name as user_name,m.muser_email as user_email,m.m_phno as user_ph,p.puser_name from tbl_noti_store as nt left join tbl_m_users as m on m.muser_id=nt.user_id left JOIN tbl_p_users as p on p.puser_id=nt.partner_id left join tbl_orders on tbl_orders.order_id=nt.order_id where tbl_orders.order_status != 'Completed' and partner_id=$uid and create_at>='$month' order by nt.id desc";
    // echo $sql;exit;
    $result = app('db')->select($sql);
    $count = count($result);
    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['cnt'] = $count;
        $output['msg'] =  "Notifications List";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Notification Found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

    
    public function partnerprofileupdate(Request $request){
         $uid = $request->header('userid');
         
        $validator = Validator::make($request->all(), [
            'phoneno' => 'required|max:10|min:10',
            'name' => 'required',
            'email' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 200);
            }else{
            if ($request->hasFile('image')) {
                $original_filename = $request->file('image')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                if($request->user_type=='Supplier'){
                    $destination_path = '../../admin/assets/images/supplier';
                }
                else{
                        $destination_path = '../../admin/assets/images/technician';
                }
                $image = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image')->move($destination_path, $image)) {
                    $image =  $image;
              
                } else {
                    $image=null;
                }
            } else {
                $image=$request->image;
            }
            
            //   if ($request->hasFile('idproof')) {
            //     $original_filename = $request->file('idproof')->getClientOriginalName();
            //     $original_filename_arr = explode('.', $original_filename);
            //     $file_ext = end($original_filename_arr);
            //     if($request->user_type=='Supplier'){
            //         $destination_path = '../../admin/assets/images/supplier';
            //     }
            //     else{
            //             $destination_path = '../../admin/assets/images/technician';
            //     }
            //     $idproof = 'U-' . rand() . '.' . $file_ext;
            //     if ($request->file('idproof')->move($destination_path, $idproof)) {
            //         $idproof =  $idproof;
              
            //     } else {
            //         $idproof=null;
            //     }
            // } else {
            //     $idproof=null;
            // }
        
            if ($request->hasFile('uploadcv')) {
                $original_filename = $request->file('uploadcv')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $uploadcv = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('uploadcv')->move($destination_path, $uploadcv)) {
                    $uploadcv =  $uploadcv;
              
                } else {
                    $uploadcv=null;
                }
            } else {
                $uploadcv=$request->cvname;
            }
            
            if ($request->hasFile('bank_image')) {
                $original_filename = $request->file('bank_image')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $bank_image	= 'U-' . rand() . '.' . $file_ext;
                if ($request->file('bank_image')->move($destination_path, $bank_image)) {
                    $bank_image = $bank_image;
              
                } else {
                    $bank_image	= null;
                }
            } else {
                $bank_image	= $request->getbank;
            }
            
            if ($request->hasFile('adhar_front_img')) {
                $original_filename = $request->file('adhar_front_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $adhar_front_img = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('adhar_front_img')->move($destination_path, $adhar_front_img)) {
                    $adhar_front_img = $adhar_front_img;
              
                } else {
                    $adhar_front_img= null;
                }
            } else {
                $adhar_front_img = $request->get_adhar_front_img;
            }
            
             if ($request->hasFile('adhar_back_img')) {
                $original_filename = $request->file('adhar_back_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $adhar_back_img	= 'U-' . rand() . '.' . $file_ext;
                if ($request->file('adhar_back_img')->move($destination_path, $adhar_back_img)) {
                    $adhar_back_img = $adhar_back_img;
              
                } else {
                    $adhar_back_img	= null;
                }
            } else {
                $adhar_back_img	= $request->get_adhar_back_img;
            }
            
             if ($request->hasFile('driving_front_img')) {
                $original_filename = $request->file('driving_front_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $driving_front_img = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('driving_front_img')->move($destination_path, $driving_front_img)) {
                    $driving_front_img = $driving_front_img;
              
                } else {
                    $driving_front_img	= null;
                }
            } else {
                $driving_front_img	= $request->get_driving_front_img;
            }
            
            if ($request->hasFile('driving_back_img')) {
                $original_filename = $request->file('driving_back_img')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $driving_back_img = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('driving_back_img')->move($destination_path, $driving_back_img)) {
                    $driving_back_img = $driving_back_img;
              
                } else {
                    $driving_back_img = null;
                }
            } else {
                $driving_back_img = $request->get_driving_back_img;
            }
             
            // $sql1 = "SELECT * FROM tbl_p_users where puser_email='$request->email' or puser_phno='$request->phoneno'";
            // $results1 = app('db')->select($sql1);
           
               $sql1="UPDATE `tbl_p_users` SET `puser_name`= '$request->name',`puser_adhar_number`= '$request->aadharno',`puser_dl_number`= '$request->drivingno',`puser_pan_number`= '$request->panno',`puser_GST_number`= '$request->gstno',`puser_company_reg_number`= '$request->firmno',`found`= '$request->found',`year_of_experience`= '$request->year_of_experience',`date_of_birth`= '$request->date_of_birth',`gender`= '$request->gender',";
                $sql = " `puser_profile_image`= '$image',`puser_document_url`= '$uploadcv',`adhar_front_img`= '$adhar_front_img',`adhar_back_img`= '$adhar_back_img',`driving_front_img`= '$driving_front_img',`driving_back_img`= '$driving_back_img' WHERE puser_id = '$uid'";

              $sql2= $sql1.$sql;
            //echo $sql2;die;
            $results = app('db')->update($sql2);

            if(!empty($results)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Profile Updated";
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Profile Not Updated";
            }
                
        }
            return response()->json($output);
    }

    public function locationupdate(Request $request){
        // print_r($request->usertype);exit
        
        $sql = "SELECT id from tbl_location where user_id='$request->id'";
        $result = app('db')->select($sql);
        // print_r($result);exit;
        if($result){
            // print_r(1234);exit;
            $uid=$result[0]->id;
            $sql = "UPDATE `tbl_location` SET `lat`= '$request->lat',`long`= '$request->long' WHERE user_id = '$uid'";
            // echo $sql;die;
            $result = app('db')->update($sql);
            if($result){
             $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Location Updated Successfully";
            $output['data'] =  $result;
            }else{
                $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Location not Updated";
            
            }
            return response()->json($output);
        }else{
        $sql = "INSERT INTO `tbl_location`(`lat`, `long`,`user_id`) VALUES ('$request->lat','$request->long','$request->id')";
            $result = app('db')->insert($sql);
            if($result){
             $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Location Inserted Successfully";
            $output['data'] =  $result;
            }else{
                
                $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Location not Inserted";
            
            }
            return response()->json($output);
        }
        
    }

public function categorylistspare(Request $request){
    $sql = "SELECT * from tbl_spare_catg where spcat_status='Active'";
        $result = app('db')->select($sql);
        $i=0;
        foreach($result as $row){
            $result[$i]->spcat_title = $row->spcat_title.' ('.$row->spcat_vehicle_type.')';
            $i++;
        }
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare Category";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Spare Category found";
            $output['data'] =  $result;
        }
        return response()->json($output);
        
}
    public function categoryspare(Request $request){
   
    
        
        
    $sql = "SELECT * from tbl_spare_catg where spcat_status='Active' and spcat_vehicle_type='$request->vehicle_type'";
    $result = app('db')->select($sql);

    $sql1 = "SELECT puser_name,puser_id,puser_pincode,puser_address from tbl_p_users where puser_id='$request->supplier_id' ";
    $result1 = app('db')->select($sql1); 
    //print_r($result);exit;

    if(!empty($result)){
        
    $j=0;
    foreach($result as $row){ 
        $i=0;
      //  $sparelist=array();
      $uid = $request->muser_id;
    
         $sql2 ="SELECT spare_cat_id,p_spare_id,p_spare_mrp+labour_rate as p_spare_mrp,p_spare_gst_percentage,p_spare_selling_price+labour_rate as p_spare_selling_price,labour_rate,gst_amount,gstspprice,sp_title,sp_description,sp_hsn_code,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,spcat_title,spcat_description, p_spare_create_by, spare_cat_id, tbl_spare.veh_id, tbl_spare.vh_cost ,(p_spare_selling_price+vh_cost) as final_cost from tbl_p_spares
    left join tbl_spare on tbl_spare.sp_id=tbl_p_spares.spare_id
   left join tbl_spare_catg on tbl_spare_catg.spcat_id=tbl_p_spares.spare_cat_id
  where p_spare_create_by=$request->supplier_id and spare_cat_id=$row->spcat_id and p_spare_status='Active' and spcat_status='Active' ";
         $result2 = app('db')->select($sql2);
        //  echo $sql2;
         if(!empty($result2)){
             $cnt=count($result2);
                 if($row->spcat_id == $result2[$i]->spare_cat_id){
                 $result[$j]->spare_list = $result2;
                     
             }
                
         }
        $j++; 
          $i++; 
    }
     
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Spare Category";
        $output['data'] =  $result;
        $output['supplier'] =  $result1;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Spare Category found";
        $output['data'] =  $result;
        $output['supplier'] =  $result1;
    }
    return response()->json($output);
    }

    public function getspare(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_spare where sp_category_id='$request->category_id' and sp_status='Active'";
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Spare found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function submitspare(Request $request){
        $uid = $request->header('userid');
        $gstamount=($request->p_spare_selling_price*$request->p_spare_gst_percentage)/100;
        $gstspprice=$request->p_spare_selling_price-$gstamount;

        $sql1 = "SELECT * from tbl_p_spares where spare_cat_id='$request->spare_cat_id' and spare_id='$request->spare_id' and p_spare_create_by='$uid' and p_spare_status='Active'";
        $result = app('db')->select($sql1);
        if(!empty($result)){
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Spare Already Created";
    
        }else{
            $sql = "INSERT INTO `tbl_p_spares`(`spare_cat_id`, `spare_id`,`p_spare_mrp`, `p_spare_gst_percentage`, `p_spare_selling_price`, `p_spare_status`,`p_spare_create_date`, `p_spare_create_by`, `gst_amount`,`gstspprice`) VALUES ('$request->spare_cat_id','$request->spare_id','$request->p_spare_mrp','$request->p_spare_gst_percentage','$request->p_spare_selling_price','Active',CURRENT_TIMESTAMP,'$uid','$gstamount','$gstspprice')";
            $results = app('db')->insert($sql);
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare Created Successufully";
          
        }
        return response()->json($output);
    }

    public function listviewspares(Request $request){
   
        $uid = $request->header('userid');
        $sql = "SELECT spcat_id,spcat_title,sp_title,sp_id,p_spare_id,p_spare_status,p_spare_selling_price,p_spare_gst_percentage,p_spare_mrp from tbl_p_spares left join tbl_spare_catg on spcat_id=spare_cat_id left join tbl_spare on spare_id=sp_id where p_spare_create_by='$uid'";
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Spare found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
  
    public function detailsviewspares(Request $request){
   
        $uid = $request->header('userid');
        $sql = "SELECT spcat_id,spcat_title,sp_title,sp_id,p_spare_id,p_spare_status,p_spare_selling_price,p_spare_gst_percentage,p_spare_mrp from tbl_p_spares left join tbl_spare_catg on spcat_id=spare_cat_id left join tbl_spare on spare_id=sp_id where p_spare_create_by='$uid' and p_spare_id='$request->p_spares_id'";
      
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Spare found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function removespare(Request $request){
        $uid = $request->header('userid');
     
        $sql = "delete from tbl_p_spares where p_spare_id='$request->p_spares_id'";
        $results = app('db')->delete($sql);
        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare Deleted";
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Spare Not Deleted";
        }
        return response()->json($output);
    }

    public function updatespare(Request $request){
        $uid = $request->header('userid');
        $gstamount=($request->p_spare_selling_price*$request->p_spare_gst_percentage)/100;
        $gstspprice=$request->p_spare_selling_price-$gstamount;

        $sql = "UPDATE `tbl_p_spares` SET `p_spare_mrp`= '$request->p_spare_mrp', `p_spare_gst_percentage`= '$request->p_spare_gst_percentage', `p_spare_selling_price` = '$request->p_spare_selling_price' , `gst_amount`= '$gstamount',`gstspprice`= '$gstspprice' WHERE p_spare_id = '$request->p_spare_id'";
        // echo $sql;die;
        $results = app('db')->update($sql);
        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare Updated";
    
        }else{
           
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Spare Not Updated";
        }
        return response()->json($output);
    }

    public function supplierorder(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_orders where supplier_id='$uid'";
        // echo $sql;die;
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Orders";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Orders found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function technicianorder(Request $request){
        $uid = $request->header('userid');
           $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address left join tbl_m_users on puser_id=muser_id  where technician_id='$uid' ORDER BY `order_id` DESC";
    
        // echo $sql;die;
        $result = app('db')->select($sql);
        $i=0;
        foreach($result as $row){
        if($result[$i]->order_schedule_date=="0000-00-00"){
            $result[$i]->order_schedule_date = '';
            
        }   
        $i++;
        }
        
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Orders";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Orders found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
    public function getpartnerprofile(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_p_users where puser_id='$uid'";
        // echo $sql;die;
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Supplier Data";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Supplier Data found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function alltools(Request $request){
        
        $uid = $request->header('userid');
   $sql = "SELECT * from tbl_puser_tools left join tbl_tools on tools_id=tool_id where technician_id='$uid'";
        $result = app('db')->select($sql);
      

        foreach($result as $result1_rec){
            $tools_id[]=$result1_rec->tools_id;
        }
        if(isset($tools_id)){
            $tools_id=implode(',',$tools_id);
            $sql1 = "SELECT * from tbl_tools where tool_status='Active' and tool_id NOT IN ($tools_id)";
      
        }
       else{
        $sql1 = "SELECT * from tbl_tools where tool_status='Active' ";
      
       }
       $result1 = app('db')->select($sql1);
       
        if(!empty($result1)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Tools";
            $output['added'] =  $result;
            $output['all'] =  $result1;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Tools Not found";
            $output['added'] =  $result;
            $output['all'] =  $result1;
        }
        return response()->json($output);
    }

    public function listviewtools(Request $request){
   
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_puser_tools left join tbl_tools on tools_id=tool_id where technician_id='$uid'";
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Tool";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Tool found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
  
    public function detailsviewtools(Request $request){
   
        $uid = $request->header('userid');
        
        $sql = "SELECT * from tbl_puser_tools left join tbl_tools on tools_id=tool_id where technician_id='$uid'";
      
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Tool";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Tool found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function removetool(Request $request){
        $uid = $request->header('userid');
     
        $sql = "delete from tbl_puser_tools where p_usertool_id='$request->p_usertool_id'";
        $results = app('db')->delete($sql);
        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Spare Deleted";
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Spare Not Deleted";
        }
        return response()->json($output);
    }

    public function submittools(Request $request){
        $uid = $request->header('userid');
        // var_dump(explode(',',$request->tools_id));die;
      
        $toolsid=$request->tools_id;

        for($i=0;$i<count($request->tools_id);$i++)
{
    $sql = "INSERT INTO `tbl_puser_tools`(`tools_id`, `p_usertool_status`,`p_usertool_create_date`, `technician_id`) VALUES ('$toolsid[$i]','Active',CURRENT_TIMESTAMP,'$uid')";
    $results = app('db')->insert($sql);
}

        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Tools added Successfully";
    
        }else{
 
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Tools Not Added";
          
        }
        return response()->json($output);
    }

  public function suppliertechorderlist(Request $request){
   
        $uid = $request->header('userid');
        if($request->User_type=='Supplier')
{
    if($request->orderstatus=='Pending'){//orderstatus=pending
        $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address  where  order_status='Pending' and (type_servicespare='Service' or type_servicespare='Spare') and supplier_id='$uid' ORDER BY `tbl_orders`.`order_date` DESC";
      
        $result = app('db')->select($sql);

    } else if($request->orderstatus=='Ongoing'){
        $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address where supplier_id='$uid' and order_status='Ongoing' ORDER BY `tbl_orders`.`order_date` DESC";
      
        $result = app('db')->select($sql);

    }else if($request->orderstatus=='Completed'){
        $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address where supplier_id='$uid' and order_status='Completed' ORDER BY `tbl_orders`.`order_date` DESC";
    //   echo $sql;exit;
        $result = app('db')->select($sql);
    }
    else if($request->orderstatus=='Confirm'){//orderstatus=confirm
        $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address left join tbl_m_users on puser_id=muser_id  where supplier_id='$uid' and order_status !='Completed'and order_status !='Pending' and supplier_order_status='Accept' ORDER BY `tbl_orders`.`order_date` DESC";
      
        $result = app('db')->select($sql);
    }


}else{
    if($request->orderstatus=='Pending'){
     //  $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address left join tbl_send_order_notification on tbl_send_order_notification.order_id=tbl_orders.order_id where  order_status='Confirm' and type_servicespare ='Spare' and tbl_send_order_notification.p_user_id='$uid'";
     //echo 'hiiiii';
       //echo $sql;
        //$result2 = app('db')->select($sql);
   
       // dd($result2);
        //  $sql1 = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address left join tbl_send_order_notification on tbl_send_order_notification.order_id=tbl_orders.order_id where order_status='Pending' and type_servicespare ='Spare' and tbl_send_order_notification.p_user_id='$uid'";
       
       
            //$sql = "SELECT DISTINCT tbl_orders.order_d,puser_id,order_type,payment_id,order_date,order_schedule_date,order_schedule_timeslot,order_address,order_pincode,technician_id,technician_accept_status,supplier_id,supplier_order_status,technician_assignby,supplier_assignby,technician_otp,technician_otp_status,supplier_otp,supplier_otp_status,order_details_id,order_number,order_status,customer_otp,customer_otp_status,order_subtotal_amount,order_gst_amount,order_coupon_code,order_coupon_discount,order_total_amount,order_coupon_amt,order_trackingnumber,type_servicespare,payment_status,uservehicleid,note,voicemsg,parentid,out_odomete_reading,spare_dispatch,order_latitude,order_longitude FROM tbl_orders LEFT JOIN tbl_send_order_notification ON tbl_send_order_notification.p_user_id = tbl_orders.technician_id where tbl_send_order_notification.p_user_id='$uid' and tbl_orders.`order_status` LIKE 'Confirm'";
            // $sql = "SELECT DISTINCT tbl_orders.* FROM tbl_orders LEFT JOIN tbl_send_order_notification ON tbl_send_order_notification.p_user_id = tbl_orders.technician_id or technician_id=0 where tbl_send_order_notification.p_user_id='$uid' and tbl_orders.`order_status` LIKE 'Confirm'AND `spare_dispatch` !='' and technician_accept_status != 'Accept'";
           // $sql = "SELECT DISTINCT tbl_orders.* FROM tbl_orders left join tbl_maddress on addressid=order_address LEFT JOIN tbl_send_order_notification ON tbl_send_order_notification.p_user_id = tbl_orders.technician_id or technician_id=0 where tbl_send_order_notification.p_user_id='$uid' and tbl_orders.`order_status` LIKE 'Confirm' and `spare_dispatch` IS NULL and supplier_order_status = 'Accept' and technician_accept_status !='' ORDER BY order_id DESC";
        
           $sql="SELECT DISTINCT tbl_orders.*, tbl_maddress.* FROM tbl_orders inner join tbl_send_order_notification ON tbl_send_order_notification.order_id = tbl_orders.order_id  join tbl_maddress on addressid=order_address  where tbl_send_order_notification.p_user_id =$uid AND tbl_orders.`order_status` LIKE 'Confirm' and `spare_dispatch` is null and supplier_order_status = 'Accept'  and technician_accept_status IS NULL ORDER BY `tbl_orders`.`order_id` DESC";
        //   echo $sql; echo 'confirm';
            $result = app('db')->select($sql);
         
        // $sql1 = "SELECT DISTINCT tbl_orders.order_id, puser_id,order_type,payment_id,order_date,order_schedule_date,order_schedule_timeslot,order_address,order_pincode,technician_id,technician_accept_status,supplier_id,supplier_order_status,technician_assignby,supplier_assignby,technician_otp,technician_otp_status,supplier_otp,supplier_otp_status,order_details_id,order_number,order_status,customer_otp,customer_otp_status,order_subtotal_amount,order_gst_amount,order_coupon_code,order_coupon_discount,order_total_amount,order_coupon_amt,order_trackingnumber,type_servicespare,payment_status,uservehicleid,note,voicemsg,parentid,out_odomete_reading,spare_dispatch,order_latitude,order_longitude FROM tbl_orders LEFT JOIN tbl_send_order_notification ON tbl_send_order_notification.p_user_id = tbl_orders.technician_id where tbl_send_order_notification.p_user_id='$uid' and tbl_orders.`order_status` LIKE 'Confirm'";
           $sql1 = "SELECT DISTINCT tbl_orders.*, tbl_maddress.* FROM tbl_orders join tbl_maddress on addressid=order_address inner join tbl_send_order_notification ON tbl_send_order_notification.order_id = tbl_orders.order_id where tbl_send_order_notification.p_user_id =$uid AND tbl_orders.`order_status` LIKE 'Pending' and `spare_dispatch` IS NULL and type_servicespare in('Service','Prepurchase','Inspection') ORDER BY `tbl_orders`.`order_id` DESC";
           $result1 = app('db')->select($sql1);
                  
          // $sql2 = "SELECT DISTINCT tbl_orders.order_id, puser_id,order_type,payment_id,order_date,order_schedule_date,order_schedule_timeslot,order_address,order_pincode,technician_id,technician_accept_status,supplier_id,supplier_order_status,technician_assignby,supplier_assignby,technician_otp,technician_otp_status,supplier_otp,supplier_otp_status,order_details_id,order_number,order_status,customer_otp,customer_otp_status,order_subtotal_amount,order_gst_amount,order_coupon_code,order_coupon_discount,order_total_amount,order_coupon_amt,order_trackingnumber,type_servicespare,payment_status,uservehicleid,note,voicemsg,parentid,out_odomete_reading,spare_dispatch,order_latitude,order_longitude FROM tbl_orders LEFT JOIN tbl_send_order_notification ON tbl_send_order_notification.p_user_id = tbl_orders.technician_id where tbl_send_order_notification.p_user_id='31' and tbl_orders.`order_status` LIKE 'spares dispatched'";
             $sql2 = "SELECT DISTINCT tbl_orders.*, tbl_maddress.* FROM tbl_orders join tbl_maddress on addressid=order_address inner join tbl_send_order_notification ON tbl_send_order_notification.order_id = tbl_orders.order_id where tbl_send_order_notification.p_user_id =$uid AND tbl_orders.`order_status` LIKE 'spares dispatched' and spare_dispatch ='' ORDER BY `tbl_orders`.`order_id` DESC";
         // echo $sql2; echo 'spare dispatched'; die;
             $result2 = app('db')->select($sql2);
         
          // $sql3 = "SELECT DISTINCT tbl_orders.* FROM tbl_orders LEFT JOIN tbl_send_order_notification ON tbl_send_order_notification.p_user_id = tbl_orders.technician_id or technician_id=0 where tbl_send_order_notification.p_user_id='$uid' and tbl_orders.`order_status` LIKE 'Confirm' AND `spare_dispatch` ='Dispatch' and technician_accept_status IS NULL";
             $sql3 = "SELECT DISTINCT tbl_orders.*, tbl_maddress.* FROM tbl_orders join tbl_maddress on addressid=order_address inner join tbl_send_order_notification ON tbl_send_order_notification.order_id = tbl_orders.order_id where tbl_send_order_notification.p_user_id =$uid AND tbl_orders.`order_status` LIKE 'Confirm' AND `spare_dispatch` ='Dispatch' and technician_accept_status ='Accept' ORDER BY `tbl_orders`.`order_id` DESC";
          // echo $sql3;// echo $sql3;
             $result3 = app('db')->select($sql3);
           // dd($sql3);
          
    
              // $sql4 = "SELECT DISTINCT tbl_orders.* FROM tbl_orders left join tbl_maddress on addressid=order_address LEFT JOIN tbl_send_order_notification ON tbl_send_order_notification.p_user_id = tbl_orders.technician_id or technician_id=0 where tbl_send_order_notification.p_user_id='$uid' and tbl_orders.`order_status` LIKE 'Pending' and `spare_dispatch` IS NOT NULL ";
        //echo $sql1;die;
          // $result4 = app('db')->select($sql4);
           
           
          $abcd=[];
          $i=0;
          if(isset($result2)){
              foreach($result2 as $result2_rec){
                  $abcd[$i]=$result2_rec;
                  $i++;
              }
          }
          if(isset($result1)){
              foreach($result1 as $result1_rec){
                  $abcd[$i]=$result1_rec;
                  $i++;
              }
          }
            if(isset($result)){
              foreach($result as $result_rec){
                  $abcd[$i]=$result_rec;
                  $i++;
              }
          }
          if(isset($result3)){
              foreach($result3 as $result_rec){
                  $abcd[$i]=$result_rec;
                  $i++;
              }
          }
           // if(isset($result4)){
        //       foreach($result4 as $result_rec){
        //           $abcd[$i]=$result_rec;
        //           $i++;
        //       }
        //   }
          $result =  $abcd;
//dd($result);die;

          

    } else if($request->orderstatus=='Ongoing'){
        $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address where technician_id='$uid' and order_status='Ongoing' ORDER BY tbl_orders.order_id DESC";
      
        $result = app('db')->select($sql);

    }else if($request->orderstatus=='Completed'){
        $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address where technician_id='$uid' and order_status='Completed' ORDER BY tbl_orders.order_id DESC";
      
        $result = app('db')->select($sql);
    }
    else if($request->orderstatus=='Confirm'){ //orderstatus=accept
        $sql = "SELECT * from tbl_orders left join tbl_maddress on addressid=order_address left join tbl_m_users on puser_id=muser_id where technician_id='$uid' and technician_accept_status='Accept' and order_status !='Completed' ORDER BY tbl_orders.order_id DESC";
      
        $result = app('db')->select($sql);
    }

}

$j=0;
foreach($result as $row){
   if($row->order_schedule_date =="0000-00-00"){
       $result[$j]->order_schedule_date='';
   }
  $j++;
}

 if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Orders";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No order found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }


    public function submitservice(Request $request){
        $uid = $request->header('userid');

        $sql1 = "SELECT * from tbl_pservice where pservice_id='$request->pservice_id' and ptechnician_id='$uid' and tool_status='Active'";
        // echo $sql1;die;
        $result = app('db')->select($sql1);
        if(!empty($result)){ 
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Service Already Created";
    
        }else{
            $sql = "INSERT INTO `tbl_pservice`(`pservice_id`, `ptechnician_id`,`tool_status`) VALUES ('$request->pservice_id','$uid','Active')";
            $results = app('db')->insert($sql);
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Service Created Successufully";
          
        }
        return response()->json($output);
    }

    public function removeservice(Request $request){
        $uid = $request->header('userid');
     
        $sql = "delete from tbl_pservice where puser_serviceid='$request->puser_serviceid'";
        $results = app('db')->delete($sql);
        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Service Deleted";
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Service Not Deleted";
        }
        return response()->json($output);
    }

    public function alltechnicianservice(Request $request){
   
        $uid = $request->header('userid');
        
        $sql = "SELECT * from tbl_pservice left join tbl_service on ser_id=pservice_id where ptechnician_id='$uid'";
      
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Service";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Service found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function techniservice(Request $request){
   
        $uid = $request->header('userid');
         $sql = "SELECT * from tbl_pservice left join tbl_service on ser_id=pservice_id where puser_serviceid='$request->puser_serviceid'";
      
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Service";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Service found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function partnerdutystatus(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT puser_availability_status from tbl_p_users where puser_id='$uid'";
        $result = app('db')->select($sql);
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Status";
            $output['data'] =  $result[0]->puser_availability_status;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No status";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function partnerdutystatusupdate(Request $request){
        $uid = $request->header('userid');
        $sql = "UPDATE `tbl_p_users` SET `puser_availability_status`= '$request->status' WHERE puser_id = '$uid'";
        // echo $sql;die;
        $results = app('db')->update($sql);
        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Status Updated";
    
        }else{
           
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Status Not Updated";
        }
        return response()->json($output);
    }
    
    
    
    public function earnings(Request $request){
        $usertype = $request->usertype;
        $user_id = $request->userid;
        // print_r($request->usertype);exit;
        //=========================Supplier=========================
        if($usertype=="Supplier"){
            
            $sql = "SELECT order_date,sp1.p_spare_selling_price,sp.margin from tbl_orders join tbl_orderdetails on tbl_orderdetails.od_order_id = tbl_orders.order_id inner JOIN tbl_p_users on tbl_p_users.puser_id = tbl_orders.supplier_id  LEFT join tbl_p_spares sp1 on tbl_orderdetails.od_service_id=sp1.p_spare_id LEFT join tbl_spare sp on sp.sp_id=sp1.spare_id where tbl_orders.order_status='Completed' AND tbl_orders.supplier_id='$user_id'"; 
            // echo $sql;exit;
            $results = app('db')->select($sql);
            $i=0;
            $result=array();
            if($results){
                $total_price=0;
                foreach($results as $row){
                        $sale_price = $row->p_spare_selling_price;
                        $margin = $row->margin;
                        $total = $sale_price-($sale_price*($margin/100));
                        $results[$i]->total_price=$total;
                        $total_price+=$total;
                        $i++;    
                    }
                                        
                $t_p= round($total_price,2);
                $result['total_earning']=(string)$t_p;
                    // $result['total_earning']=round($total_price,2);
                    // print_r(response()->json($result));exit;
                    $month=date("Y-m-d",strtotime("-1 month"));
                    $sql = "SELECT order_date,sp1.p_spare_selling_price,sp.margin from tbl_orders join tbl_orderdetails on tbl_orderdetails.od_order_id = tbl_orders.order_id inner JOIN tbl_p_users on tbl_p_users.puser_id = tbl_orders.supplier_id  LEFT join tbl_p_spares sp1 on tbl_orderdetails.od_service_id=sp1.p_spare_id LEFT join tbl_spare sp on sp.sp_id=sp1.spare_id where tbl_orders.order_status='Completed' AND tbl_orders.supplier_id='$user_id' and order_date>='$month' and order_date<'$today'"; 
                    // echo $sql;exit;
                    $results = app('db')->select($sql);
                    $i=0;
                    // $result=array();
                    $total_price_month=0;
                    foreach($results as $row){
                        $sale_price = $row->p_spare_selling_price;
                        $margin = $row->margin;
                        $total = $sale_price-($sale_price*($margin/100));
                        $results[$i]->total_price=$total;
                        $total_price_month+=$total;
                        $i++;    
                    }
                        $p_m_e = round($total_price_month,2);
                        $result['past_month_earning']=(string)$p_m_e;
                                        
                    $month=date("Y-m-d",strtotime("-7 day"));
                    $sql = "SELECT order_date,sp1.p_spare_selling_price,sp.margin from tbl_orders join tbl_orderdetails on tbl_orderdetails.od_order_id = tbl_orders.order_id inner JOIN tbl_p_users on tbl_p_users.puser_id = tbl_orders.supplier_id  LEFT join tbl_p_spares sp1 on tbl_orderdetails.od_service_id=sp1.p_spare_id LEFT join tbl_spare sp on sp.sp_id=sp1.spare_id where tbl_orders.order_status='Completed' AND tbl_orders.supplier_id='$user_id' and order_date>='$month' and order_date<'$today' "; 
                    // echo $sql;exit;
                    $results = app('db')->select($sql);
                    $i=0;
                    // $result=array();
                        $total_price_week=0;
                        foreach($results as $row){
                            $sale_price = $row->p_spare_selling_price;
                            $margin = $row->margin;
                            $total = $sale_price-($sale_price*($margin/100));
                            $results[$i]->total_price=$total;
                            $total_price_week+=$total;
                            $i++;    
                        }
                        $p_w_e=round($total_price_week,2);
                        $result['past_week_earning']=(string)$p_w_e;
    
                        return response()->json($result);

            
            }else{
                $output['Data'] = "Data Not Found!!!";
                return $output;
                
            }
            
            
        }
        //=========================Technician=========================
        if($usertype=="Technician")
        {
            $sql = "SELECT order_date,labour_rate,ser_extra_field from tbl_orders join tbl_orderdetails on tbl_orderdetails.od_order_id = tbl_orders.order_id  inner JOIN tbl_p_users on tbl_p_users.puser_id = tbl_orders.technician_id LEFT join tbl_service on tbl_service.ser_id=tbl_orderdetails.od_service_id where tbl_orders.order_status='Completed' AND tbl_orders.technician_id='$user_id'"; 
            // echo $sql;
            $results = app('db')->select($sql);
            // print_r($results);exit;
            $i=0;
            $total_price=0;
            $result=array();
            if($results){
                $total_price=0;
                foreach($results as $row){
                    $lbr_rate = $row->labour_rate;
                    $margin = $row->ser_extra_field;
                    $total = $lbr_rate-($lbr_rate*($margin/100));
                    $results[$i]->total_price=$total;
                    $total_price+=$total;
                    $i++;    
                }
                
                $t_p= round($total_price,2);
                $result['total_earning']=(string)$t_p;
                
                $month=date("Y-m-d",strtotime("-1 month"));
                $today = date("Y-m-d");
                $sql = "SELECT order_date,labour_rate,ser_extra_field from tbl_orders join tbl_orderdetails on tbl_orderdetails.od_order_id = tbl_orders.order_id  inner JOIN tbl_p_users on tbl_p_users.puser_id = tbl_orders.technician_id LEFT join tbl_service on tbl_service.ser_id=tbl_orderdetails.od_service_id where tbl_orders.order_status='Completed' AND tbl_orders.technician_id='$user_id' and order_date>='$month' and order_date<'$today'"; 
                // echo $sql;
                $results = app('db')->select($sql);
                // print_r($results);exit;
                $i=0;
                $total_price_month=0;
                // $result=array();
                
                $total_price=0;
                foreach($results as $row){
                    $lbr_rate = $row->labour_rate;
                    $margin = $row->ser_extra_field;
                    $total = $lbr_rate-($lbr_rate*($margin/100));
                    $results[$i]->total_price=$total;
                    $total_price_month+=$total;
                    $i++;    
                }
                
                $p_m_e= round($total_price_month,2);
                $result['past_month_earning']=(string)$p_m_e;
                    
                $month=date("Y-m-d",strtotime("-7 day"));
                $sql = "SELECT order_date,labour_rate,ser_extra_field from tbl_orders join tbl_orderdetails on tbl_orderdetails.od_order_id = tbl_orders.order_id  inner JOIN tbl_p_users on tbl_p_users.puser_id = tbl_orders.technician_id LEFT join tbl_service on tbl_service.ser_id=tbl_orderdetails.od_service_id where tbl_orders.order_status='Completed' AND tbl_orders.technician_id='$user_id' and order_date>'$month' and order_date<'$today'"; 
                // echo $sql;exit;
                $results = app('db')->select($sql);
                // print_r($results);exit;
                $i=0;
                $total_price_week=0;
                // $result=array();
                $total_price=0;
                foreach($results as $row){
                    $lbr_rate = $row->labour_rate;
                    $margin = $row->ser_extra_field;
                    $total = $lbr_rate-($lbr_rate*($margin/100));
                    $results[$i]->total_price=$total;
                    $total_price_week+=$total;
                    $i++;    
                }
                $p_w_e= round($total_price_week,2);
                $result['past_week_earning']=(string)$p_w_e;
                
                return response()->json($result);
            }else{
                $output['Data'] = "Data Not Found!!!";
                return $output;
                
            }
        }
        
    }
    
    
    
    public function technicianorderaccept(Request $request){
        $uid = $request->header('userid');
             if($request->User_type=='Supplier')
{
    $sql = "SELECT * from tbl_orders where order_status='Pending' and order_id='$request->orderid'";
        // $sql = "SELECT * from tbl_orders where order_status='Pending' and (supplier_id='0' or  supplier_id is null or  supplier_id='') and order_id='$request->orderid'"; 
                $results = app('db')->select($sql);
                
                if(!empty($results)){
                    
                    if($request->technician_id != '0')
                    {
        $sql = "delete from tbl_send_order_notification where order_id='$request->orderid'";
        //echo $sql; echo 'hiiiii';
        $results = app('db')->delete($sql);
                      //  dd($results);
                    } else{
                         $sql = "delete from tbl_send_order_notification where order_id='$request->orderid'";
                        // echo $sql; echo 'helloooo';
        $results = app('db')->delete($sql);
                        $sql1 = "SELECT * FROM tbl_p_users where puser_availability_status='On Dutty' and puser_type='Technician'";
            $results1 = app('db')->select($sql1);
                    
                      if(!empty($results1)){
                          
                        foreach($results1 as $row1){

                        $puser_id = $row1->puser_id;
                       
                        $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$request->orderid','$puser_id')";
                        app('db')->insert($sql);

                       
    
                        }
                        }
                    }
               $rndno = rand(1000, 9999);
                    $sqlupdate1 = "UPDATE `tbl_orders` SET `supplier_otp`='$rndno', `supplier_order_status`= 'Accept', `supplier_otp_status`= '0', `order_status`= 'Confirm',`supplier_id`= '$uid' WHERE order_id='$request->orderid'";
                    //  $sqlupdate1 = "UPDATE `tbl_orders` SET `supplier_otp`='$rndno', `supplier_order_status`= 'Accept', `supplier_otp_status`= '0', `order_status`= 'Confirm',  `spare_dispatch`= 'Dispatch',`supplier_id`= '$uid' WHERE order_id='$request->orderid'";
                    
                    app('db')->update($sqlupdate1);
                    
                       
                         $sql1 = "SELECT * from tbl_orders left join tbl_m_users on puser_id=muser_id  where order_id='$request->orderid'";
        
        $result1 = app('db')->select($sql1);
         $deviceid = $result1[0]->m_deviceid;
            $muser_name = $result1[0]->muser_name;
        //  var_dump($deviceid);  die('sssss');
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"Dear $muser_name your order has been confirmed, our technician will contact you shortly\"},\n\"headings\": {\"en\": \"Dear $muser_name Order Confirmed Successfully\"},\n\"include_player_ids\": [\"$deviceid\"]}");
                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
                     $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['msg'] =  "Order Confirmed Successfully";
            
                }else{
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] = "Something went wrong!!";
                }
}else if($request->User_type=='Technician'){
    $sql1 = "SELECT * from tbl_orders where  order_id='$request->orderid'";
    $results1 = app('db')->select($sql1);
    //print_r($results1);die;
if($results1[0]->type_servicespare=='Spare')    {
    $tech_id=$results1[0]->technician_id;
    //dd($tech_id);
    // $sql = "SELECT * from tbl_orders where order_status='spares dispatched' and (technician_id='0' or  technician_id is null or  technician_id='' or technician_id='$tech_id') and order_id='$request->orderid'";
//echo $sql;die;
  $sql = "SELECT * from tbl_orders where order_id='$request->orderid' and order_status='Confirm' and supplier_order_status= 'Accept' and (technician_id='0' or  technician_id is null or  technician_id='' or technician_id='$tech_id')";
//echo $sql;die;
 //$results = app('db')->select($sql);
  //dd($results);
  
}else{
       $sql = "SELECT * from tbl_orders where order_status='Pending' and type_servicespare !='Spare' and order_id='$request->orderid'";
     // echo $sql; echo 'elseeeeee';die;
    // echo $sql; die;
     //$results = app('db')->select($sql);
}

       $results = app('db')->select($sql);
      //dd($results);
                if(!empty($results)){
                      $sql = "delete from tbl_send_order_notification where order_id='$request->orderid'";
        $results = app('db')->delete($sql);
       $sql2 = "SELECT * from tbl_orders left join tbl_p_users on tbl_p_users.puser_id=technician_id  where order_id='$request->orderid'";
        
        $result2 = app('db')->select($sql2);
        
          $sql1 = "SELECT * from tbl_orders left join tbl_m_users on puser_id=muser_id  where order_id='$request->orderid'";
        
        $result1 = app('db')->select($sql1);
         $deviceid = $result1[0]->m_deviceid;
         $muser_name = $result2[0]->puser_name;
        //  var_dump($deviceid);  die('sssss');
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"Order Started\"},\n\"headings\": {\"en\": \"Wow $muser_name has started the work!!\"},\n\"include_player_ids\": [\"$deviceid\"]}");
                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
                        
                        
                    $rndno = rand(1000, 9999);
                    $sqlupdate1 = "UPDATE `tbl_orders` SET `technician_otp`='$rndno', `technician_accept_status`= 'Accept', `technician_otp_status`= '0', `order_status`= 'Confirm', `technician_id`= '$uid' WHERE order_id='$request->orderid'";
                    app('db')->update($sqlupdate1);
                     $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['msg'] =  "Order Confirmed Successfully";
            
                }else{
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] = "Something went wrong!!";
                }
}
        
        
            return response()->json($output);
    }

    public function partnerorderotpverify(Request $request){
      
        $validator = Validator::make($request->all(), [
            'user_app_type' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 200);
            }


            if($request->user_app_type=='Supplier')
            {
                $sql = "SELECT * FROM tbl_orders where supplier_otp='$request->otp' and supplier_otp_status='0'  and order_id='$request->orderid' ";
                // echo $sql;die;
                $results = app('db')->select($sql);
                if(!empty($results)){
    
                    $rndno = rand(1000, 9999);
                    $sqlupdate1 = "UPDATE `tbl_orders` SET `technician_otp`='$rndno', supplier_otp_status='1',`technician_otp_status`= '0', `order_status`= 'spares dispatched', `spare_dispatch`= 'Dispatch' WHERE order_id='$request->orderid'";
                    app('db')->update($sqlupdate1);

                    $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['msg'] =  "OTP Verified Successfully";
                }else{
                    
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] =  "OTP Not Matched";
                }
            }elseif($request->user_app_type=='Technician'){
                $sql = "SELECT * FROM tbl_orders where technician_otp='$request->otp' and technician_otp_status='0'  and order_id='$request->orderid' ";
                $results = app('db')->select($sql);
                if(!empty($results)){
    
                    $rndno = rand(1000, 9999);
                    $sqlupdate1 = "UPDATE `tbl_orders` SET  `technician_otp_status`= '1', `order_status`= 'technician arrived' WHERE order_id='$request->orderid'";
                    app('db')->update($sqlupdate1);

                    $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['msg'] =  "OTP Verified Successfully";
                }else{
                    
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] =  "OTP Not Matched";
                }
            }
            return response()->json($output);
    }

    public function technicianorderuploadimg(Request $request){
      
      $validator = Validator::make($request->all(), [
            'orderid' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 200);
            } else {
            if ($request->hasFile('image1')) {
                $original_filename = $request->file('image1')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                $destination_path = '../../admin/assets/images/order';
                $image1 = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image1')->move($destination_path, $image1)) {
                    $image1 =  $image1;
              
                } else {
                    $image1=null;
                }
            } else {
                $image1=null;
            }

            if ($request->hasFile('image2')) {
                $original_filename = $request->file('image2')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
             $destination_path = '../../admin/assets/images/order';
                $image2 = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image2')->move($destination_path, $image2)) {
                    $image2 =  $image2;
              
                } else {
                    $image2=null;
                }
            } else {
                $image2=null;
            }
            
            if ($request->hasFile('image3')) {
                $original_filename = $request->file('image3')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                $destination_path = '../../admin/assets/images/order';
                $image3 = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image3')->move($destination_path, $image3)) {
                    $image3 =  $image3;
              
                } else {
                    $image3=null;
                }
            } else {
                $image3=null;
            }

            if ($request->hasFile('image4')) {
                $original_filename = $request->file('image4')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
              $destination_path = '../../admin/assets/images/order';
                $image4 = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image4')->move($destination_path, $image4)) {
                    $image4 =  $image4;
              
                } else {
                    $image4=null;
                }
            } else {
                $image4=null;
            }

            if ($request->hasFile('image5')) {
                $original_filename = $request->file('image5')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
              $destination_path = '../../admin/assets/images/order';
                $image5 = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image5')->move($destination_path, $image5)) {
                    $image5 =  $image5;
              
                } else {
                    $image5=null;
                }
            } else {
                $image5=null;
            }

          
            if ($request->hasFile('image6')) {
                $original_filename = $request->file('image6')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
             $destination_path = '../../admin/assets/images/order';
                $image6 = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('image6')->move($destination_path, $image6)) {
                    $image6 =  $image6;
              
                } else {
                    $image6=null;
                }
            } else {
                $image6=null;
            }
$sql = "SELECT orderID,image1,image2,image3,image4,image5,image6 from tbl_order_images where orderID='$request->orderid'";
      
        $result = app('db')->select($sql);
        if(empty($result)){
            
            
            $sql = "INSERT INTO `tbl_order_images`(`orderID`, `image1`, `image2`, `image3`, `image4`, `image5`, `image6`,`createddate`, `status`) VALUES ('$request->orderid','$image1','$image2','$image3','$image4','$image5','$image6', CURRENT_TIMESTAMP,'Active')";
            $results = app('db')->insert($sql);
            
            $sql2 = "UPDATE `tbl_orders` SET  `odometer_reading`= '$request->odometer_reading' WHERE order_id='$request->orderid'";
            $results2 = app('db')->update($sql2);
               // dd($results2);
            if(!empty($results)){
               
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Uploaded Successfully.!";
                
                }else{
                $output['status'] = false;
            $output['statuscode'] = 404;
                $output['msg'] =  "something went wrong.!";
                    }
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
                $output['msg'] =  "Images Already Uploaded.!";
        }
            return response()->json($output);
            }
    }
    
     public function orderdetails(Request $request){
   
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_orders left join tbl_m_users on puser_id=muser_id left join tbl_user_vehicles on uservehicleid=userv_id  where order_id='$request->orderid'";
        
        $result = app('db')->select($sql);

        if($result[0]->type_servicespare=='Service'){
            $sql1 = "SELECT * from tbl_orders left join tbl_orderdetails on od_order_id=order_id left join tbl_service on ser_id=od_service_id  where order_id='$request->orderid'";
        
        }else{
            $sql1 = "SELECT * from tbl_orders left join tbl_orderdetails on od_order_id=order_id left join tbl_p_spares on p_spare_id=od_service_id left join tbl_spare on spare_id=sp_id where order_id='$request->orderid'";

        }
        
        $sql4 = "SELECT * from tbl_maddress where addressid=".$result[0]->order_address;
            $result4 = app('db')->select($sql4);
            
        $sql2 = "SELECT orderID,image1,image2,image3,image4,image5,image6 from tbl_order_images where orderID='$request->orderid'";
      
        $result2 = app('db')->select($sql2);
    
        $sql3 = "SELECT * from submit_question where orderid='$request->orderid'";
      
        $result3 = app('db')->select($sql3);
     
        if(!empty($result2)){
            $output['upload_images_status'] = true;
        }else{
            $output['upload_images_status'] = false;
        }
        
            if($request->User_type=='Supplier')
        {
    
 if($result[0]->supplier_order_status=='Accept'){
    // dd($result[0]);
            $output['Accept_request_status'] = true;
           
        }else{
            $output['Accept_request_status'] = false;
            // dd($output);
        }
    if($result[0]->supplier_otp_status=='1'){
            $output['OTP_verify_status'] = true;
        }else{
            $output['OTP_verify_status'] = false;
        }
        
}else if($request->User_type=='Technician'){
   
          if($result[0]->technician_accept_status=='Accept'){
            $output['Accept_request_status'] = true;
        }else{
            $output['Accept_request_status'] = false;
        }
       if($result[0]->technician_otp_status=='1'){
            $output['OTP_verify_status'] = true;
        }else{
            $output['OTP_verify_status'] = false;
        }
        //  if($result5[0]->supplier_otp_status=='1'){
        //     $output['Supplier OTP_verify_status'] = true;
        // }else{
        //     $output['Supplier OTP_verify_status'] = false;
        // }
          if(!empty($result3)){
             // echo 'hii';
            $output['point_check_status'] = true;
           
        }else{
          //  echo 'hello';
            $output['point_check_status'] = false;
            //  dd($output);
        }
    
}

       
        $result1 = app('db')->select($sql1);
      
  
        if(!empty($result)){
            $i=0;
            foreach($result as $row){
                $result[$i]->payment_status = ucfirst($row->payment_status);
            }
            if($result[0]->userv_vehicle_type =='Four-wheeler'){
              $result[0]->Check='50 point check';  
            }
            if($result[0]->userv_vehicle_type =='Two-wheeler'){
                $result[0]->Check='20 point check';  
            }
            
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "order";
            $output['data'] =  $result;
            $output['orderdetails'] =  $result1;
            $output['order_address'] =  $result4;
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No order found";
            $output['data'] =  $result;
            $output['orderdetails'] =  $result1;
            $output['order_address'] =  $result4;
        }
        
        return response()->json($output);
       
    }
    public function uploadedimages(Request $request){
   
        $uid = $request->header('userid');
         $sql = "SELECT orderID,image1,image2,image3,image4,image5,image6 from tbl_order_images where orderID='$request->orderid'";
      
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Images";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Service found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
    public function categoryquestion(Request $request){
   
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_question_cat where status='Active'";
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Question Category";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Question Category";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function subcategoryquestion(Request $request){
   
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_question_sub where status='Active' and catid='$request->catid'";
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Question Sub Category";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Question Sub Category";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

 public function questionlist(Request $request){
   
        $uid = $request->header('userid');
     
        // $sql = "SELECT tbl_question_cat.cat_id,cat_title,cat_title,selection_type,tbl_question_sub.sub_id,sub_title,queid,question_name from
        // tbl_questions left join tbl_question_sub on subid=sub_id left join tbl_question_cat on
        // tbl_questions.catid=tbl_question_cat.cat_id where tbl_questions.status='Active' ";
        
         $sql = "SELECT DISTINCT tbl_question_cat.cat_id,cat_title,cat_title,selection_type,tbl_question_sub.sub_id,sub_title,queid,question_name, service_type from tbl_questions left join tbl_question_sub on subid=sub_id left join tbl_question_cat on tbl_questions.catid=tbl_question_cat.cat_id where tbl_questions.status='Active' and tbl_question_cat.service_type='$request->service_type'";
       
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            if($request->service_type=='Two-wheeler'){
                $output['Check']='20 Point check';
            }
            if($request->service_type=='Four-wheeler'){
                $output['Check']='50 Point check';
            }
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Questions";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Questions";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

   public function submitquestion(Request $request){
              
        $uid = $request->header('userid');
        $questiondata=$request->question_data;
         $orderid=$request->orderid;
         
         $question=json_encode($questiondata);
         //var_dump($question);  die('sssss');
      
        
        $sql = "INSERT INTO `submit_question`(`puserid`,`orderid`,`question_answer`) VALUES ('$uid','$orderid','$question')";
        $result = app('db')->insert($sql);
        
        
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Question Submitted successfully";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Question Not submitted";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }

    public function odometerreading(Request $request){
   
        $uid = $request->header('userid');
        $sqlupdate1 = "UPDATE `tbl_orders` SET  `out_odomete_reading`= '$request->out_odomete_reading' WHERE order_id='$request->order_ID'";
       $result= app('db')->update($sqlupdate1);
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Odometer reading updated successfully ";
           
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "something went wrong";
        }
        return response()->json($output);
    }
    
    public function showordercount(Request $request){
   
        $uid = $request->header('userid');
        // var_dump(date('m'));die;
         $month=date('m');
        $sql4 = "SELECT puser_name as name FROM tbl_p_users where puser_id='$uid'";
        $results4 = app('db')->select($sql4);
      
        if($request->User_type=='Supplier')
{
    
        $sql1 = "SELECT count(order_id) as countoforder from tbl_orders  where supplier_id='$uid' and supplier_order_status is null";
      
        $result1 = app('db')->select($sql1);

        $sql2 = "SELECT count(order_id) as countoforder from tbl_orders  where supplier_id='$uid' and order_status!='Completed' and supplier_order_status='accept' ";
      
        $result2 = app('db')->select($sql2);

        $sql3 = "SELECT count(order_id) as countoforder from tbl_orders where supplier_id='$uid' and order_status='Completed'";
      
        $result3 = app('db')->select($sql3);
      
        $sql5 = "SELECT sum(order_total_amount) as total from tbl_orders where supplier_id='$uid' and order_status='Completed' and MONTH(order_date) = '$month'";
      
        $result5 = app('db')->select($sql5);

       
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Orders";
        $output['pending'] =  $result1;
        $output['accept'] =  $result2;
        $output['complete'] =  $result3;
        $output['name'] =  $results4;
        if($result5[0]->total==null){
            $output['total'] =  0.0;
        }else{
            $output['total'] =  $result5;
        }

}else if($request->User_type=='Technician'){
        $sql1 = "SELECT count(order_id) as countoforder from tbl_orders where  technician_id='$uid' and technician_accept_status is null";
      
        $result1 = app('db')->select($sql1);

        $sql2 = "SELECT count(order_id) as countoforder from tbl_orders where technician_id='$uid' and order_status!='Completed'  and technician_accept_status='accept' ";
      
        $result2 = app('db')->select($sql2);

        $sql3 = "SELECT count(order_id) as countoforder from tbl_orders where technician_id='$uid' and order_status='Completed'";
      
        $result3 = app('db')->select($sql3);
        
        $sql5 = "SELECT sum(order_total_amount) as total from tbl_orders where technician_id='$uid' and order_status='Completed' and MONTH(order_date) = '$month'";
      
        $result5 = app('db')->select($sql5);
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Orders";
        $output['pending'] =  $result1;
        $output['accept'] =  $result2;
        $output['complete'] =  $result3;
        $output['name'] =  $results4;
        if($result5[0]->total==null){
            $output['total'] =  0.0;
        }else{
            $output['total'] =  $result5;
        }
       

}else{
    $output['status'] = false;
    $output['statuscode'] = 404;
    $output['msg'] =  "No Orders Found";
}
      return response()->json($output);
    }
public function getsupplierspares(Request $request)
{

   $uid = $request->muser_id;
   $sql1 = "SELECT * FROM `tbl_spare_cart` 
    where muser_id='$uid' and userv_id='$request->userv_id'";
    // echo $sql1;die;
    $result1 = app('db')->select($sql1);
      if(!empty($result1))
    {
        foreach($result1 as $result1_rec)
        {
            $spareid[]=$result1_rec->spare_id;
        }
          $spareid=implode(',',$spareid);
        $sql = "SELECT spare_cat_id,p_spare_id,p_spare_mrp,p_spare_gst_percentage,p_spare_selling_price,gst_amount,gstspprice,sp_title,sp_description,sp_hsn_code,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,spcat_title,spcat_description from tbl_p_spares
    left join tbl_spare on tbl_spare.sp_id=tbl_p_spares.spare_id
    left join tbl_spare_catg on tbl_spare_catg.spcat_id=tbl_p_spares.spare_cat_id
     where p_spare_create_by=$request->supplier_id and spare_cat_id=$request->sparecatid and p_spare_status='Active' and p_spare_id not in($spareid)"  ;
    $result = app('db')->select($sql);
    }else{
         $sql = "SELECT spare_cat_id,p_spare_id,p_spare_mrp,p_spare_gst_percentage,p_spare_selling_price,gst_amount,gstspprice,sp_title,sp_description,sp_hsn_code,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,spcat_title,spcat_description from tbl_p_spares
    left join tbl_spare on tbl_spare.sp_id=tbl_p_spares.spare_id
    left join tbl_spare_catg on tbl_spare_catg.spcat_id=tbl_p_spares.spare_cat_id
     where p_spare_create_by=$request->supplier_id and spare_cat_id=$request->sparecatid and p_spare_status='Active'"  ;
    $result = app('db')->select($sql);
    }

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Supplier Spare";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Supplier Spare Found.!";
    }

    return response()->json($output);
}


public function savestandardsparecart(Request $request){
  
    $uid = $request->muser_id;
    $sql = "SELECT * from tbl_spare_cart where userv_id='$request->userv_id' and muser_id='$uid' and spare_id ='$request->spare_id' and supplierid='$request->supplierid'";
// echo $sql;exit;
    $result = app('db')->select($sql);

    if(empty($result)){
        $sql1 = "SELECT distinct concat (supplierid) as supplierid from tbl_spare_cart where muser_id='$uid' and  userv_id='$request->userv_id'";
        $result1 = app('db')->select($sql1);
        if(isset($result1[0]->supplierid)){
            if($result1[0]->supplierid!=$request->supplierid){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Supplier Already Exist!!";
                return response()->json($output);
            }
        }
        $sql2= "SELECT  distinct concat (userv_id) as userv_id  from tbl_spare_cart where muser_id='$uid' and supplierid='$request->supplierid'";
        $result2 = app('db')->select($sql2);
        if(isset($result2[0]->userv_id)){
            if($result2[0]->userv_id!=$request->userv_id){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Vehicle Already Exist!!";
                return response()->json($output);
            }
        }
   $sql = "INSERT INTO `tbl_spare_cart`(`muser_id`,`spare_id`, `userv_id`, `supplierid`, `quantity`) VALUES ('$uid','$request->spare_id','$request->userv_id','$request->supplierid','$request->quantity')";
            $results = app('db')->insert($sql);
        
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Spare added succesfully!!";

    }else{
       $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Something Went Wrong";
    }

    return response()->json($output);
}


public function completeorder(Request $request){
         $sqlupdate1 = "UPDATE `tbl_orders` SET `out_odomete_reading`= '$request->out_odomete_reading', `order_status`='Completed' WHERE order_id='$request->orderid'";
        //dd($sqlupdate1);
   $result= app('db')->update($sqlupdate1);
        if(!empty($result)){
        
        
           $sql1 = "SELECT * from tbl_orders left join tbl_m_users on puser_id=muser_id  where order_id='$request->orderid'";
        
        $result1 = app('db')->select($sql1);
         $deviceid = $result1[0]->m_deviceid;
            $muser_name = $result1[0]->muser_name;
        //  var_dump($deviceid);  die('sssss');
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"Dear $muser_name Order Completed\"},\n\"headings\": {\"en\": \"Dear $muser_name Order Completed\"},\n\"include_player_ids\": [\"$deviceid\"]}");
                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
        
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Order Completed Successfully!!";
                return response()->json($output);
            
        }
     else{
       $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Something Went Wrong";
    }

    return response()->json($output);
}
public function add_partner_review(Request $request){
    $uid = $request->header('userid');

    $sql = "INSERT INTO `tbl_partner_reviews`(`puser_id`, `order_id`, `start_supplier`,`star_technician`, `star_vahanom`, `review_msg`, `star_customer`,`flag`,`created_date`) 
    VALUES ('$uid','$request->order_id','$request->start_supplier','$request->star_technician','$request->star_vahanom','$request->review_msg','$request->star_customer','$request->flag',CURRENT_TIMESTAMP)";

    $results = app('db')->insert($sql);

    if(!empty($results)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Thank you for your feedback";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Oops, Something went wrong!! Please try again";
    }
    return response()->json($output);
}

public function getpartner_review(Request $request){

    $sql = "SELECT * from tbl_partner_reviews";
    $result = app('db')->select($sql);
            if(!empty($result)){
        $output['data'] = $result;
    $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Feedback Details!!";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Feedback not Details";
    }

    return response()->json($output);
}


public function appversion(Request $request)
    {
        //echo "aaaaaaaaaaaa";
        // $sql = "SELECT * FROM tbl_app_version where usertype='$request->updatedtype' and status='Active'";
        // $results = app('db')->select($sql);
        //return $request->usertype;
        $results = DB::table('tbl_app_version')
                      ->where('usertype', $request->usertype)
                      ->where('updatedtype',$request->updatedtype)
                      ->where('status','Active')
                      ->get();
        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "App Version Found!";
            $output['data'] =  $results;
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "This App Version Not Found!";
        }
        return response()->json($output);
    }
 

public function getPartnerReview(Request $request){

        $sql = "SELECT * from tbl_partner_reviews where order_id='$request->orderid'";
        //dd();
        $result = app('db')->select($sql);
                if(!empty($result)){
            $output['data'] = $result;
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Feedback Details!!";
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Feedback not Details";
        }
    
        return response()->json($output);
    }

 public function getSupplierDetail(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_p_users left join tbl_puser_working_hours 
        on tbl_puser_working_hours.puser_id=tbl_p_users.puser_id where tbl_p_users.puser_id='$request->puser_id' and tbl_p_users.puser_type='Supplier'";
        // echo $sql;die;
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Supplier Data";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Supplier Data found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
    public function getTerms(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_terms";
        // echo $sql;die;
        $result = app('db')->select($sql);
       $app_type_user=array();
     
        $app_type_partner=array();
        
        foreach($result as $row){
            
            if($row->app_type=="user"){
                array_push($app_type_user, $row);
            }
            if($row->app_type=="partner"){
                 array_push($app_type_partner, $row);
            }
        }
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Terms and condition Data";
           $output['data']['app_type_user']=$app_type_user;
            $output['data']['app_type_partner']=$app_type_partner;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Terms and condition Data found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
    public function getFaq(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_faq";
        // echo $sql;die;
        $result = app('db')->select($sql);
        
        $app_type_user=array();
     
        $app_type_partner=array();
        
        foreach($result as $row){
            
            if($row->app_type=="user"){
                array_push($app_type_user, $row);
            }
            if($row->app_type=="partner"){
                 array_push($app_type_partner, $row);
            }
        }
        
       // print_r($app_type_user);exit;
      //  $output['data']=new stdClass();
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "FAQ Data";
            $output['data']['app_type_user']=$app_type_user;
            $output['data']['app_type_partner']=$app_type_partner;
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No FAQ Data found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
    public function getLandingPage(Request $request){
        //$uid = $request->header('userid');
         $sql = "SELECT * from tbl_landingText where status='Active'";
        // echo $sql;die;
        $result = app('db')->select($sql);
     $app_type_user=array();
     
        $app_type_partner=array();
        
        foreach($result as $row){
            
            if($row->app_type=="user"){
                array_push($app_type_user, $row);
            }
            if($row->app_type=="partner"){
                 array_push($app_type_partner, $row);
            }
        }
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Landing Page Data";
           $output['data']['app_type_user']=$app_type_user;
            $output['data']['app_type_partner']=$app_type_partner;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Landing Page Data found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
    public function bankDetails(Request $request){
        $uid = $request->header('userid');
         $sql = "SELECT puser_bankacc_number,puser_bankacc_ifsc,bank_holder,upload_blank_check from tbl_p_users where puser_id = '$uid'";
        // echo $sql;die;
        $result = app('db')->select($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Bank Details";
            $output['data'] =  $result;
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Bank Details Data found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
    public function bankDetailsUpdate(Request $request){
        $uid = $request->header('userid');
      //  print_r($request);exit;
          if ($request->hasFile('upload_blank_check')) {
                $original_filename = $request->file('upload_blank_check')->getClientOriginalName();
                $original_filename_arr = explode('.', $original_filename);
                $file_ext = end($original_filename_arr);
                 $destination_path = '../../admin/assets/images/technician';
                $upload_blank_check = 'U-' . rand() . '.' . $file_ext;
                if ($request->file('upload_blank_check')->move($destination_path, $upload_blank_check)) {
                    $upload_blank_check =  $upload_blank_check;
              
                } else {
                    $upload_blank_check=null;
                }
            } else {
                $upload_blank_check=$request->getbankimage;
            }
            
         $sql = "UPDATE `tbl_p_users` SET `puser_bankacc_number`= '$request->puser_bankacc_number',`puser_bankacc_ifsc`= '$request->puser_bankacc_ifsc',`bank_holder`= '$request->bank_holder' ,`upload_blank_check`= '$upload_blank_check' WHERE puser_id = '$uid'";
        // echo $sql;die;
        $result = app('db')->update($sql);
    
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Bank details updated successfully";
        
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Bank Details not updated";
            
        }
        return response()->json($output);
    }


}