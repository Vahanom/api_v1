<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Models\tbl_m_users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Validator;
use DB;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
    public function userlogin(Request $request)
    {
    // echo "hhhh";die;
    $validator = Validator::make($request->all(), [
        'mobilenumber' => 'required|max:10|min:10'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }
        else{

            if($request->user_app_type=='User')
            {

                $usercount = tbl_m_users::where('m_phno', $request->mobilenumber);
                $rndno = rand(1000, 9999);
               
                if($usercount->count() == 0){

                    // $sql = "INSERT INTO `tbl_m_users`( `m_phno`, `muser_otp`,`muser_otp_status`,m_deviceid) VALUES ('$request->mobilenumber','$rndno','0','$request->device_id')";
                    // $results = app('db')->insert($sql);
                    
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] =  "User not found";
                    return response()->json($output);
                }else{
                    tbl_m_users::where('m_phno', $request->mobilenumber)->update(['muser_otp' => $rndno, 'm_deviceid' => $request->device_id,'muser_otp_status' =>0]);
                  
                    // $message = "Your OTP is $rndno";

                    // $apiKey = urlencode('IgUIlrzsV6Y-4GlBuIDDx5BVmGRv87UF3NQQBIBIMQ');
                    // $sender = urlencode('TXTLCL');
                    // $message = rawurlencode($message);
                    // $numbers = $request->mobilenumber;
                
                    // // Prepare data for POST request
                    // $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
                
                    // // Send the POST request with cURL
                    // $ch = curl_init('https://api.textlocal.in/send/');
                    // curl_setopt($ch, CURLOPT_POST, true);
                    // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    // $response = curl_exec($ch);
                    // curl_close($ch);

                    $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['otp'] =  $rndno;
                    $output['msg'] =  "Otp Send Successfully to Your " .$request->mobilenumber." Number .!";
                    return response()->json($output);
                }


            } else{
                $sql = "SELECT * FROM `tbl_p_users` where `puser_phno` = $request->mobilenumber ";
               
                 $rndno = rand(1000, 9999);
                $result = app('db')->select($sql);
                // var_dump($result);die;
                if(empty($result)){
                    // $sql = "INSERT INTO `tbl_p_users`( `puser_phno`, `puser_otp`,`puser_otp_status`,puser_deviceid) VALUES ('$request->mobilenumber','$rndno','0',$request->device_id)";
                    // $results = app('db')->insert($sql);
                    
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] =  "patner not found";
                    return response()->json($output);
                }else{
                    $sql = "UPDATE `tbl_p_users` SET `puser_otp`= '$rndno', `puser_deviceid`= '$request->device_id', `puser_otp_status` = 0 WHERE puser_phno = '$request->mobilenumber'";
                    // echo $sql;die;
                    $results = app('db')->update($sql);
                    $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['otp'] =  $rndno;
                    $output['msg'] =  "Otp Send Successfully to Your " .$request->mobilenumber." Number .!";
                    return response()->json($output);

                }
            }
           
        }
    }

    public function userotpverify(Request $request){
        error_reporting(0);
        if($request->otp=="" || $request->mobilenumber=="" || $request->device_id==""){
            $output['status'] = false;
            $output['statuscode'] = 400;
            $output['msg'] =  "Parameters should not be empty";
            return response()->json($output);
        }else{

            if($request->user_app_type=='User')
            {
            
            $sql = "SELECT * FROM tbl_m_users where muser_otp='$request->otp' and m_phno='$request->mobilenumber'";
            // echo $sql;die;
            $results = app('db')->select($sql);
            if(empty($results)){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Otp Not matched";
                return response()->json($output);
            }else if($results[0]->muser_otp_status==1){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Otp is already used";
                return response()->json($output);
            }else {
                  // use your token method
                  $token = openssl_random_pseudo_bytes(16);
                  $token = bin2hex($token);

                  $sql = "UPDATE `tbl_m_users` SET `m_token`='$token',`muser_otp_status`=1,`m_deviceid`='$request->device_id' WHERE m_phno='$request->mobilenumber'";
                  // echo $sql;die;
                  $result = app('db')->update($sql);
                  if($result>0){
                    $sql = "SELECT * FROM tbl_m_users where muser_otp='$request->otp' and m_phno='$request->mobilenumber'";
                    // echo $sql;die;
                    $results = app('db')->select($sql);

                    if($results[0]->muser_name==null || $results[0]->muser_name==''){
                        $output['profile'] = 'incomplete';
                    }else{
                        $output['profile'] = 'complete';
                    }

                    $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['msg'] =  "Otp verified";
                    $output['data'] =  $results;
                    return response()->json($output);
                  }else{
                    $output['status'] = true;
                    $output['statuscode'] = 404;
                    $output['msg'] =  "Otp Not Verified";
                    return response()->json($output);
                  }
            }
        } else{
            
           $sql = "SELECT * FROM tbl_p_users where puser_otp='$request->otp' and puser_phno='$request->mobilenumber'";
            // echo $sql;die;
            $results = app('db')->select($sql);
            // var_dump($results);die;
            if(empty($results)){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Otp Not matched";
                return response()->json($output);
            }else if($results[0]->puser_otp_status==1){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Otp is already used";
                return response()->json($output);
            }else if($results[0]->puser_account_status=='Inactive'){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Account is inactive";
                return response()->json($output);
            }
                else {
                  // use your token method
                  $token = openssl_random_pseudo_bytes(16);
                  $token = bin2hex($token);

                  $sql = "UPDATE `tbl_p_users` SET `puser_token`='$token',`puser_otp_status`=1,`puser_deviceid`='$request->device_id' WHERE puser_phno='$request->mobilenumber'";
                  $result = app('db')->update($sql);
                  if($result>0){
                    $sql = "SELECT * FROM tbl_p_users where puser_otp='$request->otp' and puser_phno='$request->mobilenumber'";
                    // echo $sql;die;
                    $results = app('db')->select($sql);
if($results[0]->puser_name==null || $results[0]->puser_name==''){
    $output['profile'] = 'incomplete';
}else{
    $output['profile'] = 'complete';
}
                    $output['status'] = true;
                    $output['statuscode'] = 200;
                    $output['msg'] =  "Otp verified";
                    $output['data'] =  $results;
                    return response()->json($output);
                  }else{
                    $output['status'] = true;
                    $output['statuscode'] = 404;
                    $output['msg'] =  "Otp Not Verified";
                    return response()->json($output);
                  }
            }

        }
    }
    }

    public function userresendotp(Request $request){
        if($request->mobilenumber==""){
            $output['status'] = false;
            $output['statuscode'] = 400;
            $output['msg'] =  "Parameters should not be empty";
            return response()->json($output);
        }else{
  
            $rndno = rand(1000, 9999);
            if($request->user_app_type=='User')
            {
          
                tbl_m_users::where('m_phno', $request->mobilenumber)->update(['muser_otp' => $rndno,'muser_otp_status'=>0]);

                $message = "Your OTP is $rndno";

                // $apiKey = urlencode('IgUIlrzsV6Y-4GlBuIDDx5BVmGRv87UF3NQQBIBIMQ');
                // $sender = urlencode('TXTLCL');
                // $message = rawurlencode($message);
                // $numbers = $request->mobilenumber;
            
                // // Prepare data for POST request
                // $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
            
                // // Send the POST request with cURL
                // $ch = curl_init('https://api.textlocal.in/send/');
                // curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // $response = curl_exec($ch);
                // curl_close($ch);

                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['otp'] =  $rndno;
                $output['msg'] =  "Otp Send Successfully to Your " .$request->mobilenumber." Number .!";
                return response()->json($output);
            }else{

                $sql = "UPDATE `tbl_p_users` SET `puser_otp`= '$rndno', `puser_otp_status` = 0 WHERE puser_phno = '$request->mobilenumber'";
                // echo $sql;die;
                $results = app('db')->update($sql);
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['otp'] =  $rndno;
                $output['msg'] =  "Otp Send Successfully to Your " .$request->mobilenumber." Number .!";
                return response()->json($output);
            }
            }
    }


    public function Logout(Request $request){
        $uid = $request->header('userid');
        $token = $request->header('token');
        $sql = "UPDATE `tbl_cust` SET `cust_deviceid`=NULL WHERE cust_id='$uid' and cust_mobtoken='$token'";
        $results = app('db')->update($sql);

        if($results>0){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Logout Successfully.!";
        }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Not Logout.!";
            }
        return response()->json($output);
    }

    public function userregistration(Request $request){

        $validator = Validator::make($request->all(), [
            'ph_no' => 'required|max:10|min:10',
            'name' => 'required',
            'email' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 200);
            }
            else{
        $ph_no = $request->ph_no;
        $name = $request->name;
        $email = $request->email;
        //$postal_code = $request->postal_code;
        $address=$request->address;
        $found=$request->found;
        $date_of_birth=$request->date_of_birth;
        // print_r($date_of_birth);exit;
        $date_of_birth = explode('T',$date_of_birth);
        $date_of_birth=$date_of_birth[0];
        // print_r($date_of_birth);exit;
        $gender=$request->gender;
        
        $sql1 = "SELECT * FROM tbl_m_users where muser_email='$request->email' or m_phno='$request->ph_no'";
        $results1 = app('db')->select($sql1);

        if(empty($results1)){
        $sql = "INSERT INTO `tbl_m_users`(`muser_name`, `muser_email`, `muser_address`, `found`, `m_phno`, `muser_createdate`,`muser_createby`,`date_of_birth`,`gender`) VALUES ('$name','$email','$address','$found','$ph_no',CURRENT_TIMESTAMP,'user app','$date_of_birth','$gender')";
        $results = app('db')->insert($sql);
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "User Created Successfully.!";
        }else{
        $output['status'] = false;
        $output['statuscode'] = 200;
        $output['msg'] =  "User already Created.!";
            }
        return response()->json($output);
    }
}

}
