<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;
use DateTime;
class APIController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    public function vehiclereg(Request $request){
        $validator = Validator::make($request->all(), [
            'vehicle_reg_no' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 200);
            }
            else{
            $uid = $request->header('userid');
            $sql = "SELECT * from tbl_user_vehicles where vehicle_reg_no='$request->vehicle_reg_no' and user_id='$request->userid'";
            // echo $sql;die;
            $result = app('db')->select($sql);
       
            if(!empty($result)){
                $output['status'] = false;
                $output['statuscode'] = 200;
                $output['msg'] =  "Vehicle Already Created.!";
                $output['data'] =  $result;

            }else{
                $sql = "INSERT INTO `tbl_user_vehicles`(`vehicle_reg_no`, `userv_vehicle_type`,`user_id`, `userv_createddate`, `userv_createdby`,`userv_fueltype`, `userv_manuf_year`,`vehicle_make`, `vehicle_model`, `vehicle_trim`,`vehb_id`,`userv_vehicle_status`) VALUES ('$request->vehicle_reg_no','$request->vehicle_type','$request->userid',CURRENT_TIMESTAMP,'user app','$request->fuel','$request->year','$request->make','$request->model','$request->trim','$request->vehb_id','Active')";
                $results = app('db')->insert($sql);
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Vehicle Created successfully.!";
            }

            return response()->json($output);
        }
    }

    public function banner(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT ban_title,ban_img,ban_thumb_img from tbl_banners where ban_status='Active' and ban_type='Banner'";
        // echo $sql;die;
        $result = app('db')->select($sql);
   
        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Banner";
            $output['data'] =  $result;

        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No banner found";
            $output['data'] =  $result;
        }
        return response()->json($output);
}

public function popup(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT ban_title,ban_img,ban_thumb_img from tbl_banners where ban_status='Active' and ban_type='Pop-up'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Pop-up";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Pop-up banner found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function terms(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT term_description from tbl_terms";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Terms";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Terms found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function getusernotification(Request $request){
    $uid = $request->header('userid');
    $month=date("Y-m-d h:i:s",strtotime("-7 day"));
    $sql = "SELECT nt.*,m.muser_name as user_name,m.muser_email as user_email,m.m_phno as user_ph,p.puser_name from tbl_noti_store as nt left join tbl_m_users as m on m.muser_id=nt.user_id left JOIN tbl_p_users as p on p.puser_id=nt.partner_id left join tbl_orders on tbl_orders.order_id=nt.order_id where tbl_orders.order_status != 'Completed' and user_id=$uid and create_at>='$month' group by order_id order by nt.id desc";
    // echo $sql;die;
    $result = app('db')->select($sql);
     $count = count($result);
   
    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['cnt'] = $count;
        $output['msg'] =  "Notifications List";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Notification Found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function aboutus(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT abt_description from tbl_aboutus";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "About Us";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No About Us found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function support(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT support_phno,support_email from tbl_support";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Support";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Suppport found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function insuarancelist(Request $request){
   
    
    $sql = "SELECT * from tbl_insurance where inusr_status='Active' and vehicle_type='$request->vehicle_type'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Insurance";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Insurance found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function insuaranceget(Request $request){
   
    $sql = "SELECT * from tbl_insurance where inusr_id='$request->inusr_id'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Insurance";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Insurance found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}


public function insuaranceinquiry(Request $request){
    $uid = $request->header('userid');
    $validator = Validator::make($request->all(), [
        
        'inusr_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }
        else{
        //   $deviceid= "37b54ca1-eb21-4a68-87aa-3228dd5f3f60";
          $deviceid= $request->header('deviceid');
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"Thank you for your inquiry our team will get back to you shortly\"},\n\"headings\": {\"en\": \"Insurance Inquiry\"},\n\"include_player_ids\": [\"$deviceid\"]}");

                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
      if($request->payment_status == "success"){
        $payment_status = $request->payment_status;
            
        }else{
        $payment_status = "failed";
            
        }
        $amount= $request->total;
        if($request->payment_id !=""||!empty($request->payment_id) || $request->payment_id !=null || $request->payment_id !="undefined"){
        $payment_id = $request->payment_id;
            
        }else{
            $payment_id = "";
        
        }
        $order_middle = $request->order_middle;
        
            
            $sql = "INSERT INTO `tbl_user_insurance`(`insurance_id`, `user_m_id`,payment_status,amount,payment_id,order_middle, `createdbydate`,`status`) VALUES ('$request->inusr_id','$uid','$payment_status','$amount','$payment_id','$order_middle',CURRENT_TIMESTAMP,'Active')";
            $results = app('db')->insert($sql);
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Thank you for inquiry.!";
        }
        return response()->json($output);
}

public function userget(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT * from tbl_m_users where muser_id='$uid'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "User";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No User found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function userupdate(Request $request){
 
$date_of_birth=$request->date_of_birth;
        // print_r($date_of_birth);exit;
        $date_of_birth = explode('T',$date_of_birth);
        $date_of_birth=$date_of_birth[0];
        
    $uid = $request->header('userid');
 try { 
        $sql = "UPDATE `tbl_m_users` SET  `muser_address`= '$request->address',  `muser_name`= '$request->name',`date_of_birth`= '$date_of_birth',`gender`= '$request->gender'  WHERE muser_id = '$uid'";
    $results = app('db')->update($sql);

          // Closures include ->first(), ->get(), ->pluck(), etc.
      } catch(\Illuminate\Database\QueryException $ex){ 
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Phone no or email already taken.!";
        return response()->json($output);
      }

    if($results>0){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Profile Updated.!";
    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Profile Not Updated.!";
    }
    return response()->json($output);

}


public function profileimageupdate(Request $request){
 

    $uid = $request->header('userid');
    if ($request->hasFile('image')) {
        $original_filename = $request->file('image')->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
        $destination_path = '../../admin/assets/images/user';
        $image = 'U-' . rand() . '.' . $file_ext;

        if ($request->file('image')->move($destination_path, $image)) {
            $image =  $image;
               $sql = "UPDATE `tbl_m_users` SET  `profileimage`= '$image'   WHERE muser_id = '$uid'";
               
    $results = app('db')->update($sql);

   
      
        } else {
            $image=null;
              $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Profile image Not Updated.!";
          return response()->json($output);
        }
    } else {
        $image=null;
           $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Profile image Not Updated.!";
          return response()->json($output);
         
    }
  
   

    if($results>0){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Profile image Updated.!";
    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Profile image Not Updated.!";
    }
    return response()->json($output);

}

public function pollutionlist(Request $request){
   
    
    $sql = "SELECT * from tbl_pollutions where pol_status='Active'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Pollution";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Pollution found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function pollutioneget(Request $request){
   
    $sql = "SELECT * from tbl_pollutions where pol_id='$request->pol_id'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Pollution";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Pollution found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}


public function pollutionsinquiry(Request $request){
    $uid = $request->header('userid');
    $validator = Validator::make($request->all(), [
        
        'pol_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        }
        else{
            if($request->payment_status == "success"){
        $payment_status = $request->payment_status;
            
        }else{
        $payment_status = "failed";
            
        }
        $amount= $request->total;
        if($request->payment_id !=""||!empty($request->payment_id) || $request->payment_id !=null || $request->payment_id !="undefined"){
        $payment_id = $request->payment_id;
            
        }else{
            $payment_id = "";
        
        }
        $order_middle = $request->order_middle;
      
            $sql = "INSERT INTO `tbl_user_pollution`(`pollution_id`, `muser_id`,payment_status,amount,payment_id,order_middle, status, `createdbydate`) VALUES ('$request->pol_id','$uid','$payment_status','$amount','$payment_id','$order_middle','Active',CURRENT_TIMESTAMP)";
            
                  $deviceid= "37b54ca1-eb21-4a68-87aa-3228dd5f3f60";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"Thank you for your inquiry our team will get back to you shortly\"},\n\"headings\": {\"en\": \"Pollution Inquiry\"},\n\"include_player_ids\": [\"$deviceid\"]}");

                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
            
            $results = app('db')->insert($sql);
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Thank you for inquiry.!";
        }
        return response()->json($output);
}

public function orderlist(Request $request){
    $uid = $request->header('userid');
    if($request->order_status =='Ongoing'){
        
    $sql = "SELECT *, (case when (type_servicespare = 'Service') 
 THEN
      'Quick' 
 ELSE
      'Standard'
 END)
 as type from tbl_orders left join tbl_user_vehicles on uservehicleid=userv_id where puser_id='$uid' and order_status !='Completed' order by tbl_orders.order_id desc";
    }else{
        
    $sql = "SELECT *, (case when (type_servicespare = 'Service') 
 THEN
      'Quick' 
 ELSE
      'Standard'
 END)
 as type from tbl_orders left join tbl_user_vehicles on uservehicleid=userv_id  where puser_id='$uid' and order_status='Completed' order by tbl_orders.order_id desc";
    }
    // echo $sql;die;
    $result = app('db')->select($sql);
    
     $i=0;
        foreach($result as $row){
        if($result[$i]->order_schedule_date=="0000-00-00"){
            $result[$i]->order_schedule_date = '';
            
        }   
        $i++;
        }

    if(!empty($result)){
        
        $i=0;
        foreach($result as $row){
        
        if($row->userv_vehicle_type=='Four-wheeler'){
           $result[$i]->check='50 point check';
       }
        
       if($row->userv_vehicle_type=='Two-wheeler'){
           $result[$i]->check='20 point check';
           
       }
       $i++;
        }
       
        
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] = "Orders";
        $output['data'] = $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Orders found";
       // $output['data'] =  $result;
    }
    return response()->json($output);
}

public function orderinfo(Request $request){
    
    
      $uid = $request->header('userid');
        $sql = "SELECT *, (case when (type_servicespare = 'Service') 
 THEN
      'Quick' 
 ELSE
      'Standard'
 END)
 as type from tbl_orders left join tbl_m_users on puser_id=muser_id left join tbl_user_vehicles on uservehicleid=userv_id  where order_id='$request->orderid'";
        
        $result = app('db')->select($sql);
        
        $sql2 = "SELECT puser_name,puser_address from tbl_orders left join tbl_p_users on tbl_p_users.puser_id=tbl_orders.supplier_id  where order_id='$request->orderid'";
        
        $result2 = app('db')->select($sql2);
        
         $sql3 = "SELECT puser_name,puser_address from tbl_orders left join tbl_p_users on tbl_p_users.puser_id=tbl_orders.technician_id  where order_id='$request->orderid'";
        
        $result3 = app('db')->select($sql3);
        if($result[0]->type_servicespare=='Service'){
            $sql1 = "SELECT * from tbl_orders left join tbl_orderdetails on od_order_id=order_id left join tbl_service on ser_id=od_service_id  where order_id='$request->orderid'";
       
        
        }elseif($result[0]->type_servicespare=='Inspection' || $result[0]->type_servicespare=='Prepurchase'){
            $sql1 = "SELECT * from tbl_orders left join tbl_orderdetails on od_order_id=order_id left join tbl_inspections on tbl_inspections.id=od_service_id  where order_id='$request->orderid'";
            
        }
        else{
            $sql1 = "SELECT * from tbl_orders left join tbl_orderdetails on od_order_id=order_id left join tbl_p_spares on p_spare_id=od_service_id left join tbl_spare on spare_id=sp_id where order_id='$request->orderid'";

        }
        
       
        $result1 = app('db')->select($sql1);
         $sql4 = "SELECT * from tbl_maddress where addressid=".$result[0]->order_address;
        $result4 = app('db')->select($sql4);
      
    if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "order";
            $output['data'] =  $result;
            $output['orderdetails'] =  $result1;
            $output['supplier'] =  $result2;
            $output['technician'] =  $result3;
                $output['order_address'] =  $result4;

        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No order found";
            $output['data'] =  $result;
             $output['supplier'] =  $result2;
            $output['technician'] =  $result1;
            $output['orderdetails'] =  $result1;
                        $output['order_address'] =  $result4;

        }
        return response()->json($output);
}

public function allsupplier(Request $request){
    $uid = $request->header('userid');
    $address_id = $request->address_id;
      $vehb_id = $request->vehb_id;
   $des = 0;
   $src=0;
    $sqladd = "SELECT lattitude,longitude from tbl_maddress where addressid= $address_id";
    $resultadd = app('db')->select($sqladd);
    if($resultadd){
        $src=$resultadd[0]->lattitude;
        $des=$resultadd[0]->longitude;
    }
   app('db')->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");   
            
   $sql1 = "SELECT p_spare_create_by,spare_cat_id,p_spare_id,p_spare_mrp,p_spare_gst_percentage,p_spare_selling_price,gst_amount,gstspprice,sp_title,sp_description,sp_hsn_code,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,spcat_title,spcat_description from tbl_p_spares
    left join tbl_spare on tbl_spare.sp_id=tbl_p_spares.spare_id
    left join tbl_spare_catg on tbl_spare_catg.spcat_id=tbl_p_spares.spare_cat_id
     where p_spare_status='Active' and find_in_set('$vehb_id',tbl_spare.veh_id) <> 0 group by p_spare_create_by";
    //   echo $sql1;
    $result1 = app('db')->select($sql1);
 if(!empty($result1)){
      foreach($result1 as $result1_rec)
    {
        $spareid[]=$result1_rec->p_spare_create_by;
    }
 
  //$spareid=implode(',',$spareid);

 
//     $sql = "SELECT puserw_mo_open,puserw_mo_close,puserw_tu_open,puserw_tu_close,puserw_we_open,puserw_we_close,puserw_th_open,puserw_th_close,puserw_fr_open,puserw_fr_close,puserw_sa_open,puserw_sa_close,puserw_su_open,puserw_su_close,tbl_p_users.puser_id,puser_name FROM `tbl_p_users` 
// 	left join tbl_puser_working_hours on tbl_p_users.puser_id=tbl_puser_working_hours.puser_id
// 	where tbl_p_users.puser_type='Supplier'and tbl_p_users.puser_id in($spareid)"  ;

    $i=0;
    $dataarray=array();
    // print_r($spareid);exit;
    foreach($spareid as $row){
         $sql = "SELECT puser_profile_image,puser_lat_long as src1, puser_long as des1, puserw_mo_open,puserw_mo_close,puserw_tu_open,puserw_tu_close,puserw_we_open,puserw_we_close,puserw_th_open,puserw_th_close,puserw_fr_open,puserw_fr_close,puserw_sa_open,puserw_sa_close,puserw_su_open,puserw_su_close,tbl_p_users.puser_id,puser_name,expertise FROM `tbl_p_users` 
    	left join tbl_puser_working_hours on tbl_p_users.puser_id=tbl_puser_working_hours.puser_id
    	where tbl_p_users.puser_type='Supplier'and tbl_p_users.puser_id =$row";
    	 $result = app('db')->select($sql);
    // 	echo $sql;
    	// print_r($result);exit;
        if($result){
    	    $src1=$result[0]->src1;
    	    $des1=$result[0]->des1;
            $unit='K';
            if(!empty($des1)){
                    $theta = $des - $des1;
                     $dist = sin(deg2rad($src)) * sin(deg2rad($src1)) +  cos(deg2rad($src)) * cos(deg2rad($src1)) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    $unit = strtoupper($unit);
                
                    if ($unit == "K") {
                        $miles = round($miles * 1.609344,4);
                        $miles = $miles*1000;
                    }
            }
            else{
                  $miles =  -1;
              
            }
              
            if($miles>-1 && $miles<10000 ){
                $res[$i] = $result[0];
                $res[$i]->distance = $miles;
                $res[$i]->puser_name = $res[$i]->puser_name.'  (Expertise: '.$res[$i]->expertise.')';
                $sqlstar = "SELECT sum(star_supplier)/count(*) as stars from tbl_orders join tbl_cust_reviews on tbl_orders.order_id=tbl_cust_reviews.order_id where supplier_id='$row'";
                $resultstar = app('db')->select($sqlstar);
                if($resultstar){
                    $res[$i]->rating = $resultstar[0]->stars;
                }    
                
            }
            // echo $miles."/";
    //     $theta = $lon1 - $lon2;
    // $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
    // $miles = acos($miles);
    // $miles = rad2deg($miles);
    // $miles = $miles * 60 * 1.1515;
    // $feet = $miles * 5280;
    // $yards = $feet / 3;
    // $kilometers = $miles * 1.609344;
    // $meters = $kilometers * 1000;
    // return compact('miles','feet','yards','kilometers','meters'); 
            
    	}
    	$i++;
// 		$distance= distance($sec, $des, $src1, $des1, $unit);
// 		 print_r($distance);
    }
    
// exit;

  }else{
       $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Supplier found";
         return response()->json($output);
  }
  

    if(!empty($res)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Supplier";
        $output['data'] =  array_values($res);

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Supplier Near you!!!!!";
        // $output['data'] =  $result;
    }
    return response()->json($output);
}
public function alltechnician(Request $request){
    $uid = $request->header('userid');
    
	$sql = "SELECT puser_phno,puser_pincode,puser_name,puser_account_status,puser_id FROM `tbl_p_users` where `puser_type`='Technician'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Technician";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Technician found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}
public function getmake(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT distinct(vehb_make) from tbl_vehicle where vehb_status='Active' and vehb_type='$request->vehb_type'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "make";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No make found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function getmodel(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT distinct(vehb_model) from tbl_vehicle where vehb_make='$request->make' and vehb_status='Active'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "model";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No model found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function gettrim(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT vehb_id,vehb_trim from tbl_vehicle where vehb_make='$request->make' and vehb_model='$request->model' and vehb_status='Active'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "trim";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No trim found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function alluserservice(Request $request){
   
    $uid = $request->header('userid');
    $sql1 = "SELECT * from tbl_quick_cart where  muser_id='$uid' and userv_id='$request->uservehicle' ";
    $result1 = app('db')->select($sql1);


if(!empty($result1)){
    $abcd=$result1[0]->ser_id;
    $sql = "SELECT * from tbl_service where ser_status='Active' and service_vehicle_type='$request->userv_vehicle_type' and ser_id not in($abcd)";
}else{
    
    $sql = "SELECT * from tbl_service where ser_status='Active' and service_vehicle_type='$request->userv_vehicle_type'";
}
    $result = app('db')->select($sql);
    if($request->userv_vehicle_type == "Two-wheeler"){
    $points='Aditional 20 point check';
        
    }else{
     $points='Aditional 50 point check';
       
    }
     
    $pointcheck = array(
        'point_check' => $points
    );
    
if(!empty($result)){
        foreach($result as $result_rec){
            $catid[]=$result_rec->ser_category;
        }
        $catid=implode(',',$catid);
        $sql1 = "SELECT * from tbl_service_cat where ser_catid in($catid) ";
        $result1 = app('db')->select($sql1);
       
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Service";
        $output['service'] =  $result;
        $output['category'] = $result1;
          $output['point_check'] =  $pointcheck;

    }else{
        $result1=Array();
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Service found";
        $output['service'] =  $result;
        $output['category'] = $result1;
    }
    return response()->json($output);
}
public function uservehicle(Request $request){
  
        $uid = $request->header('userid');
        $sql = "SELECT vehb_make,vehb_model,vehb_type,vehb_make,vehb_trim,userv_fueltype,vehb_image,userv_manuf_year,userv_milage,vehicle_reg_no,user_id,userv_id,tbl_user_vehicles.vehb_id from tbl_user_vehicles left join tbl_vehicle on tbl_user_vehicles.vehb_id=tbl_vehicle.vehb_id
		where user_id='$uid' and status !=1 ";
	//	print_r($sql);exit;
        $result = app('db')->select($sql);
   
        if(!empty($result)){
           $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Vehicle List";
            $output['data'] =  $result;

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Vehicle Found.!";
        }

        return response()->json($output);
    }

    public function deletevehicle(Request $request){
        $uid = $request->header('userid');
     
        //$sql = "delete from tbl_user_vehicles where userv_id='$request->userv_id'";
          $sql = "UPDATE `tbl_user_vehicles` SET `status`= 1 WHERE userv_id = '$request->userv_id'";
         
        $results = app('db')->update($sql);
        if(!empty($results)){
            
            $sql1 = "delete from tbl_quick_cart where userv_id='$request->userv_id' and muser_id='$uid' ";
            $results1 = app('db')->delete($sql1);
            
              $sql2 = "delete from tbl_spare_cart where muser_id='$uid'";
    $results2 = app('db')->delete($sql2);
            
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Vehicle data deleted successfully";
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] = "Something went wrong!!";
        }
        return response()->json($output);
    }

    public function quickservicecart(Request $request){
  
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_quick_cart where userv_id='$request->userv_id' and muser_id='$uid' and find_in_set('$request->ser_id',ser_id) <> 0";
        $result = app('db')->select($sql);
   
        if(empty($result)){

        

            $sql = "SELECT * from tbl_quick_cart where userv_id='$request->userv_id' and muser_id='$uid' ";
            $result = app('db')->select($sql);
            
            if(!empty($result)){

$arrayofservice=explode(',',$result[0]->ser_id);
$arrayofservice[]=$request->ser_id;

$stringofservice=implode(',',$arrayofservice);
$sql = "UPDATE `tbl_quick_cart` SET `ser_id`= '$stringofservice' WHERE userv_id = '$request->userv_id' and muser_id='$uid' ";
$results = app('db')->update($sql);
            }else{
                
                  $sql1 = "SELECT distinct concat (userv_id) as userv_id from tbl_quick_cart where muser_id='$uid' ";
            $result1 = app('db')->select($sql1);
               if(isset($result1[0]->userv_id)){
            if($result1[0]->userv_id!=$request->userv_id){
                
                  $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "services already present in cart with different vehicle";
                return response()->json($output);
            }
                   
               }else{
                
                $sql = "INSERT INTO `tbl_quick_cart`(`muser_id`,`ser_id`, `userv_id`, `status`) VALUES ('$uid','$request->ser_id','$request->userv_id','Active')";
                $results = app('db')->insert($sql);
                   $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Service added successfully!!";
               }
            }
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Service added successfully!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Something Went Wrong";
        }

        return response()->json($output);
    }

    public function applycoupon(Request $request){
        $uid = $request->header('userid');
        if($request->couponcode=="" || $request->subtotal1==""){
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] = "Parameters Should Not Be Empty.!";
        }else{
            $date = date('Y-m-d');
            $sql = "SELECT * FROM `tbl_coupon` where cpn_code='$request->couponcode' and cpn_status='Active' and cpn_enddate>$date";
            $result = app('db')->select($sql);
            // print_r($result);exit;
            if(!empty($result)){
                $cpn_maxusers = $result[0]->cpn_max_users;
                $cpn_maxperusers = $result[0]->cpn_maxperusers;
                $cpn_billminamount = $result[0]->cpn_min_bill_amt;
                $cpn_enddate = $result[0]->cpn_enddate;
                $cpn_maxdiscount = $result[0]->cpn_flat_amount;
                $cpn_type = $result[0]->cpn_type;

                $sql = "SELECT count(order_coupon_code) as couponusedcount,(select count(order_coupon_code) as usedperuser from tbl_orders where order_coupon_code='$request->couponcode' and puser_id='$uid') as couponusedperuser FROM `tbl_orders` where order_coupon_code='$request->couponcode'";
               
                $result = app('db')->select($sql);
                if($result[0]->couponusedcount>=$cpn_maxusers || $result[0]->couponusedperuser>=$cpn_maxperusers){
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] = "Coupon Usage Limit Exceeds.!";
                }else if(date('Y-m-d')>$cpn_enddate){
                    $output['status'] = false;
                    $output['statuscode'] = 404;
                    $output['msg'] = "Coupon Expired.!";
                 }else{
                 if($cpn_type=='Flat'){
                    //  echo "here";die;
                        $coupon_discount = $cpn_maxdiscount;
                     }else{
                        $coupon_discount = ($request->subtotal1/100)*$cpn_maxdiscount;
                     } 
                     if($request->subtotal1 >= $cpn_billminamount && $request->subtotal1 >= $coupon_discount){
                        $output['status'] = true;
                        $output['statuscode'] = 200;
                        $output['msg'] = "Coupon Applied.!";
                        $output['data'] = ["Subtotal1"=>(int)$request->subtotal1,"Subtoatl2"=>(int)$request->subtotal1-$coupon_discount,"Discount"=>(int)$coupon_discount];
                      }else{
                        $output['status'] = false;
                        $output['statuscode'] = 404;
                        $output['msg'] = "Coupon Not Applicable 1.!";
                      }
                 } 
                } else{
                        $output['status'] = false;
                        $output['statuscode'] = 404;
                        $output['msg'] = "Coupon Not Applicable 2.!";
                      }
                
                
            }
            return response()->json($output);
        }
// public function placeorder1(Request $request)
// {
              
//     $uid = $request->header('userid');
//     $delivery_type = $request->delivery_type;
//     $address = $request->address;
//     $pincode = $request->pincode;
//     $order_coupon_code = $request->order_coupon_code;
//     $order_subtotal1 = $request->Subtotal;
//     $order_coupon_amt = $request->order_coupon_amt;
//     $order_total = $request->total;
//     $order_date = date('Y-m-d');
//     $order_schedule_date = $request->order_schedule_date;
//     $service = $request->service;
//     $order_coupon_discount = $request->order_coupon_discount;
//     $spare = $request->spare;
//     $order_type = $request->ordertype;
//     $technician_id = $request->technician_id;
//     $note = $request->note;
//     $voicemsg = $request->voicemsg;

//     $sql = "INSERT INTO `tbl_orders`( `puser_id`, `order_type`, `order_status`,`payment_status`, `order_address`, `order_pincode`, `order_coupon_code`, `order_subtotal_amount`, `order_coupon_amt`, `order_total_amount`, `order_date`, `order_schedule_date`,`type_servicespare`,`order_coupon_discount`, `technician_id`,`note`,`voicemsg`) VALUES ('$uid','$delivery_type','Pending','Pending','$address','$pincode','$order_coupon_code','$order_subtotal1','$order_coupon_amt','$order_total','$order_date','$order_schedule_date','$order_type','$order_coupon_discount','$technician_id','$note','$voicemsg')";
//     app('db')->insert($sql);
//     $lastinsertid = DB::getPdo()->lastInsertId();

//     if($lastinsertid>0)
//     {

//         $sqlupdate1 = "UPDATE `tbl_orders` SET `order_trackingnumber`='VHS$lastinsertid' WHERE order_id='$lastinsertid'";
//         app('db')->update($sqlupdate1);
//         if($request->service!=null)
//         {
//             foreach($service as $row)
//             {

//                 $od_service_id = $row['serviceid'];
//                 $od_service_price = $row['serviceprice'];
//                 $sql = "INSERT INTO `tbl_orderdetails`(`od_order_id`,`od_service_id`,`od_service_price`) VALUES ('$lastinsertid','$od_service_id','$od_service_price')";
//                 app('db')->insert($sql);
//             }
//         }
//         if($request->service!=null)
//         {
//             $sql = "DELETE FROM `tbl_quick_cart` WHERE muser_id='$uid'";
//             app('db')->delete($sql);
//         }
//         if($request->spare!=null)
//         {
//             foreach($spare as $row)
//             {
//                 $od_service_id = $row['spareid'];
//                 $od_service_price = $row['spareprice'];
//                  $sql = "INSERT INTO `tbl_orderdetails`(`od_order_id`,`od_service_id`,`od_service_price`) VALUES ('$lastinsertid','$od_service_id','$od_service_price')";
//                 app('db')->insert($sql);
//             }
//         }
//         if($request->spare!=null){
//             $sql = "DELETE FROM `tbl_spare_cart` WHERE muser_id='$uid'";
//             app('db')->delete($sql);
//         }
//         $date = date('d-m-Y');
//         // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_URL, 'https://api.sendgrid.com/v3/mail/send');
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//         curl_setopt($ch, CURLOPT_POST, 1);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"personalizations\": [{\"to\": [{\"email\": \"$request->email\"}]}],\"from\": {\"email\": \"no-reply@vahanom.in\"},\"subject\":\"Great news! Order Placed successfully!\",\"content\": [{\"type\": \"text/html\",\"value\": \"Dear $request->custname,<br><p>We've received your order, You'll find a summary of your purchase  below<br> Order Id- VH$lastinsertid<br>Order Date- $date <br>Order Type-$request->delivery_type, <br> We will send you the detail invoice once the order is confirmed.<br>Thanks for shopping with Uneed !\"}]}");
//         $headers = array();
//         $headers[] = 'Authorization: Bearer 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';
//         $headers[] = 'Content-Type: application/json';
//         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//         if (curl_errno($ch)){
//             echo 'Error:' . curl_error($ch);
//         }
//         curl_close($ch);
//         $sql = "SELECT * FROM `tbl_m_users` where muser_id='$uid'";
//         $result = app('db')->select($sql);
//         $deviceid = $result[0]->m_deviceid;
//         // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//         curl_setopt($ch, CURLOPT_POST, 1);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"d162a461-e9cc-450e-b66e-d9fe8b817aec\",\n\"contents\": {\"en\": \"Order Placed Succesfully\"},\n\"headings\": {\"en\": \"Order placed\"},\n\"include_player_ids\": [\"$deviceid\"]}");
//         $headers = array();
//         $headers[] = 'Content-Type: application/json; charset=utf-8';
//         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//         $result = curl_exec($ch);
//         if (curl_errno($ch)) 
//         {
//             echo 'Error:' . curl_error($ch);
//         }
//         curl_close($ch);
//         $output['status'] = true;
//         $output['statuscode'] = 200;
//         $output['data'] = ['Orderid'=>'VH'.$lastinsertid,"Amount"=>$order_total];
//         $output['msg'] = "Order Placed.!";

//     }else{
//         $output['status'] = false;
//         $output['statuscode'] = 404;
//         $output['msg'] = "Order Not Placed.!";
//     }
//     return response()->json($output);
// }
public function placeorder(Request $request){
    if ($request->hasFile('voice')) 
    {
        $original_filename = $request->file('voice')->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
         $destination_path = './uploads/voice';
        $image = 'U-' . rand() . '.' . $file_ext;
        if ($request->file('voice')->move($destination_path, $image)) 
        {
            $image =  $image;  
        } else 
        {
            $image=null;
        }
    } else 
    {
        $image=null;
    }
    
    $uid = $request->userid;
    $supplier_id = $request->supplier_id;
    $order_total = $request->total;
    //=========================================if order id present===========
    if(isset($request->orderid))
    {
        if($request->spare!=null)
        {
            $orderdetails = "SELECT * from tbl_orderdetails where od_order_id='$request->orderid' order By order_detail_id desc limit 1";
            $orderdetailsresult = app('db')->select($orderdetails);
            $couponstatus= $orderdetailsresult[0]->statuscoupon+1;
            $spare = $request->spare;
            foreach($spare as $row)
            {
                $od_service_price = $row['p_spare_selling_price'] * $row['quantity'];
                $quantity = $row['quantity'];
                $p_spare_id =$row['p_spare_id'];
                $sql = "INSERT INTO `tbl_orderdetails`(`od_order_id`,`od_service_id`,`od_service_price`,`quantity`,`statuscoupon`) VALUES ('$request->orderid','$p_spare_id','$od_service_price','$quantity','$couponstatus')";
                app('db')->insert($sql);
              //  dd($sql);
            }
        }
        
        if($request->spare!=null)
        {
            $sql = "DELETE FROM `tbl_spare_cart` WHERE muser_id='$uid'";
            app('db')->delete($sql);
            $date =  date('y-m-d h:i:s');
            $sql1 = "SELECT * FROM tbl_p_users where puser_id =$supplier_id ";
                $results1 = app('db')->select($sql1);
                // dd($results1);
                if(!empty($results1))
                {
                     $lastinsertid = $request->orderid;
                    if($lastinsertid>0){
                    foreach($results1 as $row1)
                    {
                        $puser_id = $row1->puser_id;
                        $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$lastinsertid','$puser_id')";
                        app('db')->insert($sql);
                        // echo $sql;
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"3bebbed7-d995-4f6d-90de-89ca233b2b3c\",\n\"contents\": {\"en\": \"Please Check Details!..\"},\n\"headings\": {\"en\": \"You Got Order \"},\n\"include_player_ids\": [\"$row1->puser_deviceid\"]}");
                        $headers = array();
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));
                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        curl_close($ch);
                    }
                }
                }
            $sql="INSERT INTO `tbl_noti_store`(`title`, `description`, `user_id`, `partner_id`, `create_at`, `send_by`,order_id) VALUES ('Replaced Order VHS$request->orderid !!!!','New spare Added in $request->orderid ...','$uid','$supplier_id','$date','user','$lastinsertid')";
            app('db')->insert($sql);
        }
        
        $date = date('d-m-Y');
        $sql4 = "DELETE FROM `tbl_order_images` WHERE orderID='$request->orderid'";
        app('db')->delete($sql4);
        $payment_id =$request->old_payment_id.','.$request->payment_id;
        $labour_rate=($request->old_labour_rate+$request->labour_rate);
        $sqlupdate1 = "UPDATE `tbl_orders` SET `order_total_amount`='$request->grand_total', payment_id='$payment_id',labour_rate='$labour_rate' ,technician_otp='',spare_dispatch=null,technician_otp_status='',order_status='Pending' WHERE order_id='$request->orderid'";
        app('db')->update($sqlupdate1);
            
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['data'] = ['Orderid'=>'VHS'.$request->orderid,"Amount"=>$order_total];
        $output['msg'] = "Order Placed.!";
    }
    //======================================================if order id missing===================
    else
    {
        //print_r($request);die;
        $delivery_type = $request->delivery_type;
        $address = $request->address_id;
        $pincode = $request->pincode;
        $order_coupon_code = $request->order_coupon_code;
        $order_subtotal1 = $request->Subtotal;
        $order_coupon_amt = $request->order_coupon_amt;
        $order_date = date('Y-m-d');
        $sch_time=$request->order_schedule_time;
        $order_discription=$request->order_description;
        // $sch_time=date("h:i a",$time);
         $order_schedule_date = strtotime($request->order_schedule_date);
        if(!empty($order_schedule_date)){
            $order_schedule_date=date('Y-m-d',$order_schedule_date);
            //  print_r($order_schedule_date);exit;
        }else{
           $order_schedule_date = '0000-00-00';
            //   print_r($order_schedule_date);exit;
        }
        $service = $request->service;
        $Inspection = $request->Inspection;
       // echo $Inspection;
        $Prepurchase = $request->Prepurchase;
        $order_coupon_discount = $request->order_coupon_discount;
        $spare = $request->spare;
        //echo $spare;die;
        $order_type = $request->ordertype;
        $technician_id = $request->technician_id;
        
        if($technician_id !=null){
            $temp_technician_id=$technician_id;
        }else{
            $temp_technician_id=0;
        }
        
        $note = $image;
        $voicemsg = $request->voicemsg;
        $supplier_id = $request->supplier_id;
        $vehicleid = $request->vehicleid;
         $order_longitude = $request->order_longitude;
        $order_latitude = $request->order_latitude;
        $payment_id = $request->payment_id;
        $labour_rate = $request->labour_rate;
        // $payment_id = "test";
        if($payment_id == "" || $payment_id==null){
            $payment_id=0;
        }
        if($request->payment_status == "success"){
        $payment_status = $request->payment_status;
            
        }else{
        $payment_status = "failed";
            
        }
        // $sql = "INSERT INTO `tbl_orders`( `puser_id`, `order_type`, `order_status`,`payment_status`, `order_address`, `order_pincode`, `order_coupon_code`, `order_subtotal_amount`, `order_coupon_amt`, `order_total_amount`, `order_date`, `order_schedule_date`,`type_servicespare`,`order_coupon_discount`, `technician_id`,`note`,`voicemsg`,`supplier_id`,`uservehicleid`) VALUES ('$uid','$delivery_type','Pending','Pending','$address','$pincode','$order_coupon_code','$order_subtotal1','$order_coupon_amt','$order_total','$order_date','$order_schedule_date','$order_type','$order_coupon_discount','$technician_id','$note','$voicemsg','$supplier_id','$vehicleid')";
        
        $sql = "INSERT INTO `tbl_orders`( `puser_id`, `order_type`, `order_status`,`payment_status`,payment_id, `order_address`, `order_pincode`, `order_coupon_code`, `order_subtotal_amount`, `order_coupon_amt`, `order_total_amount`, `order_date`, `order_schedule_date`,`schedule_time`,`order_description`,`type_servicespare`,`order_coupon_discount`, `technician_id`,`note`,`voicemsg`,`supplier_id`,`uservehicleid`,`order_latitude`,`order_longitude`,labour_rate) VALUES ('$uid','$delivery_type','Pending','$payment_status','$payment_id','$address','$pincode','$order_coupon_code','$order_subtotal1','$order_coupon_amt','$order_total','$order_date','$order_schedule_date','$sch_time','$order_discription','$order_type','$order_coupon_discount','$temp_technician_id','$note','$voicemsg','$supplier_id','$vehicleid','$order_latitude','$order_longitude','$labour_rate')";
        app('db')->insert($sql);
        //echo $sql;
        //die('first');
        
        $lastinsertid = DB::getPdo()->lastInsertId();
        $src=0.0;
        $des=0.0;
        if($lastinsertid>0)
        {
            $sqlupdate1 = "UPDATE `tbl_orders` SET `order_trackingnumber`='VHS$lastinsertid' WHERE order_id='$lastinsertid'";
            app('db')->update($sqlupdate1);
            
            // **********user notication************
            $sql11="SELECT * FROM tbl_m_users join tbl_maddress on tbl_maddress.muserid = tbl_m_users.muser_id where muser_id =$uid and m_deviceid IS NOT NULL";
            $result11= app('db')->select($sql11);
            
            if($result11)
            {
                // print_r($result11);exit;
                $src=floatval($result11[0]->lattitude);
                $des=floatval($result11[0]->longitude);
                
                foreach($result11 as $row11)
                {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"We will get back to soon\"},\n\"headings\": {\"en\": \"Your Order Place successfully\"},\n\"include_player_ids\": [\"$row11->m_deviceid\"]}");
                    $headers = array();
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));
                    $result = curl_exec($ch);
                    if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                }
            //*********end user Notification*******
            
        }
        //var_dump($data);die;
        if($request->service!=null)
        {
            // dd($request->service);
            foreach($service as $row)
            {
                $sql1 = "SELECT * FROM tbl_p_users where puser_id =$request->technician_id and puser_availability_status='On Dutty' and puser_type='Technician'";
                $results1 = app('db')->select($sql1);
                // dd($results1);
                if(!empty($results1))
                {
                    foreach($results1 as $row1)
                    {
                        $puser_id = $row1->puser_id;
                        $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$lastinsertid','$puser_id')";
                        app('db')->insert($sql);
                        // echo $sql;
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"3bebbed7-d995-4f6d-90de-89ca233b2b3c\",\n\"contents\": {\"en\": \"Please Check Details!..\"},\n\"headings\": {\"en\": \"You Got Order \"},\n\"include_player_ids\": [\"$row1->puser_deviceid\"]}");
                        $headers = array();
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));
                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        curl_close($ch);
                    }
                    $date=date('Y-m-d h:i:s');
                    $sql="INSERT INTO `tbl_noti_store`(`title`, `description`, `user_id`, `partner_id`, `create_at`, `send_by`,order_id) VALUES ('Service Order Request!!!!',' order no. VHS$lastinsertid .....','$uid','$request->technician_id','$date','user','$lastinsertid')";
                    app('db')->insert($sql);    
                }
                $od_service_id = $row['ser_id'];
                $od_service_price = $row['sellingprice'];

                $sql = "INSERT INTO `tbl_orderdetails`(`od_order_id`,`od_service_id`,`od_service_price`) VALUES ('$lastinsertid','$od_service_id','$od_service_price')";
                app('db')->insert($sql);
            }
            $sql = "DELETE FROM `tbl_quick_cart` WHERE muser_id='$uid'";//echo $sql;die;
            app('db')->delete($sql);
        }
        if($request->Inspection !=null)
        {
            // echo 'hi';
            $sql1 = "SELECT * FROM tbl_p_users where puser_id =$request->technician_id and puser_availability_status='On Dutty' and puser_type='Technician'";
            $results1 = app('db')->select($sql1);
            if(!empty($results1))
            {
                foreach($results1 as $row1)
                {
                   $puser_id = $row1->puser_id;
                   $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$lastinsertid','$puser_id')";
                   app('db')->insert($sql);
                  $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"3bebbed7-d995-4f6d-90de-89ca233b2b3c\",\n\"contents\": {\"en\": \"Please Check Details!..\"},\n\"headings\": {\"en\": \"You Got Order \"},\n\"include_player_ids\": [\"$row1->puser_deviceid\"]}");
                    $headers = array();
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));
                    $result = curl_exec($ch);
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                }
                $date=date('Y-m-d h:i:s');
                    $sql="INSERT INTO `tbl_noti_store`(`title`, `description`, `user_id`, `partner_id`, `create_at`, `send_by`,order_id) VALUES ('Inspection Order Request!!!!',' order no. VHS$lastinsertid .....','$uid','$request->technician_id','$date','user','$lastinsertid')";
                    app('db')->insert($sql);
            }
            foreach($Inspection as $row)
            {
                $od_service_id = $row['id'];
                $od_service_price = $row['selling_price'];
                $sql = "INSERT INTO `tbl_orderdetails`(`od_order_id`,`od_service_id`,`od_service_price`) VALUES ('$lastinsertid','$od_service_id','$od_service_price')";
                app('db')->insert($sql);
            }
        }
        
        if($request->Prepurchase !=null)
        {
            $sql1 = "SELECT * FROM tbl_p_users where puser_id =$request->technician_id and puser_availability_status='On Dutty' and puser_type='Technician'";
            $results1 = app('db')->select($sql1);
            if(!empty($results1))
            {
                foreach($results1 as $row1)
                {
                    $puser_id = $row1->puser_id;
                    $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$lastinsertid','$puser_id')";
                    app('db')->insert($sql);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"3bebbed7-d995-4f6d-90de-89ca233b2b3c\",\n\"contents\": {\"en\": \"Please Check Details!..\"},\n\"headings\": {\"en\": \"You Got Order \"},\n\"include_player_ids\": [\"$row1->puser_deviceid\"]}");
                    $headers = array();
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));
                    $result = curl_exec($ch);
                    
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                }
                $date=date('Y-m-d h:i:s');
                    $sql="INSERT INTO `tbl_noti_store`(`title`, `description`, `user_id`, `partner_id`, `create_at`, `send_by`,order_id) VALUES ('Pre-purchase Order Request!!!!',' order no. VHS$lastinsertid .....','$uid','$request->technician_id','$date','user','$lastinsertid')";
                    app('db')->insert($sql);
            }
            foreach($Prepurchase as $row)
            {
                $od_service_id = $row['id'];
                $od_service_price = $row['selling_price'];
                $sql = "INSERT INTO `tbl_orderdetails`(`od_order_id`,`od_service_id`,`od_service_price`) VALUES ('$lastinsertid','$od_service_id','$od_service_price')";
                app('db')->insert($sql);
            }
 }
       
        if($request->spare!=null)
        {
            $sql1 = "SELECT * FROM tbl_p_users where puser_availability_status='On Dutty' and puser_type='Supplier' and puser_id=$supplier_id";
            $results1 = app('db')->select($sql1);
            
            if(!empty($results1))
            {
                foreach($results1 as $row1)
                {
                    $puser_id = $row1->puser_id;
                    $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$lastinsertid','$puser_id')";
                    app('db')->insert($sql);
                    
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"3bebbed7-d995-4f6d-90de-89ca233b2b3c\",\n\"contents\": {\"en\": \"Please Check Details!..\"},\n\"headings\": {\"en\": \"You Got Order \"},\n\"include_player_ids\": [\"$row1->puser_deviceid\"]}");
                    $headers = array();
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));
                    $result = curl_exec($ch);
                    
                    if (curl_errno($ch)) {
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                }
                $date=date('Y-m-d h:i:s');
                $sql="INSERT INTO `tbl_noti_store`(`title`, `description`, `user_id`, `partner_id`, `create_at`, `send_by`,order_id) VALUES ('Spare Order Request!!!!',' order no. VHS$lastinsertid .....','$uid','$supplier_id','$date','user','$lastinsertid')";
                app('db')->insert($sql);
            }
            $rndno = rand(1000, 9999);
            $sqlupdate1 = "UPDATE `tbl_orders` SET `supplier_otp`='$rndno', `supplier_otp_status`= '0' WHERE order_id='$lastinsertid'";
            app('db')->update($sqlupdate1);
            foreach($spare as $row)
            {
                $od_service_id = $row['p_spare_id'];
                $od_service_price = $row['p_spare_selling_price'] * $row['quantity'];
                $quantity = $row['quantity'];
                $sql = "INSERT INTO `tbl_orderdetails`(`od_order_id`,`od_service_id`,`od_service_price`,`quantity`) VALUES ('$lastinsertid','$od_service_id','$od_service_price','$quantity')";
                app('db')->insert($sql);
            }
            
            if($request->spare!=null){
                $sql = "DELETE FROM `tbl_spare_cart` WHERE muser_id='$uid'";
                app('db')->delete($sql);
            }
            // **********Technician Notification *********
            if($technician_id !=null )
            {
                $temp_technician_id=$technician_id;
                $sql11="SELECT * FROM tbl_p_users where puser_id =$temp_technician_id and puser_type='Technician' and puser_deviceid IS NOT NULL";
                $result11= app('db')->select($sql11);
                if($result11)
                {
                    // dd($result11);
                    foreach($result11 as $row11)
                    {
                       $puser_id = $row11->puser_id;
                    $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$lastinsertid','$puser_id')";
                    app('db')->insert($sql);
                     
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"3bebbed7-d995-4f6d-90de-89ca233b2b3c\",\n\"contents\": {\"en\": \"Dear $row11->puser_name Order Placed\"},\n\"headings\": {\"en\": \"You Got Order\"},\n\"include_player_ids\": [\"$row11->puser_deviceid\"]}");
                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
                         
                    }
                    $date=date('Y-m-d h:i:s');
                    $sql="INSERT INTO `tbl_noti_store`(`title`, `description`, `user_id`, `partner_id`, `create_at`, `send_by`,order_id) VALUES ('Spare Order Request!!!!','Order no. VHS$lastinsertid .....','$uid','$temp_technician_id','$date','user','$lastinsertid')";
                    app('db')->insert($sql);
                    
                }
            }else{
                //   $sql11="SELECT * FROM tbl_p_users where puser_availability_status='On Dutty' and puser_type='Technician' and puser_deviceid IS NOT NULL";
                $sql11="SELECT * FROM tbl_p_users where puser_type='Technician' and puser_deviceid IS NOT NULL and puser_long !='' and puser_lat_long !=''";
                $result11= app('db')->select($sql11);
                if($result11)
                {
                    // dd($result11);
                    foreach($result11 as $row11)
                    {
                        $src1=floatval($row11->puser_lat_long);
                	    $des1=floatval($row11->puser_long);
                        $unit='K';
                        if(!empty($des1))
                        {
                            $theta = $des - $des1;
                            $dist = sin(deg2rad($src)) * sin(deg2rad($src1)) +  cos(deg2rad($src)) * cos(deg2rad($src1)) * cos(deg2rad($theta));
                            $dist = acos($dist);
                            $dist = rad2deg($dist);
                            $miles = $dist * 60 * 1.1515;
                            $unit = strtoupper($unit);
                            if ($unit == "K") {
                                $miles = round($miles * 1.609344,4);
                                $miles = $miles*1000;
                            }
                        }
                        else{
                              $miles =  -1;
                        }
                        if($miles>-1 && $miles<10000 )
                        {
                               $puser_id = $row11->puser_id;
                                $sql = "INSERT INTO `tbl_send_order_notification`(`order_id`,`p_user_id`) VALUES ('$lastinsertid','$puser_id')";
                                app('db')->insert($sql);
                    
                            // $res[$i]->distance = $miles;
                            //=======================================Send notification <10KM ==============
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"3bebbed7-d995-4f6d-90de-89ca233b2b3c\",\n\"contents\": {\"en\": \"Please Check Details!..\"},\n\"headings\": {\"en\": \"You Got Order\"},\n\"include_player_ids\": [\"$row11->puser_deviceid\"]}");
                            $headers = array();
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));
                            $result = curl_exec($ch);
                            
                            if (curl_errno($ch)) {
                                echo 'Error:' . curl_error($ch);
                            }
                            curl_close($ch);
                            $date=date('Y-m-d h:i:s');
                            $sql="INSERT INTO `tbl_noti_store`(`title`, `description`, `user_id`, `partner_id`, `create_at`, `send_by`,order_id) VALUES ('Spare Order Request!!!!','Order no. VHS$lastinsertid .....','$uid','$puser_id','$date','user','$lastinsertid')";
                            app('db')->insert($sql);
                        }
                         
                    }  
                }
            }
                // ********end tech notification *******
            
        }
        
        
        $date = date('d-m-Y');

        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['data'] = ['Orderid'=>'VH'.$lastinsertid,"Amount"=>$order_total];
        $output['order_id'] = $lastinsertid;
        $output['msg'] = "Order Placed.!";
    }else
        {
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] = "Order Not Placed.!";
        } 
    }
    return response()->json($output);
    
}

public function supplierspares(Request $request){

    $uid = $request->header('userid');
   $sql1 = "SELECT * FROM `tbl_spare_cart` 
    where muser_id='$uid' and userv_id='$request->uservehicle'";
    // echo $sql;die;
    $result1 = app('db')->select($sql1);
      if(!empty($result1))
    {
    foreach($result1 as $result1_rec)
    {
        $spareid[]=$result1_rec->spare_id;
    }
    //dd($spare_id);die;

 
  $spareid=implode(',',$spareid);
  $sql = "SELECT spare_cat_id,p_spare_id,p_spare_mrp,p_spare_gst_percentage,p_spare_selling_price,gst_amount,gstspprice,sp_title,sp_description,sp_hsn_code,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,spcat_title,spcat_description, p_spare_create_by, spare_cat_id, tbl_spare.veh_id, tbl_spare.vh_cost ,(p_spare_selling_price+vh_cost) as final_cost from tbl_p_spares
    left join tbl_spare on tbl_spare.sp_id=tbl_p_spares.spare_id
   left join tbl_spare_catg on tbl_spare_catg.spcat_id=tbl_p_spares.spare_cat_id
  where p_spare_create_by=$request->supplier_id and spare_cat_id=$request->sparecatid and p_spare_status='Active' and FIND_IN_SET('$request->vehb_id',veh_id)";
  
  //echo $sql;die;
    $result = app('db')->select($sql);
 
 // dd($result);
}else{
    

     $sql = "SELECT spare_cat_id,p_spare_id,p_spare_mrp,p_spare_gst_percentage,p_spare_selling_price,gst_amount,gstspprice,sp_title,sp_description,sp_hsn_code,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,spcat_title,spcat_description, p_spare_create_by, spare_cat_id, tbl_spare.veh_id, tbl_spare.vh_cost ,(p_spare_selling_price+vh_cost) as final_cost from tbl_p_spares
    left join tbl_spare on tbl_spare.sp_id=tbl_p_spares.spare_id
    left join tbl_spare_catg on tbl_spare_catg.spcat_id=tbl_p_spares.spare_cat_id
     where p_spare_create_by=$request->supplier_id and spare_cat_id=$request->sparecatid and p_spare_status='Active'";
    $result = app('db')->select($sql);
      //echo $sql;die;

  //  dd($$result);
}
    $points='Aditional 50 point check';
    
    $pointcheck = array(
        'point_check' => $points
    );

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Supplier Spare";
        $output['data'] =  $result;
      //  $output['data'] =  array_merge($output['data'],$pointcheck);
       $output['point_check'] =  $pointcheck;
    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Supplier Spare Found.!";
    }

    return response()->json($output);
}

public function showtechnicianscreen(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT status from tbl_technician_screen";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if($result[0]->status=='Active'){
        $output['status'] = true;
        $output['msg'] =  "Tehnician Screen";
        
        $output['statuscode'] = 200;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Tehnician Screen";
    }
    return response()->json($output);
}

public function standardsparecart(Request $request){
  
    $uid = $request->header('userid');
    $sql = "SELECT * from tbl_spare_cart where userv_id='$request->userv_id' and muser_id='$uid' and spare_id ='$request->spare_id' and supplierid='$request->supplierid'";

    $result = app('db')->select($sql);

    if(empty($result)){
        $sql1 = "SELECT distinct concat (supplierid) as supplierid from tbl_spare_cart where muser_id='$uid' and userv_id='$request->userv_id'";
        $result1 = app('db')->select($sql1);
        if(isset($result1[0]->supplierid)){
            if($result1[0]->supplierid!=$request->supplierid){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "supplier already present in cart";
                return response()->json($output);
            }
        }
        $sql2= "SELECT  distinct concat (userv_id) as userv_id  from tbl_spare_cart where muser_id='$uid'  and supplierid='$request->supplierid'";
        $result2 = app('db')->select($sql2);
        if(isset($result2[0]->userv_id)){
            if($result2[0]->userv_id!=$request->userv_id){
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "spare already present in cart with different vehicle";
                return response()->json($output);
            }
        }
   $sql = "INSERT INTO `tbl_spare_cart`(`muser_id`,`spare_id`, `userv_id`, `supplierid`, `quantity`) VALUES ('$uid','$request->spare_id','$request->userv_id','$request->supplierid','$request->quantity')";
            $results = app('db')->insert($sql);
        
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Spare added successfully!!";

    }else{
       $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Something Went Wrong";
    }

    return response()->json($output);
}

public function getstandardcart(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT p_spare_id,cart_id,tbl_p_spares.spare_id,sp_title,labour_rate,p_spare_mrp-p_spare_selling_price as discount,p_spare_mrp+labour_rate as p_spare_mrp,p_spare_gst_percentage,quantity,p_spare_selling_price+labour_rate as p_spare_selling_price,gst_amount,gstspprice,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,(p_spare_selling_price+labour_rate) * quantity as 'Totalsparesellingprice' ,gstspprice * quantity as 'Totalsubtotal',gst_amount * quantity as 'Totalofgstamount',(p_spare_mrp+labour_rate)* quantity as 'Totalofmrp',labour_rate* quantity as 'Totaloflabour_rate' FROM `tbl_spare_cart`  left JOIN tbl_p_spares ON p_spare_id=tbl_spare_cart.spare_id
    left join tbl_spare on tbl_p_spares.spare_id=tbl_spare.sp_id
    where muser_id='$uid' and userv_id='$request->userv_id'";
   // dd($sql);
    // echo $sql;die;
    $result = app('db')->select($sql);
   if(!empty($result)){
    $total=0.0;
       $subtotal=0.0;
       $gstprice=0.0;
       $labour_rate=0.0;
       $discount = 0.0;
       foreach($result as $result_rec){
        $total=$total+($result_rec->p_spare_selling_price * $result_rec->quantity) ;
           $subtotal=$subtotal+($result_rec->gstspprice* $result_rec->quantity) ;
           $gstprice=$gstprice + ($result_rec->gst_amount* $result_rec->quantity) ;
           $labour_rate = $labour_rate + ($result_rec->labour_rate* $result_rec->quantity);
           $discount = $discount + $result_rec->discount;
       }
      
       $output['subtotal'] = $subtotal;
       $output['gstprice'] = $gstprice;
       $output['labour_rate'] = $labour_rate;
       $output['Discount'] = $discount;
       $output['total'] = $total;
        $output['status'] = true;
        $output['msg'] =  "Spare Cart";
        $output['data'] =  $result;
        $output['statuscode'] = 200;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['data'] =  $result;
        $output['msg'] =  "No Spare Cart";
    }
    return response()->json($output);
}

public function categoryuserspare(Request $request){
    $vid=$request->uservehicle;
    $sql = "SELECT * from tbl_spare_catg where spcat_status='Active' and tbl_spare_catg.spcat_vehicle_type = (select tbl_user_vehicles.userv_vehicle_type from tbl_user_vehicles WHERE tbl_user_vehicles.userv_id=$vid)";
    $result = app('db')->select($sql);

    $sql1 = "SELECT puser_name,puser_id,puser_pincode,puser_address from tbl_p_users where puser_id='$request->supplier_id'";
    $result1 = app('db')->select($sql1); 
//print_r($result);exit;

    if(!empty($result)){
        
    $j=0;
    foreach($result as $row){ 
        $i=0;
      //  $sparelist=array();
      $uid = $request->header('userid');
    
         $sql2 ="SELECT spare_cat_id,p_spare_id,p_spare_mrp+labour_rate as p_spare_mrp,p_spare_gst_percentage,p_spare_selling_price+labour_rate as p_spare_selling_price,labour_rate,gst_amount,gstspprice,sp_title,sp_description,sp_hsn_code,sp_imageone,sp_imagetwo,sp_imagethree,sp_imagefour,spcat_title,spcat_description, p_spare_create_by, spare_cat_id, tbl_spare.veh_id, tbl_spare.vh_cost ,(p_spare_selling_price+vh_cost) as final_cost from tbl_p_spares
    left join tbl_spare on tbl_spare.sp_id=tbl_p_spares.spare_id
   left join tbl_spare_catg on tbl_spare_catg.spcat_id=tbl_p_spares.spare_cat_id
  where p_spare_create_by=$request->supplier_id and spare_cat_id=$row->spcat_id and spcat_status='Active' and  p_spare_status='Active' and p_spare_id NOT In (SELECT tbl_spare_cart.spare_id from tbl_spare_cart WHERE tbl_spare_cart.muser_id=$uid) ";
         $result2 = app('db')->select($sql2);
        //  echo $sql2;
         if(!empty($result2)){
             $cnt=count($result2);
                 if($row->spcat_id == $result2[$i]->spare_cat_id){
                 $result[$j]->spare_list = $result2;
                     
             }
                
         }
        $j++; 
          $i++; 
    }
     
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Spare Category";
        $output['data'] =  $result;
        $output['supplier'] =  $result1;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Spare Category found";
        $output['data'] =  $result;
        $output['supplier'] =  $result1;
    }
    return response()->json($output);
}

public function getquickcart(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT userv_id,gstexcludedspprice,sellingprice,gstamount,tbl_service.ser_id,sellingprice,price,ser__title,ser__description,ser_thumbnail,ser_image  FROM `tbl_quick_cart`  left JOIN tbl_service ON FIND_IN_SET( tbl_service.ser_id,tbl_quick_cart.ser_id) > 0 
    where muser_id='$uid' and userv_id='$request->userv_id'";
    // echo $sql;die;
    $result = app('db')->select($sql);
   if(!empty($result)){
       
       $total=0;
       $subtotal=0;
       $gstprice=0;
       $discount=0;
       foreach($result as $result_rec){
        $total=$total+$result_rec->sellingprice;
        $discount = $discount + ($result_rec->price - $result_rec->sellingprice);
        if($result_rec->gstexcludedspprice==null){
            $result_rec->gstexcludedspprice=0;
        }
        if($result_rec->gstamount==null){
            $result_rec->gstamount=0;
        }
        // print_r($subtotal);
           $subtotal=$subtotal+$result_rec->gstexcludedspprice;
           $gstprice=$gstprice + $result_rec->gstamount;
       }
         $output['vehid'] = $request->userv_id;
       $output['subtotal'] = $subtotal;
       $output['gstprice'] = $gstprice;
       $output['discount'] = $discount;
       $output['total'] = $total;
       
       
        $output['status'] = true;
        $output['msg'] =  "Service Cart";
        $output['data'] =  $result;
        $output['statuscode'] = 200;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['data'] =  $result;
        $output['msg'] =  "No Service Cart";
    }
    return response()->json($output);
}

public function deletestandardcart(Request $request){
    $uid = $request->header('userid');
 
    $sql = "delete from tbl_spare_cart where cart_id='$request->cart_id'";
    $results = app('db')->delete($sql);
    if(!empty($results)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Cart deleted successfully";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Something went wrong!!";
    }
    return response()->json($output);
}

public function deletequickcart(Request $request){
    $uid = $request->header('userid');
 
    $sql = "delete from tbl_quick_cart where qc_id='$request->qc_id'";
    $results = app('db')->delete($sql);
    if(!empty($results)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Cart deleted successfully";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Something went wrong!!";
    }
    return response()->json($output);
}

public function deletestandardspare(Request $request){
    $uid = $request->header('userid');
 
    $sql = "SELECT * from tbl_spare_cart where userv_id='$request->userv_id' and muser_id='$uid' and find_in_set('$request->spareid',spare_id) <> 0";
  
    $result = app('db')->select($sql);
    if(!empty($result)){
        $arrayofspare=explode(',',$result[0]->spare_id);
 
    if(count($arrayofspare)==1){
        $cartid=$result[0]->cart_id;
        $sql = "delete from tbl_spare_cart where cart_id='$cartid'";
        $results = app('db')->delete($sql);

    }else{
        $arrayofspare = array_flip($arrayofspare);
        unset($arrayofspare[ $request->spareid ]);
        $arrayofspare = array_flip($arrayofspare);
        $arrayofspare=implode(',',$arrayofspare);
        $sql = "UPDATE `tbl_spare_cart` SET `spare_id`= '$arrayofspare' WHERE userv_id = '$request->userv_id' and muser_id='$uid' ";
        $results = app('db')->update($sql);
    }
    if(!empty($results)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Spare deleted successfully";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Something went wrong!!";
    }
    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Something went wrong!!";
    }
    
    return response()->json($output);
}

public function deletequickservice(Request $request){
    $uid = $request->header('userid');
 
    $sql = "SELECT * from tbl_quick_cart where userv_id='$request->userv_id' and muser_id='$uid' and find_in_set('$request->ser_id',ser_id) <> 0";

    $result = app('db')->select($sql);
    if(!empty($result)){
        $arrayofspare=explode(',',$result[0]->ser_id);
 
    if(count($arrayofspare)==1){
        $qc_id=$result[0]->qc_id;
        $sql = "delete from tbl_quick_cart where qc_id='$qc_id'";
        $results = app('db')->delete($sql);

    }else{
        $arrayofspare = array_flip($arrayofspare);
        unset($arrayofspare[ $request->ser_id ]);
        $arrayofspare = array_flip($arrayofspare);
        $arrayofspare=implode(',',$arrayofspare);
        
        $sql = "UPDATE `tbl_quick_cart` SET `ser_id`= '$arrayofspare' WHERE userv_id = '$request->userv_id' and muser_id='$uid' ";
        $results = app('db')->update($sql);
    }
    if(!empty($results)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Service deleted successfully";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Something went wrong!!";
    }
    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Something went wrong!!";
    }
    
    return response()->json($output);
}

public function quickinspection(Request $request){
    $uid = $request->header('userid');
   
            $sql = "INSERT INTO `tbl_quickinspection`(`userID`, `inspectiontype`, `vehicleid`,`status`, `remark`, `createdat`) VALUES ('$uid','$request->inspection_type','$request->userv_id','Active','$request->remark',CURRENT_TIMESTAMP)";
            $results = app('db')->insert($sql);
            
                  $deviceid= "37b54ca1-eb21-4a68-87aa-3228dd5f3f60";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"Thank you for your inquiry our team will get back to you shortly\"},\n\"headings\": {\"en\": \"Repair Inspection Inquiry\"},\n\"include_player_ids\": [\"$deviceid\"]}");

                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
            if(!empty($results)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Thank you for your enquiry our team will get back to you shortly";
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] = "Something went wrong!!";
            }
        return response()->json($output);
}

public function prepurchaseinspection(Request $request){
    $uid = $request->header('userid');
   
            $sql = "INSERT INTO `tbl_prepurchaseinspection`(`userID`, `inspectiontype`, `vehicleid`,`status`, `remark`, `createdat`) VALUES ('$uid','$request->inspection_type','$request->userv_id','Active','$request->remark',CURRENT_TIMESTAMP)";
            $results = app('db')->insert($sql);
            
                      $deviceid= "37b54ca1-eb21-4a68-87aa-3228dd5f3f60";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://onesignal.com/api/v1/notifications');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"app_id\": \"a7b9106b-bc64-4d7d-a2e9-bcabdb5898a5\",\n\"contents\": {\"en\": \"Thank you for your inquiry our team will get back to you shortly\"},\n\"headings\": {\"en\": \"Pre Purchase Inspection Inquiry\"},\n\"include_player_ids\": [\"$deviceid\"]}");

                          $headers = array();
                         curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                  'Authorization: Basic ZjQ3Nzk0YzQtMjhhNy00MmE3LWI5ZDAtNTVhMTcxYTg1ZDEy'));

                        $result = curl_exec($ch);
                        if (curl_errno($ch)) {
                            echo 'Error:' . curl_error($ch);
                        }
                        
                        curl_close($ch);
                        
            if(!empty($results)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Thank you for your inquiry our team will get back to you shortly";
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] = "Something went wrong!!";
            }
        return response()->json($output);
}

public function deliverydates(Request $request){
    $date = new DateTime(date("d M Y "));

    for($i=0;$i<=6;$i++){
        $date->modify("+$i day");
        $dates[]=$date->format("d M Y");
    }

            if(!empty($dates)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['dates'] =  $dates;
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] = "Something went wrong!!";
            }
        return response()->json($output);
}

  public function getinspectiontype(Request $request){
    
        $uid = $request->header('userid');
       
        $vehicle_type =$request->vehicle_type;
        $expanded =$request->inspection_type;
        
    if($expanded !=0){
       //$sql = "SELECT * from tbl_inspections where vehicle_type='$vehicle_type' and expanded='$expanded'" ;
       $sql="SELECT tbl_inspections.*  ,(gst_percentage*selling_price)/100 as gst_amount, (selling_price-((gst_percentage*selling_price)/100)) as sub_total, (price-selling_price) as discount  from tbl_inspections where tbl_inspections.status = 'Active' and vehicle_type='$vehicle_type' and expanded='$expanded'" ;
       //echo $sql;die;
    }else{
       // $sql = "SELECT * from tbl_inspections where vehicle_type='$vehicle_type' and expanded='$expanded'";
        $sql="SELECT tbl_inspections.*  ,(gst_percentage*selling_price)/100 as gst_amount, (selling_price-((gst_percentage*selling_price)/100)) as sub_total, (price-selling_price) as discount  from tbl_inspections where vehicle_type='$vehicle_type' and expanded='$expanded' and tbl_inspections.status = 'Active'" ;
    }
        $result = app('db')->select($sql);
    //  print_r($result);exit;
        if(!empty($result)){
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Inspection Type";
            $output['data'] =  $result;

        }else{
        $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Inspection Type Found.!";
        }

        return response()->json($output);
    }
    
    public function serviceget(Request $request){
   
    $sql = "SELECT * from tbl_service where ser_id='$request->ser_id'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $i=0;
        foreach($result as $row)
        {
            if($row->ser_type=="Quick"){
                $result[$i]->ser_type = "Repair";    
            }
            if($row->ser_type=="Standard"){
                $result[$i]->ser_type = "Replacement";
            }
        }
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "service";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No service found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function getaddress(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT * from tbl_maddress where muserid='$uid'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "address";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No address found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function saveaddress(Request $request){
    $uid = $request->header('userid');
   
            $sql = "INSERT INTO `tbl_maddress`(`muserid`, `pincode`, `address`,`type`, `block_number`, `landmark`, `lattitude`, `longitude`,`createdat`) 
                VALUES ('$uid','$request->pincode','$request->address','$request->type','$request->block_number','$request->landmark','$request->lattitude','$request->longitude',CURRENT_TIMESTAMP)";
                $results = app('db')->insert($sql);
            if(!empty($results)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Address Saved successfully";
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] = "Something went wrong!!";
            }
        return response()->json($output);
}

public function updateaddress(Request $request){
   
   $sql = "UPDATE `tbl_maddress` SET `pincode`= '$request->pincode', `address`= '$request->address' , `type`= '$request->type' , `lattitude`= '$request->lattitude', `longitude`= '$request->longitude', `block_number`= '$request->block_number', `landmark`= '$request->landmark'   
        WHERE addressid = '$request->addressid'";
        $results = app('db')->update($sql); 
            if(!empty($results)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Address updated successfully";
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] = "Something went wrong!!";
            }
        return response()->json($output);
}

public function deleteaddress(Request $request){
   
    $sql = "delete from tbl_maddress where addressid='$request->addressid'";
        $results = app('db')->delete($sql);
            if(!empty($results)){
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Address Deleted successfully";
        
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] = "Something went wrong!!";
            }
        return response()->json($output);
}


public function saveenquiry(Request $request){
    $uid = $request->header('userid');   
    // var_dump($request->hasFile('image1'));die;
    if ($request->hasFile('image1')) {
        $original_filename = $request->file('image1')->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
        $destination_path = './uploads/order/';
        $image1 = 'U-' . rand() . '.' . $file_ext;
        if ($request->file('image1')->move($destination_path, $image1)) {
            $image1 =  $image1;
      
        } else {
            $image1=null;
        }
    } else {
        $image1=null;
    }

    if ($request->hasFile('image2')) {
        $original_filename = $request->file('image2')->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
        $destination_path = './uploads/order/';
        $image2 = 'U-' . rand() . '.' . $file_ext;
        if ($request->file('image2')->move($destination_path, $image2)) {
            $image2 =  $image2;
      
        } else {
            $image2=null;
        }
    } else {
        $image2=null;
    }
    
    if ($request->hasFile('image3')) {
        $original_filename = $request->file('image3')->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
        $destination_path = './uploads/order/';
        $image3 = 'U-' . rand() . '.' . $file_ext;
        if ($request->file('image3')->move($destination_path, $image3)) {
            $image3 =  $image3;
      
        } else {
            $image3=null;
        }
    } else {
        $image3=null;
    }

    if ($request->hasFile('image4')) {
        $original_filename = $request->file('image4')->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
        $destination_path = './uploads/order/';
        $image4 = 'U-' . rand() . '.' . $file_ext;
        if ($request->file('image4')->move($destination_path, $image4)) {
            $image4 =  $image4;
      
        } else {
            $image4=null;
        }
    } else {
        $image4=null;
    }

    
    $sql = "INSERT INTO `user_enquiry_images`(`image1`, `image2`, `image3`, `image4`, `description`,`createdat`, `muserid`) VALUES ('$image1','$image2','$image3','$image4','$request->description', CURRENT_TIMESTAMP,'$uid')";
    $results = app('db')->insert($sql);
    
    
    if(!empty($results)){
       
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Enquiry Saved successfully.!";
        
        }else{
        $output['status'] = false;
    $output['statuscode'] = 404;
        $output['msg'] =  "something went wrong.!";
            }

    return response()->json($output);
}

public function getpartaddress(Request $request){
    $uid = $request->header('userid');
    $sql = "SELECT * from tbl_maddress where muserid='$uid' and addressid='$request->addressid'";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "address";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No address found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

public function voiceupload(Request $request){
    
    
    if ($request->hasFile('voice')) {
        $original_filename = $request->file('voice')->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $file_ext = end($original_filename_arr);
         $destination_path = './uploads/voice';
        $image = 'U-' . rand() . '.' . $file_ext;

        if ($request->file('voice')->move($destination_path, $image)) {
            $image =  $image;  
            
            } else {
            $image=null;
       }
    } else {
        $image=null;
    }
    
    $uid = $request->userid;
    $order_id = $request->order_id;
    $sqlupdate1 = "UPDATE `tbl_orders` SET `voicemsg`='$image' WHERE order_id='$order_id'";
                      $results=  app('db')->update($sqlupdate1);
                        
    if($results>0){

                        $output['status'] = true;
                        $output['statuscode'] = 200;
                        $output['msg'] = "Audio Uploaded.!";
    
                    }else{
                        $output['status'] = false;
                        $output['statuscode'] = 404;
                        $output['msg'] = "Audio Not Uploaded.!";
                    } 
              
                
               
                return response()->json($output);
            
}

 public function getservicecart(Request $request){
  
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_quick_cart where  muser_id='$uid'";
        $result = app('db')->select($sql);
   
        if(!empty($result)){
              $output['qc_id'] = $result[0]->qc_id;  
            $output['vehicleid'] = $result[0]->userv_id;  
        $output['data'] = $result;
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Cart Found!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Cart Not Found";
        }

        return response()->json($output);
    }


 public function addcontactdetail(Request $request){
  
       $sql = "INSERT INTO `tbl_contact_details`(`name`, `email`, `phno`,`msg`, `createdbydate`) VALUES ('$request->name','$request->email','$request->phno','$request->msg',CURRENT_TIMESTAMP)";
            $results = app('db')->insert($sql);
             if(!empty($results)){
           
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Contact Submitted successfully!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Contact not submitted";
        }

        return response()->json($output);
    }
    
    
    public function getcontactdetail(Request $request){
  
        $sql = "SELECT * from tbl_contact_details";
        $result = app('db')->select($sql);
             if(!empty($result)){
            $output['data'] = $result;
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Contact Details!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Contact not Details";
        }

        return response()->json($output);
    }

public function getsparecart(Request $request){
  
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_spare_cart where  muser_id='$uid'";
        $result = app('db')->select($sql);
   
        if(!empty($result)){
              $output['cart_id'] = $result[0]->cart_id;  
            $output['vehicleid'] = $result[0]->userv_id;  
              $output['supplierid'] = $result[0]->supplierid;  
        $output['data'] = $result;
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Cart Found!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Cart Not Found";
        }

        return response()->json($output);
    }
    
    public function deletesparecart(Request $request){
    $uid = $request->header('userid');
 
    $sql = "delete from tbl_spare_cart where muser_id='$uid'";
    $results = app('db')->delete($sql);
    if(!empty($results)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Cart deleted successfully";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Something went wrong!!";
    }
    return response()->json($output);
}

public function submitquote(Request $request){
    $uid = $request->header('userid');
       $sql = "INSERT INTO `tbl_getaquote`(`muser_id`, `uservehicle_id`, `message`, `created`) VALUES ('$uid','$request->uservehicle_id','$request->message',CURRENT_TIMESTAMP)";
            $results = app('db')->insert($sql);
             if(!empty($results)){
           
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Quote submitted successfully!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Quote not submitted";
        }

        return response()->json($output);
    }
    
    
    public function getquote(Request $request){
  
        $sql = "SELECT * from tbl_getaquote";
        $result = app('db')->select($sql);
             if(!empty($result)){
            $output['data'] = $result;
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Quote!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Quote not Details";
        }

        return response()->json($output);
    }


  public function getquestionorder(Request $request){
   $uid = $request->header('userid');
           $sql = "SELECT * from submit_question
           where orderid='$request->orderid' ";
        $result = app('db')->select($sql);
             if(!empty($result)){
            $output['data'] = $result;
        $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Question data!!";

        }else{
           $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Data not found";
        }

        return response()->json($output);
    }

public function add_cust_review(Request $request){
    $uid = $request->header('userid');

    $sql = "INSERT INTO `tbl_cust_reviews`(`muser_id`, `order_id`, `star_supplier`,`star_technician`, `star_vahanom`, `review_msg`,`flag`,`created_date`) 
    VALUES ('$uid','$request->order_id','$request->star_supplier','$request->star_technician','$request->star_vahanom','$request->review_msg','Active',CURRENT_TIMESTAMP)";

    $results = app('db')->insert($sql);

    if(!empty($results)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Thankyou for your feedback";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] = "Oops, Something went wrong!! Please try again";
    }
    return response()->json($output);
}

public function getreview(Request $request){

    $sql = "SELECT * from tbl_cust_reviews";
    $result = app('db')->select($sql);
            if(!empty($result)){
        $output['data'] = $result;
    $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Feedback Details!!";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Feedback not Details";
    }

    return response()->json($output);
}
 public function userAppversion(Request $request)
    {
        $sql = "SELECT * FROM tbl_app_version where usertype='$request->usertype' and status='Active'";
        $results = app('db')->select($sql);
        
        if(!empty($results)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "App Version Found!";
            $output['data'] =  $results;
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "This App Version Not Found!";
        }
        return response()->json($output);
    }

public function getCustReview(Request $request){

    $sql = "SELECT * from tbl_cust_reviews where order_id='$request->orderid'";
    //dd();
    $result = app('db')->select($sql);
            if(!empty($result)){
        $output['data'] = $result;
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Feedback Details!!";

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "Feedback not Details";
    }

    return response()->json($output);
}

public function questionlist_percentage(Request $request){
   
        $uid = $request->header('userid');
        $orderid =$request->orderid;
        $ser_type='';
        $sqlSub ="SELECT * FROM submit_question WHERE orderid = ".$orderid;
          
            $resultSub = app('db')->select($sqlSub);
            if(isset($resultSub[0]->question_answer))
            {

            // $question_answerarray = json_encode($resultSub[0]->question_answer,true);

$question_answerarray=json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resultSub[0]->question_answer), true );
                // dd($question_answerarray);
                $mainArray = array(
                    'Bad' => array(),
                    'Ok' => array(),
                    'Good' => array(),
                );
                $questionAnswer = [];
                foreach($question_answerarray as $key => $value){
                   //
                    $sqlQue ="SELECT * FROM tbl_questions WHERE queid = ".$value['queid'];
 
                    $resultquestion = app('db')->select($sqlQue);
                  
                   
                //   if($resultquestion){
                //       $ser_type=$resultquestion[0]->ser_type;
                //   }
                   // print_r($ser_type);exit;
                    
                  
                    
                    // }else{
                    //     $output['Check']='Blank Data';
                    // }
               
                   
                    array_push($questionAnswer,array('question'=>$resultquestion[0]->question_name,'answer'=>$value['answser'],'remark'=>$value['remark']));
                   
                    if($value['answser'] == 'bad'){
                   
                    array_push($mainArray['Bad'],$value);
                    }
                    if($value['answser'] == 'ok'){
                     
                        array_push($mainArray['Ok'],$value);
                    }
                    if($value['answser'] == 'good'){
                       
                        array_push($mainArray['Good'],$value);
                    }
                }
                  //print_r($resultquestion);exit;
                
                  if($resultquestion[0]->ser_type == 'Two-Wheeler'){
                    $output['Check'] ='20 Point Check';
                    //  print_r($output);exit;
                } 
                if($resultquestion[0]->ser_type == 'Four-Wheeler'){
                    $output['Check'] ='50 Point Check';
                }
                    
                $countbad = 0;
                $markbad = 0;
                $countexce = 0;
                $markexce = 0;
                $countgood = 0;
                $markgood = 0;
            
                foreach ($mainArray as $key => $value) {
                    // print_r($value);
                    if($key == 'Bad'){
                        $countbad += (count($value));
                        $markbad += (count($value) * 0.5);
                    }
                    if($key == 'Ok'){
                        $countexce += (count($value));
                        $markexce += (count($value) * 2);
                    }
                    if($key == 'Good'){
                        $countgood += (count($value));
                        $markgood += (count($value) * 1.5);
                    }
                }

                $subArray = array(
                    'Bad' =>  array('count'=>$countbad,'mark'=>$markbad),
                    'Ok' => array('count'=>$countexce,'mark'=>$markexce),
                    'Good' =>  array('count'=>$countgood,'mark'=>$markgood),
                );
                $marktotal=$markbad + $markexce + $markgood;
                $counttotal=$countbad + $countexce + $countgood;
                $count= $counttotal*2;
            //  dd($count);
                $totalpercentage=($marktotal/$count)*100;
                $percentage = (string)(round($totalpercentage, 2));
                $badpercentage=$countbad/$counttotal*100;
                $badper = (string)(round($badpercentage, 2));
                $excpercentage=$countexce/$counttotal*100;
                $excper = (string)(round($excpercentage, 2));
                $goodpercentage=$countgood/$counttotal*100;
                $goodper = (string)(round($goodpercentage, 2));
                $totalArray = array(
                    'totalcount' => $countbad + $countexce + $countgood,
                    'totalmark' => $markbad + $markexce + $markgood,
                    'percentage' => $percentage,
                    'bad-percentage'=>$badper,
                    'excellent-percentage'=>$excper,
                    'good-percentage'=>$goodper,
                );
                
               $sql5 = "SELECT out_odomete_reading, odometer_reading from tbl_orders join submit_question 
                on tbl_orders.order_id=submit_question.orderid where tbl_orders.order_id='$orderid'";
                $resultarray = app('db')->select($sql5);
                
                $odometerarray=json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resultarray[0]->odometer_reading), true );
                 $odometerarray1=json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resultarray[0]->out_odomete_reading), true );
                 
                $out_odomete_reading =array(
                    'out_odomete_reading'=>$odometerarray1,
                    'odometer_reading'=>$odometerarray,
                );
                
         
            if(!empty($resultSub)){
                
              //  print_r($ser_type);exit;
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Questions";
                $output['questionAnswer'] =$questionAnswer;
                $output['data'] =  $subArray;
                $output['marks'] =  $totalArray;
                $output['out_odomete_reading'] = $out_odomete_reading;

            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Questions";

            }
            return response()->json($output);
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Not found";
            return response()->json($output);
        }
    }
    
    public function alltechnicianvehicle(Request $request){
    $uid = $request->header('userid');
    $des = 0;
   $src=0;
    $sqladd = "SELECT lattitude,longitude from tbl_maddress where addressid= $request->address_id";
    $resultadd = app('db')->select($sqladd);
    if($resultadd){
        $src=floatval($resultadd[0]->lattitude);
        $des=floatval($resultadd[0]->longitude);
    }
   	$sql1 = "SELECT * FROM `tbl_user_vehicles` where `userv_id`='$request->veh_id' ";
    // echo $sql;die;
    $result1 = app('db')->select($sql1);
    
    $vehicletype=$result1[0]->userv_vehicle_type;
	$sql = "SELECT * FROM `tbl_p_users` where `puser_availability_status`='On Dutty' AND `puser_type`='Technician' AND FIND_IN_SET('$vehicletype',expertise) ";
    // echo $sql;die;
    //dd($sql);
    $result = app('db')->select($sql);
//dd($result);

//dd($technician_certificate);
    if(!empty($result)){
        $i=0;
        $res=array();
        foreach($result as $row){
            $src1=floatval($row->puser_lat_long);
    	    $des1=floatval($row->puser_long);
            $unit='K';
            if(!empty($des1)){
                    $theta = $des - $des1;
                     $dist = sin(deg2rad($src)) * sin(deg2rad($src1)) +  cos(deg2rad($src)) * cos(deg2rad($src1)) * cos(deg2rad($theta));
                    $dist = acos($dist);
                    $dist = rad2deg($dist);
                    $miles = $dist * 60 * 1.1515;
                    $unit = strtoupper($unit);
                
                    if ($unit == "K") {
                        $miles = round($miles * 1.609344,4);
                        $miles = $miles*1000;
                    }
            }
            else{
                  $miles =  -1;
              
            }
              
            if($miles>-1 && $miles<10000 ){
                $sqlstar = "SELECT sum(star_technician)/count(*) as stars from tbl_orders join tbl_cust_reviews on tbl_orders.order_id=tbl_cust_reviews.order_id where technician_id='$row->puser_id'";
                $resultstar = app('db')->select($sqlstar);
                if($resultstar){
                    $result[$i]->rating = $resultstar[0]->stars;
                }    
                $result[$i]->puser_name = $result[$i]->puser_name.'  (Expertise: '.$result[$i]->expertise.')';
                array_push($res,$result[$i]);
                // $res[$i]->distance = $miles;
            }
          $i++;
        }
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Technician";
        $output['data'] =  $res;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No Technician found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

  public function getshowtechnician(Request $request){
    $uid = $request->header('userid');
    
	$sql = "SELECT status FROM `tbl_showtechnician` ";
    // echo $sql;die;
    $result = app('db')->select($sql);

    if(!empty($result)){
        $output['status'] = true;
        $output['statuscode'] = 200;
        $output['msg'] =  "Status";
        $output['data'] =  $result;

    }else{
        $output['status'] = false;
        $output['statuscode'] = 404;
        $output['msg'] =  "No status found";
        $output['data'] =  $result;
    }
    return response()->json($output);
}

   public function getinsuaranceinquiry(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_insurance  left join tbl_user_insurance on insurance_id=inusr_id where user_m_id='$uid'";
        $result = app('db')->select($sql);
       // dd($result);
                if(!empty($result)){
            $output['data'] = $result;
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Insuarance inquiry Details!!";
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Insuarance inquiry not Details";
        }
    
        return response()->json($output);
    }

     public function getpollutionsinquiry(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_user_pollution left join tbl_pollutions on pollution_id=pol_id where muser_id='$uid'";
        $result = app('db')->select($sql);
       // dd($result);
                if(!empty($result)){
            $output['data'] = $result;
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Pollution Details!!";
    
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Pollution not Details";
        }
    
        return response()->json($output);
    }
    
    public function getBanners(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_banners where ban_status='Active'";
       
        $result = app('db')->select($sql);
        // dd($result);
                if(!empty($result)){
            $output['data'] = $result;
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Banners Details!!";
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Banners not Details";
        }
    
        return response()->json($output);
    }
    
     public function technicianRatingCalculation(Request $request){
        $uid = $request->header('userid');
        $technician_id =$request->technician_id;
        $sql = "SELECT star_technician from tbl_orders join tbl_cust_reviews 
        on tbl_orders.order_id=tbl_cust_reviews.order_id where technician_id='$technician_id'";
        $result = app('db')->select($sql);
        // echo $sql;
        // print_r($result);die();
        $count_row = count($result);
        if($count_row>0)
        {
            $sum_arr = array();
            // $star_technician_sum = '';
            foreach($result as $row) {
                foreach($row as $result_data) {
                    //$test = $result_data;
                // print_r($test);

                $sum_arr[] = $result_data;
                $star_technician_sum =  array_sum($sum_arr);
                }
            }
            $total=$star_technician_sum/$count_row;
        
            $totalArray = array(
                'star_technician_count' => $count_row,
                'star_technician_sum' => $star_technician_sum,
                'total' => $total
            );
        
            if(!empty($result)){
                $output['data'] = $result;
                $output['totalArray'] = $totalArray;
                $output['status'] = true;
                $output['statuscode'] = 200;
                $output['msg'] =  "Details!!";
            
            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Details not found";
            }
            return response()->json($output);
        }else{
            // echo 'hii';die;
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Details not found";
 
        }
        return response()->json($output);
    }

    public function supplierRatingCalculation(Request $request){
        $uid = $request->header('userid');
        $supplier_id =$request->supplier_id;
        $sql = "SELECT star_supplier from tbl_orders join tbl_cust_reviews 
        on tbl_orders.order_id=tbl_cust_reviews.order_id where supplier_id='$supplier_id'";
        $result = app('db')->select($sql);
        //echo $sql;
        //print_r($result);
        $count_row = count($result);
       // echo $count_row;die;
        if($count_row>0)
        {
            $sum_arr = array();
            foreach($result as $row) {
                foreach($row as $result_data) {
                    //$test = $result_data;
                // print_r($test);
                $sum_arr[] = $result_data;
                $star_supplier_sum =  array_sum($sum_arr);
                }
            }
            $total=$star_supplier_sum/$count_row;
   
            $totalArray = array(
                'star_supplier_count' => $count_row,
                'star_supplier_sum' => $star_supplier_sum,
                'total' => $total
            );

            if($result){
            $output['data'] = $result;
            $output['totalArray'] = $totalArray;
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Details!!";

            }else{
                $output['status'] = false;
                $output['statuscode'] = 404;
                $output['msg'] =  "Details not found";
            }
            
            return response()->json($output);
        }else{
            // echo 'hii';die;
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Details not found";

        }
        return response()->json($output);

    }
    
     public function technicianDetails(Request $request){
        $uid = $request->header('userid');
        $puser_id = $request->puser_id;
        //dd($puser_id);
        $sql = "SELECT * FROM `tbl_p_users` where `puser_id`='$puser_id'";
        // dd($sql);
        $result = app('db')->select($sql);

        $technician_certificate = array(
            'technician_certificate'=>$result[0]->technician_certificate,
        );
         $vehicle_make = array(
            'puser_veh_id'=>$result[0]->puser_veh_id,
        );

        if(!empty($result)){
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Technician";
            $output['data'] =  $result;
            $output['technician_certificate'] =  $technician_certificate;
            $output['vehicle_make'] =  $vehicle_make;
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "No Technician found";
            $output['data'] =  $result;
        }
        return response()->json($output);
    }
    
     public function getcontact(Request $request){
        $uid = $request->header('userid');
        $sql = "SELECT * from tbl_contact";
       
        $result = app('db')->select($sql);
        
        $app_type_user=array();
        $app_type_partner=array();
        
        foreach($result as $row){
            
            if($row->app_type=="user"){
                array_push($app_type_user, $row);
            }
            if($row->app_type=="partner"){
                 array_push($app_type_partner, $row);
            }
        }
        // dd($result);
            if(!empty($result)){
           // $output['data'] = $result;
            $output['status'] = true;
            $output['statuscode'] = 200;
            $output['msg'] =  "Contact Details";
            $output['data']['app_type_user']=$app_type_user;
            $output['data']['app_type_partner']=$app_type_partner;
        }else{
            $output['status'] = false;
            $output['statuscode'] = 404;
            $output['msg'] =  "Contact not Details";
        }
    
        return response()->json($output);
    }
}