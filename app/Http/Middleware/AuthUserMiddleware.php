<?php

namespace App\Http\Middleware;
use App\Models\tbl_m_users;
use Closure;

class AuthUserMiddleware
{
   
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
     
        if($request->order_middle == "5fc64c0b40aff6cdbe426a39XX2"){
             $usercount = Tbl_m_users::where('muser_id', $request->userid)
        ->where('m_token', $request->token)
        ->where('m_deviceid', $request->deviceid);
        
        }else{
             $usercount = Tbl_m_users::where('muser_id', $request->header('userid'))
        ->where('m_token', $request->header('token'))
        ->where('m_deviceid', $request->header('deviceid'));
        
        }
        
        if($usercount->count() == 1){
            return $next($request);
        }else if($usercount->count() == 0){
            // $output['Authorizedstatus'] = false;
            // $output['statuscode'] = 401;
            // $output['msg'] =  "Not Authorized";
            // return response()->json($output);
            return $next($request);
        }
    }

}
