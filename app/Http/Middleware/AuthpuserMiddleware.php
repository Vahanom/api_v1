<?php

namespace App\Http\Middleware;
use App\Models\tbl_p_users;
use Closure;

class AuthpuserMiddleware
{
   
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $cond = ['puser_id'=>$request->header('userid'),'puser_account_status'=>'Active'];
        $usercount = tbl_p_users::where($cond)
        ->where('puser_token', $request->header('token'))
        ->where('puser_deviceid', $request->header('deviceid'));
        if($usercount->count() == 1){
            return $next($request);
        }else if($usercount->count() == 0){
            // $output['Authorizedstatus'] = false;
            // $output['statuscode'] = 401;
            // $output['msg'] =  "Not Authorized";
            // return response()->json($output);
            return $next($request);
        }
    }

}
