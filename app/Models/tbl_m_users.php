<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class tbl_m_users extends Model 
{
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $primaryKey = 'muser_id ';
    protected $table = "tbl_m_users";
    protected $fillable = [
        'muser_id', 'muser_name', 'muser_email', 'muser_address', 'muser_pincode', 'muser_status', 'muser_otp', 'muser_otp_status','muser_createby', 'muser_createdate', 'muser_modifyby', 'muser_modifydate'
    ];
    public $timestamps = false;
}
