<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class tbl_p_users extends Model 
{
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected  $primaryKey = 'puser_id';
    protected $table = "tbl_p_users";
    protected $fillable = [
        'puser_name', 'puser_address','puser_pincode','puser_lat_long','puser_phno','puser_email','puser_adhar_number','puser_dl_number'
 , 'puser_pan_number', 'puser_GST_number','puser_bankacc_number','puser_bankacc_ifsc','puser_profile_image','puser_availability_status','puser_document_url','puser_company_reg_number','puser_account_status', 'puser_created_by','puser_created_date','puser_modify_date','puser_modify_by','puser_type','puser_long','technician_alternatephonenumber', 'idproof','puser_otp','puser_deviceid','puser_otp_status','puser_token'
];

    public $timestamps = false;
}
