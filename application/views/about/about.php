<!doctype html>
<html class="no-js" lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>About Us</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.html">


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>
<body>
<main>


<div class="container my-5 py-5 mt-40"></div> 
  <div class="container my-5 py-5 z-depth-1">


    <!--Section: Content-->
    <section class="dark-grey-text">

      <!--Grid row-->
      <div class="row">

        <!--Grid column-->
        <div class="col-md-6 mb-4 mb-md-0">

          <h3 class="font-weight-bold">Best mechanics from your community</h3>

          <p class="text-muted">On average our mechanics have over ten years of experience and most are master technicians with OEM Certifications.<br>We extensively screen all of our mechanics with background, criminal, and reference checks.<br>On each mechanic’s profile page, you’ll see a full list of all their certifications, years of experience, job skills, and feedback from real customers. We constantly monitor the performance of our mechanics to make sure they’ll provide you with professional and courteous service.<br>At Vahanom, you can trust the people who are working on your Vehicle</p>

          <!-- <a class="btn btn-purple btn-md ml-0" href="#" role="button">Start now<i class="fa fa-gem ml-2"></i></a> -->

          <hr class="my-5">

          <p class="font-weight-bold">Follow us on:</p>

          <!--Facebook-->
          <a href="#" class="mx-1 " role="button"><i class="fab fa-facebook-f text-danger"></i></a>
          <a href="#" class="mx-1" role="button"><i class="fab fa-twitter text-danger"></i></a>
          <a href="#" class="mx-1" role="button"><i class="fab fa-linkedin-in text-danger"></i></a>
          <a href="#" class="mx-1" role="button"><i class="fab fa-instagram text-danger"></i></a>
          <a href="#" class="mx-1" role="button"><i class="fab fa-pinterest text-danger"></i></a>
          <a href="#" class="mx-1" role="button"><i class="fab fa-youtube text-danger"></i></a>
          <a href="#" class="mx-1" role="button"><i class="fab fa-github text-danger"></i></a>
          <a href="#" class="mx-1" role="button"><i class="fab fa-stack-overflow text-danger"></i></a>

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-5 mb-4 mb-md-0">

          <img src="/assets/img/about/about.png" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->


    </section>
    <!--Section: Content-->


  </div>
<div class="container mt-5">


  <!--Section: Content-->
  <section class="team-section text-center dark-grey-text mt-150">

    <!-- Section heading -->
    <h3 class="font-weight-bold mb-4 pb-2">Meet some of our mechanics</h3>
    <!-- Section description -->
    <!-- <p class="text-center w-responsive mx-auto mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam
      eum porro a pariatur veniam.</p>
 -->
  	<!--Grid row-->
    <div class="row text-center">

      <!--Grid column-->
      <div class="col-md-4 mb-4">

        <div class="testimonial">
          <!--Avatar-->
          <div class="avatar mx-auto">
            <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(1).jpg" class="rounded-circle z-depth-1 img-fluid">
          </div>
          <!--Content-->
          <h4 class="font-weight-bold dark-grey-text mt-4">Rocco</h4>
          <h6 class="font-weight-bold blue-text my-3">22 Years of experience</h6>
          <p class="font-weight-normal dark-grey-text">
            <i class="fas fa-quote-left pr-2"></i>Rocco has worked at Volvo, Honda, Acura, Lexus and Toyota. He has 22 years of experience and specializes in diagnostics.</p>
            <!-- <button class="btn btn-primary">See Profile</button> -->
          <!--Review-->
          <div class="orange-text">
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="far fa-star"> </i>
          </div>
        </div>

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-md-4 mb-4">

        <div class="testimonial">
          <!--Avatar-->
          <div class="avatar mx-auto">
            <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(8).jpg" class="rounded-circle z-depth-1 img-fluid">
          </div>
          <!--Content-->
          <h4 class="font-weight-bold dark-grey-text mt-4">Robert</h4>
          <h6 class="font-weight-bold blue-text my-3">31 years of experience</h6>
          <p class="font-weight-normal dark-grey-text">
            <i class="fas fa-quote-left pr-2"></i> A BMW, Nissan & Toyota certified mechanic with over 30 years of experience. Robert loves to work on all types of cars.</p>
          <!--Review-->
          <div class="orange-text">
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="far fa-star"> </i>
          </div>
        </div>

      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-md-4 mb-4">

        <div class="testimonial">
          <!--Avatar-->
          <div class="avatar mx-auto">
            <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg" class="rounded-circle z-depth-1 img-fluid">
          </div>
          <!--Content-->
          <h4 class="font-weight-bold dark-grey-text mt-4">Grgegorz</h4>
          <h6 class="font-weight-bold blue-text my-3">41 Years of experience</h6>
          <p class="font-weight-normal dark-grey-text">
            <i class="fas fa-quote-left pr-2"></i>Grgegorz grew up in Poland. He has an extensive training in European cars. He is also a dignostic pro. </p>
          <!--Review-->
          <div class="orange-text">
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="fas fa-star"> </i>
            <i class="far fa-star"> </i>
          </div>
        </div>

      </div>
      <!--Grid column-->

    </div>
    <!--Grid row-->

  </section>
  <!--Section: Content-->


</div>
</main>


<?php include('/home/vahanomvpa/public_html/application/views/layout/footer.php');?>


<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

</body>

</html>