<!doctype html>
<html class="no-js" lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Supplier Registration</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.html">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>
<body>

<!--<div id="preloader-active">
<div class="preloader d-flex align-items-center justify-content-center">
<div class="preloader-inner position-relative">
<div class="preloader-circle"></div>
<div class="preloader-img pere-text">
<img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt="">
</div>
</div>
</div>
</div>

<style type="text/css">
	.header-area{
		padding: 39px 16px;
	}

	.sticky-bar{
		padding: 20px 16px;
	}
</style>

<header>

<div class="header-area header-transparrent ">
	<div class="main-header header-sticky">
		<div class="container">
			<div class="row align-items-center" >

				<div class="col-xl-2 col-lg-2 col-md-2">
					<div class="logo">
						<a href="index-2.html"><img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-12">
					<div class="mobile_menu d-block d-lg-none"></div>
				</div>
			</div>
		</div>
	</div>
</div>

</header>-->
<?php  //$this->load->view('./layout/navbar'); ?>
<?php //include('layout/navbar.php');?>
<main> 



<div class="applic-apps section-padding2">
	<div class="container-fluid" >

		<div class="row justify-content-center">
			<div class="col-lg-10">
				<div class="card">
					<div class="card-body">
						<h3 class="mb-3" style="color:#ed1c24;">Supplier Registration : </h3>
						<form method="post" action="<?php echo  base_url('Supplier/add'); ?>" enctype="multipart/form-data"> 
							<div class="row justify-content-center">
								<div class="col-lg-4">
								  <label>Supplier Name:</label>
								   <input type="text" required name="supplier_name" class="single-input-secondary" id="supplier_name" placeholder="Enter Name" />
								</div>
								<div class="col-lg-4">
									<label>Supplier Email:</label>
									<input type="email" required name="supplier_email" class="single-input-secondary" onchange="duplicatecheckemail();" id="supplier_email" placeholder="Enter Email" />
								</div>
								<div class="col-lg-4">
									<label>Adhar Number:</label>
									<input type="text" required name="supplier_aadhar" onchange="adharcheck();" class="single-input-secondary" id="supplier_aadhar" placeholder="Enter Adhar Number" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>Pan Number:</label>
									<input type="text" required name="supplier_pan" class="single-input-secondary" onchange="validatepannumber();" id="supplier_pan" placeholder="Enter Pan Number" />
								</div>
								<div class="col-lg-4">
									<label>Address:</label>
									<textarea cols="40" id="supplier_address" class="single-input-secondary" name="supplier_address" rows="" placeholder="Enter Address"></textarea>
								</div>
								<div class="col-lg-4">
									<label>Pincode:</label>
									<input type="number" required name="supplier_pincode" class="single-input-secondary" onchange="pincodevalidation();" id="supplier_pincode" placeholder="Enter Pincode" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>Contact Number:</label>
									<input type="number" class="single-input-secondary" onchange="duplicatecheck()" required name="supllier_phonenumber" id="supllier_phonenumber"  placeholder="Enter Phone Number" />
								</div>
								<div class="col-lg-4">
									 <label>Latitude:</label>
									 <input type="number" class="single-input-secondary" required name="latlong" id="latlong"  placeholder="Enter lat long" />
								</div>
								<div class="col-lg-4">
									 <label>Bank Account Number:</label>
									<input type="number" class="single-input-secondary" min="9999999999" max="999999999999999999999999999999" required name="supllier_bankacc" id="supllier_bankacc"  placeholder="Enter Bank Account Number" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>Profile Picture:</label>
									<input type="file"  accept="image/x-png,image/jpeg" required name="imageone" class="single-input-secondary" id="imageone" />
								</div>
								<div class="col-lg-4">
									 <label>Company Registration Number:</label>
									  <input type="text" class="single-input-secondary" maxlength="30"  minlength="10"  required name="companyregnumber" id="companyregnumber"  placeholder="Enter Company Registration Number" />
								</div>
								<div class="col-lg-4">
									 <label>GST Number:</label>
									  <input type="text"  pattern=".*\S+.*" onchange="validategstnumber();" class="single-input-secondary" required name="gstnumber" id="gstnumber"  placeholder="Enter GST Number" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>Select Status:</label>
														   <select id="status" required class="single-input-secondary" name="status">
															<option value="">Select Status</option>
															<option value="Active">Active</option>
															<option value="Inactive">Inactive</option>
															</select>
								</div>
								<div class="col-lg-4">
									 <label>Longitude:</label>
									<input type="number" class="single-input-secondary" required name="long" id="long"  placeholder="Enter long" />
								</div>
								<div class="col-lg-4">
									<label>ID Proof:</label>
									<input type="file"  accept="image/x-png,image/jpeg" required name="imagetwo" class="single-input-secondary" id="imagetwo" />
								</div>
							</div>

							<!--<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<input type="checkbox" onchange="displaytime('monday','monday1');" id="monday" >

												   <label>Monday:</label>
												   <div  id="monday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="mondaystarttime" id="mondaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="mondayendtime" id="mondayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div><br/>

												   <input type="checkbox" onclick="displaytime('Tuesday','Tuesday1')" id="Tuesday" >

												   <label>Tuesday:</label>
												   <div  id="Tuesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Tuesdaystarttime" id="Tuesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Tuesdayendtime" id="Tuesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															</div><br/>


													 <input type="checkbox" onclick="displaytime('Wednesday','Wednesday1')" id="Wednesday" >

												   <label>Wednesday:</label>
												   <div  id="Wednesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Wednesdaystarttime" id="Wednesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Wednesdayendtime" id="Wednesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div> </div><br/>

												    <input type="checkbox"  onclick="displaytime('Thursday','Thursday1')" id="Thursday">

												   <label>Thursday:</label>
												   <div  id="Thursday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Thursdaystarttime" id="Thursdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Thursdayendtime" id="Thursdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>	<br/>



												   <input type="checkbox" onclick="displaytime('Friday','Friday1')" id="Friday">

												   <label>Friday:</label>
												   <div  id="Friday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Fridaystarttime" id="Fridaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Fridayendtime" id="Fridayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div><br/>

												  <input type="checkbox" onclick="displaytime('Saturday','Saturday1')" id="Saturday" >

												   <label>Saturday:</label>
												   <div  id="Saturday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Saturdaystarttime" id="Saturdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Saturdayendtime" id="Saturdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div><br/>


												<div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Sunday','Sunday1')" id="Sunday">

												   <label>Sunday:</label>
												   <div  id="Sunday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Sundaystarttime" id="Sundaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Sundayendtime" id="Sundayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div><br/>-->

												<div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onchange="displaytime('monday','monday1');" id="monday" >

												   <label>Monday:</label>
												   <div  id="monday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="mondaystarttime" id="mondaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="mondayendtime" id="mondayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Tuesday','Tuesday1')" id="Tuesday" >

												   <label>Tuesday:</label>
												   <div  id="Tuesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Tuesdaystarttime" id="Tuesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Tuesdayendtime" id="Tuesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															</div>
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Wednesday','Wednesday1')" id="Wednesday" >

												   <label>Wednesday:</label>
												   <div  id="Wednesday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Wednesdaystarttime" id="Wednesdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Wednesdayendtime" id="Wednesdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div> </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox"  onclick="displaytime('Thursday','Thursday1')" id="Thursday">

												   <label>Thursday:</label>
												   <div  id="Thursday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Thursdaystarttime" id="Thursdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Thursdayendtime" id="Thursdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>		
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Friday','Friday1')" id="Friday">

												   <label>Friday:</label>
												   <div  id="Friday1" style="display:none">
												   <div class="row">
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Fridaystarttime" id="Fridaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Fridayendtime" id="Fridayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Saturday','Saturday1')" id="Saturday" >

												   <label>Saturday:</label>
												   <div  id="Saturday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Saturdaystarttime" id="Saturdaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Saturdayendtime" id="Saturdayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
															
												   </div>
												   </div>
												   <div class="form-group row">
												   <div class="col-lg-12">
												   <input type="checkbox" onclick="displaytime('Sunday','Sunday1')" id="Sunday">

												   <label>Sunday:</label>
												   <div  id="Sunday1" style="display:none">
												   <div class="row" >
												   <div class="col-lg-6">
												   <input type="time"   class="form-control"  name="Sundaystarttime" id="Sundaystarttime"  placeholder="Enter GST Number" />
														   
															 </div>
															
												 
													   <div class="col-lg-6">
														   <input type="time" class="form-control"  name="Sundayendtime" id="Sundayendtime"  placeholder="Enter GST Number" />
														  
															 </div>
															 </div>
															 </div>
												   </div>
												   </div>
								</div>
								<div class="col-lg-4">
									
								</div>
								<div class="col-lg-4">
									
								</div>
							</div>


							

							

							<div class="row mt-2 justify-content-center">
								<div class="col-lg-12 text-center">
									<div class="button-group-area mt-10">
										<!--<a href="#" class="genric-btn primary-border" id="submit" >Submit</a>-->
										<button type="submit" class="btn btn-primary mr-2">Save</button>
									</div>
								</div>
							</div>



						</form>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>




</main>
<?php include('/home/vahanomvpa/public_html/application/views/layout/footer.php');?>

<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
  

	$('#submit').click(function(){
	 	alert("Thank you for your registration, our team will get back to you shortly.")
	});

</script>
<script type="text/javascript">
	function displaytime(value,value1){
	var checkBox = document.getElementById(value);
// alert(''+value+'starttime');
	if (checkBox.checked == true){
		document.getElementById(value1).style.display = "block";
		document.getElementById(''+value+'starttime').setAttribute("required","");
		document.getElementById(''+value+'endtime').setAttribute("required","");
		// document.getElementById(''+value+'starttime').setAttribute("required ", "");
  } else {
	document.getElementById(''+value+'starttime').required = false;

	document.getElementById(''+value+'endtime').required = false;
	document.getElementById(value1).style.display = "none";

  }

}
</script>
 <script>
					
					function check(){
						// alert('sssss');
	// duplicatecheck();
	validatepannumber();
	// duplicatecheckemail();	
	adharcheck();	
	pincodevalidation();	
	validategstnumber();
	//   validatetime();	
}
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("supplier_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("supplier_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
						
				function duplicatecheck(){
	var value=document.getElementById("supllier_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("supllier_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Supplier/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("supllier_phonenumber").value='';
				return false;
			}else{
				return true;
			}
        }
    });
}

function duplicatecheckemail(){
	var value=document.getElementById("supplier_email").value;
	
 $.ajax({
	url: websiteurl + 'Supplier/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("supplier_email").value='';
				return false;
			}else{
				return true;
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("supplier_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	    return true;
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("supplier_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("supplier_pincode").value;
	
	var phoneno = /^\(?([2-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
		return true;
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("supplier_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
	// console.log(return !!(validatetime() & duplicatecheck() & duplicatecheckemail() & validategstnumber()));
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }
						function displaytime(value,value1){
	var checkBox = document.getElementById(value);
// alert(''+value+'starttime');
	if (checkBox.checked == true){
		document.getElementById(value1).style.display = "block";
		document.getElementById(''+value+'starttime').setAttribute("required","");
		document.getElementById(''+value+'endtime').setAttribute("required","");
		// document.getElementById(''+value+'starttime').setAttribute("required ", "");
  } else {
	document.getElementById(''+value+'starttime').required = false;

	document.getElementById(''+value+'endtime').required = false;
	document.getElementById(value1).style.display = "none";

  }

}

function validatetime(){
	var monday1 = document.getElementById("monday1").style.display;
	var Tuesday1 = document.getElementById("Tuesday1").style.display;
	var Wednesday1 = document.getElementById("Wednesday1").style.display;
	var Thursday1 = document.getElementById("Thursday1").style.display;
	var Friday1 = document.getElementById("Friday1").style.display;
	var Saturday1 = document.getElementById("Saturday1").style.display;
	var Sunday1 = document.getElementById("Sunday1").style.display;

    if(monday1=='block'){
		var mstarttime = document.getElementById("mondaystarttime").value;
		var mendtime = document.getElementById("mondayendtime").value;
if(mstarttime>=mendtime){
	alert('Monday Start Time and End Time should be Greater');
	document.getElementById("mondaystarttime").value='';
	document.getElementById("mondayendtime").value='';
	return false;
}

    }else{
		return true;
	}


	if(Tuesday1=='block'){
		var Tuesdaystarttime = document.getElementById("Tuesdaystarttime").value;
		var Tuesdayendtime = document.getElementById("Tuesdayendtime").value;
if(Tuesdaystarttime>=Tuesdayendtime){
	alert('Tuesday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Wednesday1=='block'){
		var Wednesdaystarttime = document.getElementById("Wednesdaystarttime").value;
		var Wednesdayendtime = document.getElementById("Wednesdayendtime").value;
if(Wednesdaystarttime>=Wednesdayendtime){
	alert('Wednesday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Thursday1=='block'){
		var Thursdaystarttime = document.getElementById("Thursdaystarttime").value;
		var Thursdayendtime = document.getElementById("Thursdayendtime").value;
if(Thursdaystarttime>=Thursdayendtime){
	alert('Thursday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Friday1=='block'){
		var Fridaystarttime = document.getElementById("Fridaystarttime").value;
		var Fridayendtime = document.getElementById("Fridayendtime").value;
if(Fridaystarttime>=Fridayendtime){
	alert('Friday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}
	if(Saturday1=='block'){
		var Saturdaystarttime = document.getElementById("Saturdaystarttime").value;
		var Saturdayendtime = document.getElementById("Saturdayendtime").value;
if(Saturdaystarttime>=Saturdayendtime){
	alert('Saturday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}

	if(Sunday1=='block'){
		var Sundaystarttime = document.getElementById("Sundaystarttime").value;
		var Sundayendtime = document.getElementById("Sundayendtime").value;
if(Sundaystarttime>=Sundayendtime){
	alert('Sunday Start Time and End Time should be Greater');
	return false;
}

    }else{
		return true;
	}

}
function alphanumeric(inputtxt)
{ 
	// alert(inputtxt);
var letters = /^[0-9a-zA-Z]+$/;

if(inputtxt.match(letters))
{
// alert('Your registration number have accepted : you can try another');
// document.form1.text1.focus();
return true;
}
else
{
  alert('Please input alphanumeric characters only');
  document.getElementById("ifscnumber").value='';
return false;
}
}
//hide items fron header
$(document).ready(function(){
 $("#Feature").hide();
 $("#Contact").hide();
  $(".contact_class").hide();
 $(".feature_class").hide();
});
						</script>
</body>
</html>