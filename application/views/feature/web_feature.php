<!doctype html>
<html class="no-js" lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Technician Registration</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.html">


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>
<body>

<!--<div id="preloader-active">
<div class="preloader d-flex align-items-center justify-content-center">
<div class="preloader-inner position-relative">
<div class="preloader-circle"></div>
<div class="preloader-img pere-text">
<img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt="">
</div>
</div>
</div>
</div>

<style type="text/css">
	.header-area{
		padding: 39px 16px;
	}

	.sticky-bar{
		padding: 20px 16px;
	}
</style>

<header>

<div class="header-area header-transparrent ">
	<div class="main-header header-sticky">
		<div class="container">
			<div class="row align-items-center" >

				<div class="col-xl-2 col-lg-2 col-md-2">
					<div class="logo">
						<a href="index-2.html"><img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-12">
					<div class="mobile_menu d-block d-lg-none"></div>
				</div>
			</div>
		</div>
	</div>
</div>

</header>-->
<section class="service-area sky-blue section-padding2" style="padding-bottom:5px;padding-top: 150px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-top:0;margin-bottom:0;">Don't go anywhere. We <br>come to you</h2>
<p>
Avoid the hassle of towing your vehicle to the garage and paying hefty sums to tow trucks. Get a mechanic right where your wheels break down. Vahanom is available for you every day at the regular time to get you on the move in your own vehicle. Our vehicle doctors bring all the tools and parts you describe to help you out in the first place. All you need is patience and some space for our mechanics to work.
</p>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption height-600 text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/calendar.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">The day before your appointment</a></h4>
<p>you will receive an email reminder from us on the day before your appointment. This reminder email is to help you remember your scheduled appointment day and timing. </p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption height-600 active text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/appointment.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">The appointment day</a></h4>
<p>You will get a text and email along with mechanic details to navigate him to your location. Avoid turning your vehicle on half an hour before the meet to keep the engine cool. Our mechanic will come to your place in 30 minutes.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption height-600 text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/meeting.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">During your appointment
</a></h4>
<p>You can ask any questions about your vehicle to our mechanics. We also help our customers with replacing and disposing of worn-out parts. Once our vehicle doctors complete the service, we initiate our 50-point inspection that you receive in your email.</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="service-area sky-blue section-padding2" style="padding-bottom:30px;padding-top:30px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-top:0;margin-bottom:0;">Save your receipts and service records online</h2><br>
<p>
Halt stuffing your file cabinets with paper receipts and your vehicle repair records. 
Why create a space for vehicle repair records when you can save them online? With Vahanom, you get the top-notch servicing of your vehicle on your doorstep at your convenient time while getting a free 50-point inspection of your drive stored in your app account. It includes all safety checks and essential safety items health-check like fluid levels, belts, brakes, engine codes, filters, tire treads, and more. Save the maintenance history of your vehicle so that when selling your wheels, you don't have to ram down its price and share your service records with the buyer.
</p>

</div>
</div>
</div>
</div>

</section>

<!--<section class="service-area sky-blue section-padding2" style="padding-bottom:30px;padding-top:30px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-6">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0;">Get maintenance reminders</h2>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">Digital reminders, not window stickers REGULAR MAINTENANCE MADE EASY</a></h4>
<p>Stay on schedule, your way. Get maintenance reminders via email or text. We’ll send you reminders when it’s time to do your next oil or your scheduled service.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">QUICK EASY BOOKING</a></h4>
<p>From reminders to booking, it takes less than 5 minutes. Simply open the link in the reminder, select a time and book your next maintenance. A highly rated mechanic will come to you to perform the service. Taking care of your Vehicle has never been easier. And as they say, prevention is better than care.
</p>
</div>
</div>
</div>
</div>
</div>
</section>-->

<!-- get maintance remindder-->
<div class="container-fluid" style="padding: 50px;background-color: #f9fafc">
   <div class="section-tittle text-center">
        <h2>Get maintenance reminders</h2><br>
   </div>
    <div class="row">
        <div class="col-lg-6">
           <div id="accordion2">
             <div class="card">
             <div class="card-header" id="headingOne" style="padding-left:30px">
                <h5 class="mb-0">
                   <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="true" aria-controls="collapseOne" style="height:100px; width:600px">
                  Digital reminders
                   </button>
                </h5>
              </div>
                 <div id="collapseOne2" class="collapse" aria-labelledby="headingOne" data-parent="#accordion2">
                 <div class="card-body">
                       Stay on schedule, your way. Get maintenance reminders via email or text. We’ll send you reminders when it’s time to do your next oil or your scheduled service.
                  </div>
                  </div>
            </div>
           </div>
        </div>

        <div class="col-lg-6">
           <div id="accordion1">
             <div class="card">
             <div class="card-header" id="headingOne" style="padding-left:30px">
                <h5 class="mb-0">
                   <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne" style="height:100px; width:600px">
                  
QUICK EASY BOOKING

                   </button>
                </h5>
              </div>
                 <div id="collapseOne1" class="collapse" aria-labelledby="headingOne" data-parent="#accordion1">
                 <div class="card-body">
                   From reminders to booking, it takes less than 5 minutes. Simply open the link in the reminder, select a time and book your next maintenance. A highly rated mechanic will come to you to perform the service. Taking care of your Vehicle has never been easier. And as they say, prevention is better than care.
                  </div>
                  </div>
            </div>
           </div>
      </div>

    </div>
</div>
<!-- -->

<section class="service-area sky-blue " style="padding-bottom:30px;padding-top:30px;">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0">Manage all your automobiles at once</h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption height-600 text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/online-test.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Manage all your Vehicles online</a></h4>
<p>With your free account on the Vahanom app, you can register all your vehicles for service. Get fair and transparent pricing, schedule appointments, and see your wheel's entire service history. 
</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption height-600 active text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/manufacturing.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Get manufacturer-advised maintenance schedule
</a></h4>
<p>We show you the manufacturer-recommended servicing schedule for your wheels. You don't have to open your automobile's manual to know the time-table of servicing.
</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption height-600 text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/car.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Own more than one vehicle? </a></h4>
<p>Add all your vehicles and manage them all at once through your free account at Vahanom. We will keep a service record of each of your wheels, be it two-wheelers or four-wheelers.
</p>
</div>
</div>
</div>
</div>
 
<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption height-600 text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/24-hours.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Get 24/7 access to your service records</a></h4>
<p>Don't juggle with messy paperwork. Collect all your vehicle data in one place. Once we service your wheels, we send you a digital copy of the receipt. You can access it anytime from anywhere by logging in to your account.</p>
</div>
</div>
</div>

</div>
</div>
</section>



<?php include('/home/vahanomvpa/public_html/application/views/layout/footer.php');?>


<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');

  	$('#submit').click(function(){
	 	alert("Thank you for your registration, our team will get back to you shortly.")
	});
</script>
 <script>


$(document).ready(function(){
 $("#Feature").hide();
 $("#Contact").hide();
 $(".contact_class").hide();
 $(".feature_class").hide();
});
						</script>
</body>
</html>