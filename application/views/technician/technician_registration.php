<html class="no-js" lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Technician Registration</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.html">


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>
<body>

<!--<div id="preloader-active">
<div class="preloader d-flex align-items-center justify-content-center">
<div class="preloader-inner position-relative">
<div class="preloader-circle"></div>
<div class="preloader-img pere-text">
<img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt="">
</div>
</div>
</div>
</div>

<style type="text/css">
	.header-area{
		padding: 39px 16px;
	}

	.sticky-bar{
		padding: 20px 16px;
	}
</style>

<header>

<div class="header-area header-transparrent ">
	<div class="main-header header-sticky">
		<div class="container">
			<div class="row align-items-center" >

				<div class="col-xl-2 col-lg-2 col-md-2">
					<div class="logo">
						<a href="index-2.html"><img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-12">
					<div class="mobile_menu d-block d-lg-none"></div>
				</div>
			</div>
		</div>
	</div>
</div>

</header>-->
<?php // $this->load->view('./layout/navbar'); ?>
<!-- Life to shoet start.. -->
<section class="service-area sky-blue section-padding2">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="padding-top:12px;margin-bottom:0">Professional Automotive <br> Technicians!</h2><br>
<p>Be Your Own Boss & Work with the #1 Mobile Mechanic Company in the Country</p>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">FLEXIBLE HOURS</a></h4>
<p>Work evenings, weekends, or full-time -- you decide!</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">GREAT PAY</a></h4>
<p>Make rupee150-400 hourly -- 2-3x what shops & dealers pay you.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-plane"></span>
</div>
<div class="service-cap">
<h4><a href="#">BE YOUR OWN BOSS</a></h4>
<p>You choose the type of jobs, where to work, and when to work. No politics or managers!</p>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- lide to short end-->




<!-- Life to shoet start.. -->
<section class="service-area sky-blue ">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0">Why are thousands of mechanics joining us?</h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">CHOOSE YOUR SCHEDULE</a></h4>
<p>One day a week? 5 days a week? Just evenings? Weekends only? You have total control. You can simply use our Android app to set your own hours and we will book you appointments based on your availability.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">FAIR PAY</a></h4>
<p>Our mechanics make anywhere between 150-400 an hour based on their location, skills and level of experience. We pay flat rate and we don’t do any warranty work. You get paid fairly.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-plane"></span>
</div>
<div class="service-cap">
<h4><a href="#">NO COMPLEX JOBS</a></h4>
<p>We focus on basic repair and maintenance work -- brakes, tire rotations, timing belts, 30/60/90k service, alternators, A/C, etc. No engine overhauls or major transmission work.</p>
</div>
</div>
</div>
</div>
 <!-- second -->
<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">FOCUS ON WHAT YOU LOVE</a></h4>
<p>We handle marketing, booking appointments, delivering parts, buying insurance and providing support. You do what you love - fix Vehicle s. We take care of the rest.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">WIN-WIN FOR EVERYONE</a></h4>
<p>When you work directly with Vehicle owners in your neighbourhood, you can make more money with no commute. Plus, Vehicle owners enjoy the convenience. Everyone wins.</p>
</div>
</div>
</div>

</div>
</div>
</section>
<!-- lide to short end-->

<!-- Life to shoet start.. -->
<section class="service-area sky-blue section-padding2">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0">Joining Vahanom is as easy as 1-2-3</h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">EASY ONLINE APPLICATION</a></h4>
<p>It takes less than 10 minutes to fill out the entire application. If you have questions, call us at</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">PHONE INTERVIEW & ONBOARDING</a></h4>
<p>A service advisor will call you to interview you and provide more information about how Vahanom works.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-plane"></span>
</div>
<div class="service-cap">
<h4><a href="#">START WORKING</a></h4>
<p>Make rupees 100-400 an hour based on your skills and experience. Set your own hours -- choose evenings, weekends, or full-time.</p>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- lide to short end-->
<main> 

<div class="applic-apps section-padding2">
	<div class="container-fluid" >

		<div class="row justify-content-center">
			<div class="col-lg-10">
				<div class="card">
					<div class="card-body">
						<h3 class="mb-3" style="color:#ed1c24;">Technician Registration : </h3>
						<form method="post" action="<?php echo  base_url('Technician/add'); ?>" enctype="multipart/form-data" >
							<div class="row justify-content-center">
								<div class="col-lg-4">
									<label>Technician Name:</label>
									<input type="text" required name="technician_name" class="single-input-primary" id="technician_name" placeholder="Enter Name" />
									
								</div>
								<div class="col-lg-4">
									<label>Technician Email:</label>
									<input type="email" required name="technician_email" class="single-input-primary" id="technician_email" placeholder="Enter Email" />
								</div>
								<div class="col-lg-4">
									<label>Adhar Number:</label>
									<input type="text" required name="technician_aadhar" onchange="adharcheck();" class="single-input-primary" id="technician_aadhar" placeholder="Enter Adhar Number" />
								</div>
								
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>Pan Number:</label>
									<input type="text" required name="technician_pan" onchange="validatepannumber();" class="single-input-secondary" id="technician_pan" placeholder="Enter Pan Number" />
								</div>
								<div class="col-lg-4">
									<label>Address:</label>
									<textarea cols="40" id="technician_address" class="single-input-secondary"  name="technician_address" rows="2" placeholder="Enter Address"></textarea>
								</div>
								<div class="col-lg-4">
									<label>Pincode:</label>
									<input type="number" required name="technician_pincode" onchange="pincodevalidation();" class="single-input-secondary" id="technician_pincode" placeholder="Enter Pincode" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									 <label>Contact Number:</label>
									 <input type="number" class="single-input-secondary" onchange="duplicatecheck()" required name="technician_phonenumber" id="technician_phonenumber"  placeholder="Enter Phone Number" />
								</div>
								<div class="col-lg-4">
									<label>Alternate Contact Number:</label>
									<input type="number" class="single-input-secondary" onchange="duplicatecheckalter()" name="technician_alternatephonenumber" id="technician_alternatephonenumber"  placeholder="Enter Phone Number" />
								</div>
								<div class="col-lg-4">
									<label>Bank Account Number:</label>
									<input type="number" class="single-input-secondary" min="9999999999" max="999999999999999999999999999999" required name="technician_bankacc" id="technician_bankacc"  placeholder="Enter Bank Account Number" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>IFSC number:</label>
									<input type="text" class="single-input-secondary" onkeyup="alphanumeric(this.value)"  maxlength="11" required name="ifscnumber" id="ifscnumber"  placeholder="Enter IFSC Number" />
								</div>
								<div class="col-lg-4">
									<label>Profile Picture:</label>
									<input type="file"  accept="image/x-png,image/jpeg" required name="imageone" class="single-input-secondary" id="imageone" />
								</div>
								<div class="col-lg-4">
									 <label>Upload CV:</label>
									 <input type="file"   accept=".pdf,.doc,.docx" required name="imagetwo" class="single-input-secondary" id="imagetwo" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>GST Number:</label>
									<input type="text"  pattern=".*\S+.*" onchange="validategstnumber();" class="single-input-secondary" required name="gstnumber" id="gstnumber"  placeholder="Enter GST Number" />
								</div>
								<div class="col-lg-4">
									<label>Select Status:</label>
														   <select id="status" required class="single-input-secondary" name="status">
															<option value="">Select Status</option>
															<option value="Active">Active</option>
															<option value="Inactive">Inactive</option>
															</select>
								</div>
								<div class="col-lg-4">
									 <label>Latitude:</label>
									 <input type="number" class="single-input-secondary" required name="latlong" id="latlong"  placeholder="Enter lat" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>Longitude:</label>
									<input type="number" class="single-input-secondary" required name="long" id="long"  placeholder="Enter long" />
								</div>
								<div class="col-lg-4">
									<label>I want to work:</label>
														   <select id="working_type" required class="single-input-secondary" name="working_type">
															<option value="">Select Status</option>
															<option value="Part Time">Part Time</option>
															<option value="Full Time">Full Time</option>
															</select>
								</div>
								<div class="col-lg-4">
									<label>Distance:</label>
									<input type="text" required name="technician_order_distance" class="single-input-secondary" id="technician_order_distance" placeholder="Enter Order Distance" />
								</div>
							</div>

							<div class="row justify-content-center mt-2">
								<div class="col-lg-4">
									<label>Certifications I have obtained:</label></br>
														  <input type="checkbox" name="technician_certificate[]" value="ASEA1">ASE A1</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA2">ASE A2</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA3">ASE A3</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA4">ASE A4</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA5">ASE A5</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA6">ASE A6</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA7">ASE A7</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA8">ASE A8</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEA9">ASE A9</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="ASEL1">ASE L1</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="Epa609">Epa609</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="AirBag">Air Bag</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="MACS A/C">MACS A/C</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="SmogLicensed">Smog Licensed</br>
                                                          <input type="checkbox" name="technician_certificate[]" value="Brake and Lamp Certified">Brake and Lamp Certified</br>
								</div>
								<div class="col-lg-4">
									<label>How Many Hours per week:</label>
									<input type="text" required name="weekly_available_hours" class="single-input-secondary" id="weekly_available_hours" placeholder="Enter Weekly available hours" />
								</div>
								<div class="col-lg-4">
									
								</div>
								
							</div>



							

							<div class="row mt-2 justify-content-center">
								<div class="col-lg-12 text-center">
									<div class="button-group-area mt-10">
										<!--<a href="#" class="genric-btn primary-border" id="submit">Submit</a>-->
										<button type="submit" class="btn btn-primary mr-2">Save</button>
									</div>
								</div>
							</div>



						</form>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>




</main>
<?php include('/home/vahanomvpa/public_html/application/views/layout/footer.php');?>



<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');

  	$('#submit').click(function(){
	 	alert("Thank you for your registration, our team will get back to you shortly.")
	});
</script>
 <script>
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
						function alphanumeric(inputtxt)
{ 
	// alert(inputtxt);
var letters = /^[0-9a-zA-Z]+$/;

if(inputtxt.match(letters))
{
// alert('Your registration number have accepted : you can try another');
// document.form1.text1.focus();
return true;
}
else
{
  alert('Please input alphanumeric characters only');
  document.getElementById("ifscnumber").value='';
return false;
}
}
				function duplicatecheck(){
	var value=document.getElementById("technician_phonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_phonenumber").value='';
	   return false;
     }


 $.ajax({
	url: websiteurl + 'Technician/phonenocheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Phone Number is Already exists');
				document.getElementById("technician_phonenumber").value='';
			}
        }
    });
}
function duplicatecheckalter(){
	var value=document.getElementById("technician_alternatephonenumber").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("technician_alternatephonenumber").value='';
	   return false;
     }


}
function duplicatecheckemail(){
	var value=document.getElementById("technician_email").value;
	
 $.ajax({
	url: websiteurl + 'Technician/emailcheckexist',
        type: 'POST',
        data: {
            key: value
        },
        dataType: 'json',
        success: function(data) {
		
			if(data==2){
				alert('Email is Already exists');
				document.getElementById("technician_email").value='';
			}
        }
    });
}

function adharcheck(){
	var value=document.getElementById("technician_aadhar").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{6})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Adhar Number");
	   document.getElementById("technician_aadhar").value='';
	   return false;
     }
}

function pincodevalidation(){
	var value=document.getElementById("technician_pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("technician_pincode").value='';
	   return false;
     }
}
function validategstnumber(){
                            var cgstnumber = document.getElementById("gstnumber").value;
                            if(cgstnumber==""){
                            return true;
                        }else{
                            var regpan = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
                            if(regpan.test(cgstnumber) == false)
                            {
								alert("Not a valid GST Number");
	   document.getElementById("gstnumber").value='';
                                   
                                    return false;
                                }else{
                                return true;
                                }
                            }
                        }
$(document).ready(function(){
 $("#Feature").hide();
 $("#Contact").hide();
 $(".contact_class").hide();
 $(".feature_class").hide();
});
						</script>
</body>

</html>