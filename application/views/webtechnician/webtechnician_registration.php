<!doctype html>
<html class="no-js" lang="zxx">

<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Technician Registration</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.html">


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<style>
	.service-area .services-caption {
		height: 90% !important;
	}
</style>
<style>
	.service-area .services-caption {
		height: 90% !important;
	}
      /* Steps */
      .step {
        list-style: none;
        margin: 0;
      }

      .step-element {
        display: flex;
        padding: 1rem 0;
      }

      .step-number {
        position: relative;
        width: 7rem;
        flex-shrink: 0;
        text-align: center;
      }

      .step-number .number {
        color: #bfc5ca;
        background-color: #eaeff4;
        font-size: 1.5rem;
      }

      .step-number .number {
        width: 48px;
        height: 48px;
        line-height: 48px;
      }

      .number {
        display: inline-flex;
        justify-content: center;
        align-items: center;
        width: 38px;
        border-radius: 10rem;
      }

      .step-number::before {
        content: '';
        position: absolute;
        left: 50%;
        top: 48px;
        bottom: -2rem;
        margin-left: -1px;
        border-left: 2px dashed #eaeff4;
      }

      .step .step-element:last-child .step-number::before {
        bottom: 1rem;
      }
      .nice-select{
          display:none !important;
      }
      #reference{
          display:block !important;
          cursor: pointer;
      }
    </style>

</head>
<body>

<!--<div id="preloader-active">
<div class="preloader d-flex align-items-center justify-content-center">
<div class="preloader-inner position-relative">
<div class="preloader-circle"></div>
<div class="preloader-img pere-text">
<img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt="">
</div>
</div>
</div>
</div>

<style type="text/css">
	.header-area{
		padding: 39px 16px;
	}

	.sticky-bar{
		padding: 20px 16px;
	}
</style>

<header>

<div class="header-area header-transparrent ">
	<div class="main-header header-sticky">
		<div class="container">
			<div class="row align-items-center" >

				<div class="col-xl-2 col-lg-2 col-md-2">
					<div class="logo">
						<a href="index-2.html"><img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-12">
					<div class="mobile_menu d-block d-lg-none"></div>
				</div>
			</div>
		</div>
	</div>
</div>

</header>-->
<?php // $this->load->view('./layout/navbar'); ?>



<!-- Life to shoet start.. -->
<!--<section class="service-area sky-blue ">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0">Why are thousands of mechanics joining us?</h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">CHOOSE YOUR SCHEDULE</a></h4>
<p>One day a week? 5 days a week? Just evenings? Weekends only? You have total control. You can simply use our Android app to set your own hours and we will book you appointments based on your availability.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">FAIR PAY</a></h4>
<p>Our mechanics make anywhere between 150-400 an hour based on their location, skills and level of experience. We pay flat rate and we don’t do any warranty work. You get paid fairly.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-plane"></span>
</div>
<div class="service-cap">
<h4><a href="#">NO COMPLEX JOBS</a></h4>
<p>We focus on basic repair and maintenance work -- brakes, tire rotations, timing belts, 30/60/90k service, alternators, A/C, etc. No engine overhauls or major transmission work.</p>
</div>
</div>
</div>
</div>-->

 <!-- second -->
<!--<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">FOCUS ON WHAT YOU LOVE</a></h4>
<p>We handle marketing, booking appointments, delivering parts, buying insurance and providing support. You do what you love - fix Vehicle s. We take care of the rest.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">WIN-WIN FOR EVERYONE</a></h4>
<p>When you work directly with Vehicle owners in your neighbourhood, you can make more money with no commute. Plus, Vehicle owners enjoy the convenience. Everyone wins.</p>
</div>
</div>
</div>

</div>
</div>
</section>-->
<!-- lide to short end-->

<!-- Life to shoet start.. -->
<!--<section class="service-area sky-blue section-padding2">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0">Joining Vahanom is as easy as 1-2-3</h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">EASY ONLINE APPLICATION</a></h4>
<p>It takes less than 10 minutes to fill out the entire application. If you have questions, call us at</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">PHONE INTERVIEW & ONBOARDING</a></h4>
<p>A service advisor will call you to interview you and provide more information about how Vahanom works.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-plane"></span>
</div>
<div class="service-cap">
<h4><a href="#">START WORKING</a></h4>
<p>Make rupees 100-400 an hour based on your skills and experience. Set your own hours -- choose evenings, weekends, or full-time.</p>
</div>
</div>
</div>
</div>
</div>
</section>-->
<!-- lide to short end-->
<main> 

<section id="features" class="best-features-area section-padd4 mt-50" style="padding-bottom: 5%;padding-top: 5%;">
	<div class="container-fluid" style="padding-left:4% ;padding-right:4% ;">
		<div class="row justify-content-end">
			<div class="col-xl-8 col-lg-8 mb-40">
				<div class="col-lg-12 col-md-12">
					<div class="section-tittle">
						<h3 style="margin-bottom: 1% !important;">Technician Registration</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-xl-2 col-lg-2 col-md-2"></div>
						<div class="col-xl-7 col-lg-7 col-md-7 mt-20">
							<iframe style="width: 100%;height:310px;box-shadow: 0px 0px 22px -8px;border-radius: 25px;" src="https://www.youtube.com/embed/PHZ0J5vD8Lg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
						<div class="col-xl-1 col-lg-1 col-md-1"></div>
						<div class="col-xl-5 col-lg-5 col-md-5 mt-2" style="padding-top: 4%;padding-left: 5%;padding-right: 10%;box-shadow: 0px 0px 22px -8px;border-radius: 25px;">
							<div class="single-features ">
								<div class="features-icon">
									<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Customer_support.png"></span>
								</div>
								<div class="features-caption">
									<h3>Flexible Hours</h3>
									<p>Work Evenings, Weekends, or Full-Time --You Decide!</p>
								</div>
							</div>
						</div>
						<div class="col-xl-1 col-lg-1 col-md-1"></div>
						<div class="col-xl-5 col-lg-5 col-md-5 mt-20" style="padding-top: 4%;padding-left: 5%;padding-right: 10%;box-shadow: 0px 0px 22px -8px;border-radius: 25px;">
							<div class="single-features ">
	 							<div class="features-icon">
									<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Get_Payments_Easily.png"></span>
								</div>
								<div class="features-caption">
									<h3>Great Pay</h3>
									<p>Make <b>Rupee 150-400/-</b> Hourly -- 2-3x What Shops & Dealers Pay You</p>
								</div>
							</div>
						</div>
						<div class="col-xl-1 col-lg-1 col-md-1"></div>
						
						<div class="col-xl-3 col-lg-3 col-md-3"></div>
						<div class="col-xl-6 col-lg-6 col-md-6 mt-20" style="padding-top: 4%;padding-left: 5%;padding-right: 10%;box-shadow: 0px 0px 22px -8px;border-radius: 25px;">
							<div class="single-features">
								<div class="features-icon">
									<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
								</div>
								<div class="features-caption">
									<h3>Be Your Own Boss</h3>
									<p>You Choose the Type of Jobs, Where to Work & when to work.No Politics or Managers.</p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
			<div class="col-xl-4 col-lg-4 ">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="section-tittle">
							<h3 style="margin-bottom: 4% !important;">Register Now</h3>
						</div>
					</div>
				</div>
				<div class="row" style="box-shadow: 0px 0px 22px -8px;padding: 3%;border-radius: 25px;margin-left: 1%;margin-right: 1%;">
					<form method="post" action="<?php echo  base_url('WebTechnician/add'); ?>" enctype="multipart/form-data" >
						
					<div class="col-lg-12" style="padding: 2%;">
						<label>Name:</label>
						<input type="text" required name="name" class="single-input-primary" id="name" placeholder="Enter Name" />
						
					</div>
					<div class="col-lg-12" style="padding: 2%;">
						<label>Email:</label>
						<input type="email" name="email" class="single-input-primary" id="email" placeholder="Enter Email" />
					</div>
					<div class="col-lg-12" style="padding: 2%;">
						 <label>Phone Number:</label>
						 <input type="number" required class="single-input-secondary" onchange="duplicatecheck()" required name="phone_number" id="phone_number"  placeholder="Enter Phone Number" />
					</div>
					<div class="col-lg-12" style="padding: 2%;">
						<label>Pincode:</label>
						<input type="number" required name="pincode" onchange="pincodevalidation();" class="single-input-secondary" id="pincode" placeholder="Enter Pincode" />
					</div>
					<div class="col-lg-12 " style="padding: 2%;">
						<label>Where did you hear about Vahanom?</label>
							<select id="reference" name="reference" class="form-control" required>
								<option value="">Choose One</option>
								<option value="Friends">Friends</option>
								<option value="News">News</option>
								<option value="Social Media">Social Media</option>
								<option value="Other">Other</option>
								
							</select>
					</div>
					
					<div class="col-lg-12" style="padding: 2%;">
						<label>Address:</label>
						<textarea cols="40" id="address" class="single-input-secondary" name="address" rows="2" placeholder="Enter Address"></textarea>
					</div>
					<div class="col-lg-12 text-center">
						<div class="button-group-area mt-10">
							<!--<a href="#" class="genric-btn primary-border" id="submit">Submit</a>-->
							<button type="submit" class="btn btn-primary mr-2">Apply Now</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>
</section>
  <!-- Section -->
  <section class="service-area sky-blue " style="padding-top: 5%">
    
    

    <!-- Modal -->
    <div class="container">
	    <h3 class="font-weight-bold text-center dark-grey-text pb-2">Three Easy Steps</h3>
	    
	    <div class="row align-items-center">

	      <div class="col-lg-6 mb-4">
	        <div class="view z-depth-1-half rounded">
	              <img class="rounded img-fluid" src="<?php echo base_url(); ?>assets/img/hero/hero_right.png" class="img">
	          </div>
	      </div>

	      <div class="col-lg-6 mb-4">

	        <ol class="step pl-0">
	          <li class="step-element pb-0">
	            <div class="step-number">
	              <span class="number">1</span>
	            </div>
	            <div class="step-excerpt">
	              <h4 class="font-weight-bold dark-grey-text mb-3">EASY ONLINE APPLICATION</h4>
	              <p class="text-muted">It takes less than 10 minutes to fill out the entire application. If you have questions, call us at</p>
	            </div>
	          </li>
	          <li class="step-element pb-0">
	            <div class="step-number">
	              <span class="number">2</span>
	            </div>
	            <div class="step-excerpt">
	              <h4 class="font-weight-bold dark-grey-text mb-3">PHONE INTERVIEW & ONBOARDING</h4>
	              <p class="text-muted">A service advisor will call you to interview you and provide more information about how Vahanom works.</p>
	            </div>
	          </li>
	          <li class="step-element pb-0">
	            <div class="step-number">
	              <span class="number">3</span>
	            </div>
	            <div class="step-excerpt">
	              <h4 class="font-weight-bold dark-grey-text mb-3">START WORKING</h4>
	              <p class="text-muted">Make rupees 100-400 an hour based on your skills and experience. Set your own hours -- choose evenings, weekends, or full-time.</p>
	            </div>
	          </li>
	        </ol>

	      </div>

	    </div>
    </div>
  </section>
  <!-- Section -->
<div class="container my-5 p-5 z-depth-1" 	>


  <!--Section: Content-->
  <section class="dark-grey-text">

    <!-- Section heading -->
    <h2 class="text-center font-weight-bold mb-4 pb-2">Why are thousands of mechanics joining us?</h2>

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-lg-5 text-center text-lg-left">
        <img class="img-fluid" src="<?php echo base_url(); ?>assets/img/gallery/app3.png" alt="Sample image" style="box-shadow: 0px 0px 22px -8px;border-radius: 70px;">
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-lg-7">

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-1">
            <i class="fas fa-share fa-lg indigo-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3">CHOOSE YOUR SCHEDULE</h5>
            <p class="grey-text">One day a week? 5 days a week? Just evenings? Weekends only? You have total control. You can simply use our Android app to set your own hours and we will book you appointments based on your availability.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-1">
            <i class="fas fa-share fa-lg indigo-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3">FAIR PAY</h5>
            <p class="grey-text">Our mechanics make anywhere between 150-400 an hour based on their location, skills and level of experience. We pay flat rate and we don’t do any warranty work. You get paid fairly.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!--Grid row-->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-1">
            <i class="fas fa-share fa-lg indigo-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3">NO COMPLEX JOBS</h5>
            <p class="grey-text mb-0">We focus on basic repair and maintenance work -- brakes, tire rotations, timing belts, 30/60/90k service, alternators, A/C, etc. No engine overhauls or major transmission work.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!--Grid row-->
        <!--Grid row-->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-1">
            <i class="fas fa-share fa-lg indigo-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3">WIN-WIN FOR EVERYONE</h5>
            <p class="grey-text mb-0">When you work directly with Vehicle owners in your neighbourhood, you can make more money with no commute. Plus, Vehicle owners enjoy the convenience. Everyone wins.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!--Grid row-->
        <!--Grid row-->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-1">
            <i class="fas fa-share fa-lg indigo-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-xl-10 col-md-11 col-10">
            <h5 class="font-weight-bold mb-3">FOCUS ON WHAT YOU LOVE</h5>
            <p class="grey-text mb-0">We handle marketing, booking appointments, delivering parts, buying insurance and providing support. You do what you love - fix Vehicle s. We take care of the rest.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!--Grid row-->

      </div>
      <!--Grid column-->

    </div>
    <!-- Grid row -->

  </section>
  <!--Section: Content-->


</div>
<!-- Life to shoet start.. -->
<!-- <section class="service-area sky-blue section-padding2" style="padding-bottom:5px;padding-top: 150px;">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2>Calling all mechanics/technicians to join the revolution<br></h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">Easy Registration process</a></h4>
<p>With Vahanom, become an approved technician/mechanic with an easy registration process. All you need is to pass our evaluation test to become a certified mechanic. For our registration process, we ask you for your government-approved documents and your contact details. We then create your professional Vahanom Mechanic Account.  </p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">Good payout</a></h4>
<p>Get paid what you worth. We hire professionals and pay them decent commissions on all their services to a client. We strive to become the best vehicle repair service in India. That's possible only when our mechanics are happy with the payout. Vahanom makes sure every mechanic/technician gets good pay, even when the client has a minimum service requirement.  </p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-plane"></span>
</div>
<div class="service-cap">
<h4><a href="#">Be your own boss</a></h4>
<p>Work as much or as little as you want with Vahanom. Mechanics get paid based on their services and not on their time. It allows our mechanics/technicians to have as many days off as they want or work as much they want. Accept or reject any requests for servicing/repairing from your Vahanom account. Have complete control over your earnings with Vahanom.</p>
</div>
</div>
</div>
</div>
</div>
</section>
 -->

 <!-- lide to short end-->


<div class="container-fluid" style="padding: 50px;background-color: #f9fafc">
   <div class="section-tittle text-center">
        <h2>What do our mechanics say?</h2>
   </div>
    <div class="row">
    	<div class="col-lg-12 mt-10 text-center">
           	<h2>Technician and his reviews</h2>
        </div>

</div>
</div>





</main>

<?php include('/home/vahanomvpa/public_html/application/views/layout/footer.php');?>


<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');

  	$('#submit').click(function(){
	 	alert("Thank you for your registration, our team will get back to you shortly.")
	});
</script>
 <script>
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
						function alphanumeric(inputtxt)
{ 
	// alert(inputtxt);
var letters = /^[0-9a-zA-Z]+$/;

if(inputtxt.match(letters))
{
// alert('Your registration number have accepted : you can try another');
// document.form1.text1.focus();
return true;
}
else
{
  alert('Please input alphanumeric characters only');
  document.getElementById("ifscnumber").value='';
return false;
}
}

function duplicatecheck(){
	var value=document.getElementById("phone_number").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("phone_number").value='';
	   return false;
     }


}

function pincodevalidation(){
	var value=document.getElementById("pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("pincode").value='';
	   return false;
     }
}

$(document).ready(function(){
 $("#Feature").hide();
 $("#Contact").hide();
 $(".contact_class").hide();
 $(".feature_class").hide();
});
						</script>
</body>

</html>