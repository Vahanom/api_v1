<!doctype html>
<html class="no-js" lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Career</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.html">


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<style type="text/css">
    .nice-select{
  width: 100% !important;
  }
  
  </style>
</head>
<body>
<main> 
<section id="features" class="best-features-area section-padd4 mt-80" style="background-image: -webkit-linear-gradient(0deg, #ED1C24 0%, #FFB2B5 100%);padding-bottom: 5%;padding-top: 7%;">
	<div class="container-fluid" style="padding-left:10% ;padding-right:4% ;">
		<div class="row justify-content-end">	
			<div class="col-xl-8 col-lg-8 ">
				<div class="container">
					<div class="row">
						<section class="px-md-5 mx-md-5 text-white text-center text-lg-left dark-grey-text">

					      <h1 class="font-weight-bold text-white">Join Us Now</h1>

					      <p class="mb-5 text-white" style="font-size: 20px;">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Id quam sapiente molestiae
					        numquam quas, voluptates omnis nulla ea odio quia similique corrupti magnam, doloremque laborum reiciendis
					        doloribus eius optio autem. Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>

					          <!--Grid row-->
						      <div class="row">

						        <!--Grid column-->
						        <div class="col-lg-4 col-md-4 mb-lg-0 mb-4">

						          <img src="<?php echo base_url(); ?>assets/img/shape/available-app1.png" class="img-fluid" alt="">

						        </div>
						        <!--Grid column-->
						        <div class="col-lg-6 col-md-6 mb-lg-0 mb-lg-0 mb-2">
						       						          <h2 class="text-white">Our app is available at your service</h2>

						          <p class="text-white">Download from any device and take good care of your vehicle now. </p>
						
						        </div>
						        <!--Grid column-->

						      </div>
						      <!--Grid row-->
				      		<div class="app-btn mt-40">
								<a href="#" class="app-btn1"><img src="<?php echo base_url(); ?>assets/img/shape/app_btn1.png"  alt="" class="img mt-10"></a>
								<a href="#" class="app-btn2"><img src="<?php echo base_url(); ?>assets/img/shape/app_btn2.png" alt="" class="img mt-10"></a>
							</div>

					    </section>
					    <!--Section: Content-->
	
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4 mb-40 mt-20">
				<div class="row" >
					<form method="post" action="<?php echo  base_url('Career/add'); ?>" enctype="multipart/form-data" style="box-shadow: 0px 0px 22px -8px;padding: 3%;border-radius: 25px;margin-left: 1%;margin-right: 1%;background-color: white;">
						
					<div class="col-lg-12" style="padding: 2%;">
						<label>Name:</label>
						<input type="text" required name="name" class="single-input-primary" id="name" placeholder="Enter Name" onkeypress="return /[a-z]/i.test(event.key)"/>
						
					</div>
					<div class="col-lg-12" style="padding: 2%;">
						<label>Email:</label>
						<input type="email" name="email" class="single-input-primary" id="email" placeholder="Enter Email" />
					</div>
					<div class="col-lg-12" style="padding: 2%;">
						 <label>Phone Number:</label>
						 <input type="number" required class="single-input-secondary" onchange="duplicatecheck()" required name="phone_number" id="phone_number"  placeholder="Enter Phone Number" />
					</div>
					<div class="col-lg-12 " style="padding: 2%;">
						<label>Job Openings:</label>
							<select id="openings" name="openings" class="form-control" required>
								<option value="">---Select---</option>
								<?php foreach($openings as $row){?>
						    		<option vlaue="<?php echo $row->openings?>"><?php echo $row->openings?></option>
								<?php } ?>
							</select>
					</div>
					<br>
					<div class="col-lg-12" style="padding: 2%;margin-top: 5%">
						<label>Select CV:</label>
						<input type="file"   accept=".pdf,.doc,.docx" required name="resume" class="" id="resume" />
               	 	</div>
					<div class="col-lg-12" style="padding: 2%;">
						<label>Message:</label>
						<textarea cols="40" id="message" class="single-input-secondary" name="message" rows="2" placeholder="Enter Message" required ></textarea>
					</div>
					<div class="col-lg-12 text-center">
						<div class="button-group-area mt-10 mb-10">
							<!--<a href="#" class="genric-btn primary-border" id="submit">Submit</a>-->
							<button type="submit" class="btn btn-primary mr-2">Apply Now</button>
						</div>
					</div>
				</form>
				</div>	
			</div>
		</div>
	</div>
</section>

</main>


<?php include('/home/vahanomvpa/public_html/application/views/layout/footer.php');?>


<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');

  	$('#submit').click(function(){
	 	alert("Thank you for your registration, our team will get back to you shortly.")
	});
</script>
 <script>
					$(".alphaonly").keydown(function(event){
        var inputValue = event.which;
        // console.log(inputValue);
        if (!(inputValue >= 65 && inputValue <= 123)
                            && (inputValue != 32 && inputValue != 0)
                            && ( inputValue != 8)
                            && (inputValue != 9)){
                                event.preventDefault(); 
                        }
    });
					 function validatepannumber(){
                        var pannumber = document.getElementById("technician_pan").value;
                        if(pannumber==""){
                            return true;
                        }else{
                            var regpan = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/;
                            if(regpan.test(pannumber) == false)
                                {
									alert("Not a valid Pan Number");
	   document.getElementById("technician_pan").value='';
	   return false;
                                }else{
                                return true;
                                }
                            }
                        }
						function alphanumeric(inputtxt)
{ 
	// alert(inputtxt);
var letters = /^[0-9a-zA-Z]+$/;

if(inputtxt.match(letters))
{
// alert('Your registration number have accepted : you can try another');
// document.form1.text1.focus();
return true;
}
else
{
  alert('Please input alphanumeric characters only');
  document.getElementById("ifscnumber").value='';
return false;
}
}

function duplicatecheck(){
	var value=document.getElementById("phone_number").value;
	
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Phone Number");
	   document.getElementById("phone_number").value='';
	   return false;
     }


}

function pincodevalidation(){
	var value=document.getElementById("pincode").value;
	
	var phoneno = /^\(?([1-9]{1})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{2})$/;
  if(value.match(phoneno))
     {
	  
	 }
   else
     {
	   alert("Not a valid Pincode Number");
	   document.getElementById("pincode").value='';
	   return false;
     }
}

$(document).ready(function(){
 $("#Feature").hide();
 $("#Contact").hide();
 $(".contact_class").hide();
 $(".feature_class").hide();
});
						</script>
</body>

</html>