


<!-- Footer -->
<footer class="page-footer font-small teal pt-5" id="contact">

  <!-- Footer Text -->
  <div class="container text-center text-md-left">
 
    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3 row">

        <!-- Content -->
        <a href="/"><img src="<?php echo base_url(); ?>assets/img/logo/logo2.png" alt=""></a>
	    <p class="pt-4">On average our mechanics have over ten years of experience and most are master technicians with OEM Certifications.</p>
	    <hr class="my-3">
        <div class="col-md-6">
        <i class="fa fa-envelope text-danger">test@test.com</i>	
        </div>
	    <div class="col-md-6">
        	
        <i class="fa fa-mobile text-danger"></i>+91-1234567890
        </div>
        <div class="col-md-12"><i class="fa fa-map-marker text-danger"></i> test Address</div>
        
        <p class="font-weight-bold">Follow us on:</p>

        <!--Facebook-->
        <a href="#" class="mx-1 " role="button"><i class="fab fa-facebook-f text-danger"></i></a>
        <a href="#" class="mx-1" role="button"><i class="fab fa-linkedin-in text-danger"></i></a>
        <a href="#" class="mx-1" role="button"><i class="fab fa-instagram text-danger"></i></a>
          
      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-6 mb-md-0 mb-3">

        <!-- Content -->
        <h5 class="text-uppercase font-weight-bold">Contact Us</h5>
        <div class="footer-tittle">
	</div>

	<form>
		<div class="row">
			<div class="col-lg-6 mb-1">
				<input type="text" name="first_name" placeholder="Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name'" required class="form-control single-input-primary">
			</div>
			<div class="col-lg-6 mb-1">
				<input type="number" name="NUMBER" placeholder="Phone Number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Secondary color'" required class="form-control single-input-secondary">
			</div>
			<div class="col-lg-12 mb-1">
				<input type="email" name="EMAIL" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'" required class="form-control single-input">
			</div>
			<div class="col-lg-12 mb-1">
				<textarea class="form-control single-textarea" placeholder="Message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'" required></textarea>
			</div>
			

			<div class="col-lg-12 mb-1">
				<div class="button-group-area mt-10">
							<button type="submit" class="btn btn-primary mr-2">Submit</button>
				</div>
			</div>

		</div>
	</form>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Text -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">Designed & Developed by 
    <a href="https://qset.in/" class="text-danger"> Qset</a> | <a href="/Privacy/" class="text-danger">Privacy Policy</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->