<body>
  <style>
    @media(max-width: 991px) {
         .nav-item:hover{
           height:90px;
           overflow-y: scroll;
         }
}

  </style>
  
  <style type="text/css">
    .nice-select{
  width: 100% !important;
  font-size: 1rem !important;
  height:auto !important;
  padding-left:3% !important;
  }
  </style>
<?php    
    // Append the requested resource location to the URL   
    $url= $_SERVER['REQUEST_URI'];    
?>

<div id="preloader-active">
<div class="preloader d-flex align-items-center justify-content-center">
<div class="preloader-inner position-relative">
<div class="preloader-circle"></div>
<div class="preloader-img pere-text">
<img src="<?php echo base_url(); ?>assets/img/logo/loader50.png" alt="">
</div>
</div>
</div>
</div>

<header>

<div class="header-area header-transparrent ">
<div class="main-header header-sticky">
<div class="container">
<div class="row align-items-center">

<div class="col-xl-2 col-lg-2 col-md-2">
<div class="logo">
<a href="index-2.html"><img src="<?php echo base_url(); ?>assets/img/logo/logo2.png" alt=""></a>
</div>
</div>
<div class="col-xl-10 col-lg-10 col-md-10">

<div class="main-menu f-right d-none d-lg-block">
<nav>
<ul id="navigation">
<li class="<?php if($url=='/') echo 'active';?>"><a href="<?php echo base_url();?>"> Home</a></li>
<!--<li><a href="#features" id="Feature" class="feature_class">Feature</a></li>-->
<li class="<?php if($url=='/Feature/') echo 'active';?>"><a href="<?php echo base_url();?>Feature/">Feature</a></li>
<li class="<?php if($url=='/AboutUs/') echo 'active';?>"><a href="<?php echo base_url();?>AboutUs/" >About Us</a></li>
<li class="<?php if($url=='/WebTechnician/') echo 'active';?>">
        <a class="nav-link " href="<?php echo base_url();?>WebTechnician/"  role="button" >
          Registration
        </a>
         </li>
<li class="<?php if($url=='/career/') echo 'active';?>"><a href="<?php echo base_url();?>career/">Careers</a></li>
<!-- <li><a href="services.html">Services</a></li> -->
<!-- <li><a href="pricing.html">Pricing</a></li> -->
<!-- <li><a href="#">Pages</a>
<ul class="submenu">
<li><a href="blog.html">Blog</a></li>
<li><a href="single-blog.html">Blog Details</a></li>
<li><a href="elements.html">Element</a></li>
</ul>
</li> -->
<li><a href="#contact" id="contact1" class="Contact_class">Contact Us</a></li>
</ul>
</nav>
</div>
</div>

<div class="col-12">
<div class="mobile_menu d-block d-lg-none"></div>
</div>
</div>
</div>
</div>
</div>

</header>
