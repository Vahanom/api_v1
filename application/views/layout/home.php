<main>
  <style type="text/css">
    .nice-select{
  width: 100% !important;
  }
  </style>
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Get A Quote</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
         <!-- Default form register -->
          <form class="text-center" action="<?php echo  base_url('Getaquote/add'); ?>" enctype="multipart/form-data" method="post">
            <p>Tell us what your Vehicle needs or ask for a Inspection. Receive a free, fast, fair & transparent price quote.</p>
                <input type="text" required name="full_name" id="defaultRegisterFormFirstName" class="form-control mb-4" placeholder="Name" onkeypress="return /[a-z]/i.test(event.key)">
            
            <!-- E-mail -->
            <input type="email" id="defaultRegisterFormEmail" name="email" required class="form-control mb-4" placeholder="E-mail">

            <!-- Phone number -->
            <input type="number" id="defaultRegisterPhonePassword" name="contact" required class="form-control mb-4" placeholder="Phone Number"
              aria-describedby="defaultRegisterFormPhoneHelpBlock">

            <select id="type_vehicle" required class="form-control mb-4" name="type_vehicle" >
              <option value="">Vehicle Type</option>
              <option value="Four-wheeler">Four-wheeler</option>
              <option value="Two-wheeler">Two-wheeler</option>
              <option value="Bus">Bus</option>
              <option value="Truck">Truck</option>
            </select>
            
            <input type="text" id="defaultRegistervehname" name="veh_name" class="form-control mb-4" placeholder="Vehicle Make Ex. Maruti Suzuki"
              aria-describedby="defaultRegisterFormvehnameBlock" required>
                                   
            <input type="text" id="defaultRegistervehmodel" name="veh_model" class="form-control mb-4" placeholder="Vehicle Model Ex. Swift"
              aria-describedby="defaultRegisterFormvehmodelBlock" required>
           <!-- E-mail -->
            <textarea id="defaultRegisterFormMessage" class="form-control mb-4" name="message" required placeholder="Description"></textarea> 
           
            <!-- Sign up button -->
            <button class="btn btn-info my-4 btn-block" type="submit">Submit Now</button>

           
          </form>
          <!-- Default form register -->

      </div>
    </div>
  </div>
</div>
<div class="slider-area ">
 <div class="slider-active">
<div class="single-slider slider-height slider-padding sky-blue d-flex align-items-center">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="col-lg-6 col-md-9 ">
        <div class="hero__caption">
          <span data-animation="fadeInUp" data-delay=".4s">Skip visiting mechanics</span>
          <h1 data-animation="fadeInUp" data-delay=".6s">Get your vehicle repaired</h1>
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <h2 style="font-weight: bold;">at your home</h2>
              </div>
              <div class="carousel-item">
                <h2 style="font-weight: bold;">at your office</h2>
              </div>
              <div class="carousel-item">
                <h2 style="font-weight: bold;">on-the-spot</h2>
              </div>
            </div>
          </div>
          <p data-animation="fadeInUp" data-delay=".8s">Service at your home or office 路 7 days a week 路 Fair and transparent pricing
          </p>
          <p data-animation="fadeInUp" data-delay=".8s">Get a mechanic for your two-wheeler or your car right to your door.
          </p>
          <div class="slider-btns">
            <a data-animation="fadeInLeft" data-delay="1.0s"  href="" data-toggle="modal" data-target="#modalContactForm" class="btn radius-btn">Get A Quote</a>
            <!--<a data-animation="fadeInRight" data-delay="1.0s" class="popup-video video-btn ani-btn" href="https://www.youtube.com/watch?v=1aP-TXUpNoU"><i class="fas fa-play"></i></a>-->
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="hero__img d-none d-lg-block f-right" data-animation="fadeInRight" data-delay="1s">
          <img src="<?php echo base_url(); ?>assets/img/hero/hero_right.png" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="single-slider slider-height slider-padding sky-blue d-flex align-items-center">
<div class="container">
<div class="row d-flex align-items-center">
<div class="col-lg-6 col-md-9 ">
<div class="hero__caption">
<span data-animation="fadeInUp" data-delay=".4s">App Landing Page</span>
<h1 data-animation="fadeInUp" data-delay=".6s">Get things done<br>with VAHANOM</h1>
<p data-animation="fadeInUp" data-delay=".8s">Dorem ipsum dolor sitamet, consectetur adipiscing elit, sed do eiusm tempor incididunt ulabore et dolore magna aliqua.</p>
 
<div class="slider-btns">

<a data-animation="fadeInLeft" data-delay="1.0s" href="industries.html" class="btn radius-btn">Download</a>

<a data-animation="fadeInRight" data-delay="1.0s" class="popup-video video-btn ani-btn" href="https://www.youtube.com/watch?v=1aP-TXUpNoU"><i class="fas fa-play"></i></a>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="hero__img d-none d-lg-block f-right" data-animation="fadeInRight" data-delay="1s">
<img src="<?php echo base_url(); ?>assets/img/hero/banner1.png" alt="">
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<section id="about" class="best-features-area section-padd4" style="padding-bottom: 5px;padding-top: 100px;">
<div class="container">
<div class="row justify-content-end">
<div class="col-xl-8 col-lg-10">

<div class="row">
<div class="col-lg-10 col-md-10">
<div class="section-tittle">
<h2>Here's what we got for you with our app</h2>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-6 col-lg-6 col-md-6">
<div class="single-features mb-70">
<div class="features-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/mind.svg"></img></span>
</div>
<div class="features-caption">
<h3>Peace of Mind</h3>
<p>Book a service online all day, any day. Our mechanics will come to service your automobile at your home or office, or anywhere- be it night or day, national holiday or weekends. 
</p>
</div>
</div>
</div>
<div class="col-xl-6 col-lg-6 col-md-6">
<div class="single-features mb-70">
 <div class="features-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/pricing.svg"></span>
</div>
<div class="features-caption">
<h3>Fair And Transparent Pricing</h3>
<p>We charge you what you see on screen- that's a promise!
Our pricing provides estimates upfront so that you don't have to worry about paying anything extra
.</p>
</div>
</div>
</div>
<div class="col-xl-6 col-lg-6 col-md-6">
<div class="single-features mb-70">
<div class="features-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/auto-doc.svg"></span>
</div>
<div class="features-caption">
<h3>Verified and experienced Auto-doctors</h3>
<p>We hire experienced and professional mechanics to maintain your wheels. All our mechanics are verified and take care of your vehicle, just like you'd do- with the right team and tools.</p>
</div>
</div>
</div>
<!--<div class="col-xl-6 col-lg-6 col-md-6">
<div class="single-features mb-70">
<div class="features-icon">
<span class="flaticon-support"></span>
</div>
<div class="features-caption">
<h3>Creative Design</h3>
<p>Aorem psum olorsit amet ectetur adipiscing elit, sed dov.</p>
</div>
</div>
</div>-->
</div>
</div>
</div>
</div>

<div class="features-shpae d-none d-lg-block">
<img src="<?php echo base_url(); ?>assets/img/shape/best-features1.png" alt="">
</div>
</section>


<section class="service-area sky-blue section-padding2" style="padding-bottom:20px;padding-top: 50px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom: 2px;padding-top: 10px;">Life is too short to<br>spend at an auto service</h2>
<p>Enjoy repairing your vehicle at your doorstep. Get your wheels fixed and ready to go with these three easy steps- It's as easy as ready-set-go!</p>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30" style="height: auto !important;">
<a data-toggle="collapse" href="#get_a_quote" aria-controls="get_a_quote"><div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/left-quote.png"></span>
</div>
<div class="service-cap">
<h4>Get a quote</a></h4>
<p id="get_a_quote" class="collapse " role="tabpanel" aria-labelledby="heading4">Get instant pricing on all of your vehicle services. Submit what needs to be fixed in as little as 60 seconds and receive a quote on your screen. For repairs where pricing is not available instantly, book an inspection, and our mechanics will be there in no time.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30" style="height: auto !important;">
<a data-toggle="collapse" href="#schedule_an_apt" aria-controls="schedule_an_apt"><div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/calendar.png"></span>
</div>
<div class="service-cap">
<h4>Schedule an appointment</a></h4>
<p id="schedule_an_apt" class="collapse " role="tabpanel" aria-labelledby="heading4">Select a day and time that suits you, and we'll be there. We work 24/7, 365 days a week, so you don't have to take time off your busy schedule. Our experienced repairmen put the customer's time on top priority- That's what makes us the best mechanics.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30" style="height: auto !important;">
<a data-toggle="collapse" href="#veh_fixed" aria-controls="veh_fixed"><div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/fix-sign.png"></span>
</div>
<div class="service-cap">
<h4>Get your vehicle fixed</a></h4>
<p id="veh_fixed" class="collapse " role="tabpanel" aria-labelledby="heading4">Sit back and relax. No waiting in line or shuttling back and forth to the garage. Vahanom has a team of enthusiastic mechanics and the right set of tools to get the job done at your doorstep. Get your wheels fixed at your place and time.</p>
</div>
</div>
</div>
</div>
</div>
</section>


<div class="applic-apps section-padding2" style="padding-bottom:50px;padding-top:70px;">
<div class="container-fluid">
<div class="row">

<div class="col-xl-4 col-lg-4 col-md-8">
<div class="single-cases-info mb-30">
<h3>Get everything repaired at your home or office</h3>
<p>No more paying tow-trucks for smaller parts like spark plugs and fuel pumps. Here are all of the services you get at your doorstep.
</p>
</div>
</div>

<div class="col-xl-8 col-lg-8 col-md-col-md-7">
<div class="app-active owl-carousel">
<div class="single-cases-img">
<img src="<?php echo base_url(); ?>assets/img/gallery/app1.png" alt="">
</div>
<div class="single-cases-img">
<img src="<?php echo base_url(); ?>assets/img/gallery/app2.png" alt="">
</div>
<div class="single-cases-img">
<img src="<?php echo base_url(); ?>assets/img/gallery/app3.png" alt="">
</div>
<div class="single-cases-img">
<img src="<?php echo base_url(); ?>assets/img/gallery/app2.png" alt="">
</div>
<div class="single-cases-img">
<img src="<?php echo base_url(); ?>assets/img/gallery/app1.png" alt="">
</div>
</div>
</div>
</div>
</div>
</div>



<!-- 
<section class="best-pricing pricing-padding" data-background="assets/img/gallery/best_pricingbg.jpg">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-6 col-md-8">
<div class="section-tittle section-tittle2 text-center">
<h2>Choose Your Very Best Pricing Plan.</h2>
</div>
</div>
</div>
</div>
</section> -->


<!-- <div class="pricing-card-area">
<div class="container">
<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="single-card text-center mb-30">
<div class="card-top">
<span>2 Years</span>
<h4>$05 <span>/ month</span></h4>
</div>
<div class="card-bottom">
<ul>
<li>Increase traffic 50%</li>
<li>E-mail support</li>
<li>10 Free Optimization</li>
<li>24/7 support</li>
</ul>
 <a href="services.html" class="btn card-btn1">Get Started</a>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="single-card  text-center mb-30">
<div class="card-top">
<span>2 Years</span>
<h4>$05 <span>/ month</span></h4>
</div>
<div class="card-bottom">
<ul>
<li>Increase traffic 50%</li>
<li>E-mail support</li>
<li>10 Free Optimization</li>
<li>24/7 support</li>
</ul>
<a href="services.html" class="btn card-btn1">Get Started</a>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="single-card text-center mb-30">
<div class="card-top">
<span>2 Years</span>
<h4>$05 <span>/ month</span></h4>
</div>
<div class="card-bottom">
<ul>
<li>Increase traffic 50%</li>
<li>E-mail support</li>
<li>10 Free Optimization</li>
<li>24/7 support</li>
</ul>
<a href="services.html" class="btn card-btn1">Get Started</a>
</div>
</div>
</div>
</div>
</div>
</div> -->
<!-- how it works.. -->
<!--<section class="service-area sky-blue section-padding2" style="padding-bottom:50px;padding-top:70px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0;">Request vehicle service <br>online</h2>
<p style="font-weight: bold;">Vehicle trouble? We got you covered.</p>
<p>
Book a service online and get your vehicle fixed at your convenient place in no time. We make the process of repairing stress-free.
Maintain your vehicles without missing out on our maintenance schedule on our app and website. Our app makes it easier for you to get maintenance reminders and check all maintenance history.
Get transparent pricing for your vehicle repair. With Vahanom, you get the best pricing, as we don't have the overhead cost of a static garage. 
We charge you less while we offer repair and maintenance at your doorstep.
</p>
<p>Inspection scheduling was made easy.

</p>
<p>Here's a simple three-step process to get your wheels running:
</p>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="60" height="60" src="<?php echo base_url(); ?>assets/img/Website_icons/Quick_Messaging.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">We ask you what's wrong</a></h4>
<p>The first thing we do is to ask you what's going on with your vehicle. Our expert technicians expect the answers which a non-technical customer can tell, like "My car is not starting, or my bike is shaking." Don't worry; we make it up to you with our questionnaires. We ask you the make, year, model, and company name of your wheels. Once we get it, we ask you for the vehicle location and your contact details.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="60" height="60" src="<?php echo base_url(); ?>assets/img/Website_icons/Quick_Messaging.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Get a fair estimate</a></h4>
<p>With the information you give and our expert's opinion, we provide a list of parts, services, and cost estimation to keep your wheels rolling. In most cases, we get you instant, fair, and transparent pricing for your vehicle. Our mechanics then accept our pricing and honor the quote. No need to negotiate the pricing. Have peace of mind with fixed prices with no last-minute surprises.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span ><img class="manImg" width="60" height="60" src="<?php echo base_url(); ?>assets/img/Website_icons/Quick_Messaging.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Schedule a quick inspection</a></h4>
<p>With scheduling a quick inspection, you get a verified and reviewed mechanic coming to help you at your doorstep. You can also check the work history of your mechanic. Our expert mechanics are what we take pride in, and they believe in providing top-notch service to our customers every time you book a service with us. Vahanom's vehicle doctors are available for you every day, from 7 am to 9 pm.</p>
</div>
</div>
</div>
</div>
</div>
</section>-->

<div class="container-fluid" style="padding:5%;background-color: #f9fafc">
  <div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-6">
      <h1>Request vehicle service online</h1>
      <p>Vehicle trouble? We got you covered.</p>
      <p>Book a service online and get your vehicle fixed at your convenient place in no time. We make the process of repairing stress-free.Maintain your vehicles without missing out on our maintenance schedule on our app and website. Our app makes it easier for you to get maintenance reminders and check all maintenance history. 
      Get transparent pricing for your vehicle repair. With Vahanom, you get the best pricing, as we don't have the overhead cost of a static garage. We charge you less while we offer repair and maintenance at your doorstep.Inspection scheduling was made easy.
      </p>
    </div>
    <div class="col-lg-4">
      <!--Accordion wrapper-->
      <div class="accordion md-accordion accordion-3 z-depth-1-half" id="accordionEx194" role="tablist"
        aria-multiselectable="true">

        <p >Here's a simple three-step process to get your wheels running:</p>

        <hr class="mb-0">

        <!-- Accordion card -->
        <div class="card">

          <!-- Card header -->
          <div class="card-header" role="tab" id="heading4">
            <a data-toggle="collapse" data-parent="#accordionEx194" href="#collapse4" 
              aria-controls="collapse4">
              <h5 class="mb-0 mt-3 red-text">
                <i class="fas fa-plus rotate-icon"></i> We ask you what's wrong 
              </h5>
            </a>
          </div>

          <!-- Card body -->
          <div id="collapse4" class="collapse " role="tabpanel" aria-labelledby="heading4"
            data-parent="#accordionEx194">
            <div class="card-body pt-0">
              <p>The first thing we do is to ask you what's going on with your vehicle. Our expert technicians expect the answers which a non-technical customer can tell, like "My car is not starting, or my bike is shaking." Don't worry; we make it up to you with our questionnaires. We ask you the make, year, model, and company name of your wheels. Once we get it, we ask you for the vehicle location and your contact details.</p>
            </div>
          </div>
        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class="card">

          <!-- Card header -->
          <div class="card-header" role="tab" id="heading5">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse5"
              aria-expanded="false" aria-controls="collapse5">
              <h5 class="mb-0 mt-3 red-text">
                <i class="fas fa-plus rotate-icon"></i> Get a fair estimate 
              </h5>
            </a>
          </div>

          <!-- Card body -->
          <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5"
            data-parent="#accordionEx194">
            <div class="card-body pt-0">
              <p>With the information you give and our expert's opinion, we provide a list of parts, services, and cost estimation to keep your wheels rolling. In most cases, we get you instant, fair, and transparent pricing for your vehicle. Our mechanics then accept our pricing and honor the quote. No need to negotiate the pricing. Have peace of mind with fixed prices with no last-minute surprises.</p>
            </div>
          </div>
        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class="card">

          <!-- Card header -->
          <div class="card-header" role="tab" id="heading6">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse6"
              aria-expanded="false" aria-controls="collapse6">
              <h5 class="mb-0 mt-3 red-text">
                <i class="fas fa-plus rotate-icon"></i> Schedule a quick inspection </h5>
            </a>
          </div>

          <!-- Card body -->
          <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6"
            data-parent="#accordionEx194">
            <div class="card-body pt-0">
              <p>With scheduling a quick inspection, you get a verified and reviewed mechanic coming to help you at your doorstep. You can also check the work history of your mechanic. Our expert mechanics are what we take pride in, and they believe in providing top-notch service to our customers every time you book a service with us. Vahanom's vehicle doctors are available for you every day, from 7 am to 9 pm.</p>
            </div>
          </div>
        </div>
        <!-- Accordion card -->
      </div>
      <!--/.Accordion wrapper-->
    </div>
    <div class="col-lg-1"></div>
    
  </div>
</div>



<!--how it work end -->
<!--
<section class="service-area sky-blue section-padding2" style="padding-bottom:50px;padding-top:30px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-top:0;margin-bottom:0;">Don't go anywhere. We <br>come to you</h2>
<p>
Avoid the hassle of towing your vehicle to the garage and paying hefty sums to tow trucks. Get a mechanic right where your wheels break down. Vahanom is available for you every day at the regular time to get you on the move in your own vehicle. Our vehicle doctors bring all the tools and parts you describe to help you out in the first place. All you need is patience and some space for our mechanics to work.
</p>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">The day before your appointment</a></h4>
<p>you will receive an email reminder from us on the day before your appointment. This reminder email is to help you remember your scheduled appointment day and timing. </p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">The appointment day</a></h4>
<p>You will get a text and email along with mechanic details to navigate him to your location. Avoid turning your vehicle on half an hour before the meet to keep the engine cool. Our mechanic will come to your place in 30 minutes.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">During your appointment
</a></h4>
<p>You can ask any questions about your vehicle to our mechanics. We also help our customers with replacing and disposing of worn-out parts. Once our vehicle doctors complete the service, we initiate our 50-point inspection that you receive in your email.</p>
</div>
</div>
</div>
</div>
</div>
</section> -->

<!-- -->
<!--<section class="service-area sky-blue section-padding2" style="padding-bottom:30px;padding-top:30px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-top:0;margin-bottom:0;">Save your receipts and service records online</h2><br>
<p>
Halt stuffing your file cabinets with paper receipts and your vehicle repair records. 
Why create a space for vehicle repair records when you can save them online? With Vahanom, you get the top-notch servicing of your vehicle on your doorstep at your convenient time while getting a free 50-point inspection of your drive stored in your app account. It includes all safety checks and essential safety items health-check like fluid levels, belts, brakes, engine codes, filters, tire treads, and more. Save the maintenance history of your vehicle so that when selling your wheels, you don't have to ram down its price and share your service records with the buyer.
</p>

</div>
</div>
</div>
</div>

</section>
-->
<section class="service-area sky-blue section-padding2" style="padding-bottom:30px;padding-top:30px;background-color: #fff">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-6">
<div class="section-tittle text-center">
<h2 style="margin-top:10%;">Repair payment made easy
</h2>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30" style="height: auto !important;box-shadow: 0px 0px 22px -9px;border-radius: 45px;">
<a data-toggle="collapse" href="#easy_online" aria-controls="easy_online"><div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Get_Payments_Easily.png"></img></span>
</div>
<div class="service-cap">
<h4>Easy<br> online payments</a></h4>
<p id="easy_online" class="collapse " role="tabpanel" aria-labelledby="heading4">With Vahanom, you get the option to pay online or financing options (where applicable and available). We do not bill your transaction until we complete our services for your vehicle. We currently are working for receiving cash and check payments, but it is not available at the moment. Please note that you get fully charged at the time of booking if you have a pre-paid card.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30" style="height: auto !important;box-shadow: 0px 0px 22px -9px;border-radius: 45px;">
<a data-toggle="collapse" href="#access_service" aria-controls="access_service"><div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/report.png"></img></span>
</div>
<div class="service-cap">
<h4>Access<br> service record online
</a></h4>
<p id="access_service" class="collapse " role="tabpanel" >No more stashing your paperwork and going through tonnes of paper every time you service your vehicle. With Vahanom, get a digital record stored in your free account on your app. Once we service or repair your car or bike, we send you an electronic receipt to your account. All your service receipts are kept safe in your account history, which you can share when selling your vehicle.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30" style="height: auto !important;box-shadow: 0px 0px 22px -9px;border-radius: 45px;">
<a data-toggle="collapse" href="#check_wheels" aria-controls="check_wheels"><div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/icons/tire.png"></img></span>
</div>
<div class="service-cap">
<h4>Check<br> what your wheels need</a></h4>
<p id="check_wheels" class="collapse " role="tabpanel">At the time of appointment, feel free to ask any questions about your wheels. If our expert mechanic believes that your car doesn't need unnecessary services, he can lower your bill by removing the redundant part change or servicing. That way, you pay only for the services your vehicle requires, and you will be in control of your expenses. You can always have the option to add or cancel the additional services and fixes.
</p>
</div>
</div>
</div>
</div>
</div>
</section>

<!--<section class="service-area sky-blue " style="padding-bottom:30px;padding-top:30px;">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2>We warranty our services</h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Guaranteed peace of minded
</a></h4>
<p>With Vahanom, you get the expert mechanic and transparent, right at your doorstep. Get your vehicle repaired on-site, professionally, and with fair pricing.
</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Extensive inspection 
</a></h4>
<p>We only hire the best to serve the best. At Vahanom, we hire only experts and qualified professionals to help our clients with their requirements.
</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Industry-standard insurance</a></h4>
<p>Our mechanics carry industry-standard insurance as liability insurance for your vehicle. You get 101% security when trusting your wheels with us.</p>
</div>
</div>
</div>
</div>
 
<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Service warranty</a></h4>
<p>We offer our customers with 3-month/ 1500 Kms (whichever comes first) service warranty. Subject to certain conditions, if you feel any defects, we will work with you to resolve them within time.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Extreme_Security.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">It's a win-win for everyone</a></h4>
<p>When you work directly with vehicle owners in your neighborhood, you can make more money with no commute. Plus, Vehicle owners enjoy the convenience. You get an online business. Everyone wins. </p>
</div>
</div>
</div>
</div>

<div class="row d-flex justify-content-center" >
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom: 0;">P.S. We service your vehicles with total transparency. Please read
</h2><br>
<p>If you pay the mechanic directly for any repair services or if the mechanic performs any job that is not authorized by Vahanom, our warranty does not apply. Only repair services ordered through Vahanom are covered by our warranty.</p><p>
If you ordered a repair service where we did not perform the diagnostic, our warranty only covers the defect in the replacement. (e.g. if you ordered a battery replacement because your Vehicle was not starting, if the Vehicle does not start after replacing the battery, this warranty does not cover the "Vehicle not starting event".)</p>
<p>If after a mechanic performs a service, you have reason to believe the services caused other problems, the warranty will be voided if you hire another mechanic (outside of Vahanom) to inspect or work on your Vehicle. If we send a mechanic to inspect the Vehicle and the inspection demonstrates that the problem was indeed caused by the previous service, then the mechanic will fix the Vehicle at no cost to you. If the problem was caused by other reasons, the mechanic will bill you for the inspection or any further repair services provided by them.
</p>
</div>
</div>
</div>

</div>

</section> -->


<!--<section class="service-area sky-blue section-padding2" style="padding-bottom:30px;padding-top:30px;">
<div class="container">

<div class="row d-flex justify-content-center">
<div class="col-lg-6">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0;">Get maintenance reminders</h2>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span class="flaticon-businessman"></span>
</div>
<div class="service-cap">
<h4><a href="#">Digital reminders, not window stickers REGULAR MAINTENANCE MADE EASY</a></h4>
<p>Stay on schedule, your way. Get maintenance reminders via email or text. We鈥檒l send you reminders when it鈥檚 time to do your next oil or your scheduled service.</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span class="flaticon-pay"></span>
</div>
<div class="service-cap">
<h4><a href="#">QUICK EASY BOOKING</a></h4>
<p>From reminders to booking, it takes less than 5 minutes. Simply open the link in the reminder, select a time and book your next maintenance. A highly rated mechanic will come to you to perform the service. Taking care of your Vehicle has never been easier. And as they say, prevention is better than care.
</p>
</div>
</div>
</div>
</div>
</div>
</section>-->

<!--<section class="service-area sky-blue " style="padding-bottom:30px;padding-top:30px;">
<div class="container">
<div class="row d-flex justify-content-center">
<div class="col-lg-12">
<div class="section-tittle text-center">
<h2 style="margin-bottom:0">Manage all your automobiles at once</h2><br>
</div>
</div>
</div>

<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Easy_to_Customize.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Manage all your Vehicles online</a></h4>
<p>With your free account on the Vahanom app, you can register all your vehicles for service. Get fair and transparent pricing, schedule appointments, and see your wheel's entire service history. 
</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption active text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Easy_to_Customize.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Get manufacturer-advised maintenance schedule
</a></h4>
<p>We show you the manufacturer-recommended servicing schedule for your wheels. You don't have to open your automobile's manual to know the time-table of servicing.
</p>
</div>
</div>
</div>
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Easy_to_Customize.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Own more than one vehicle? </a></h4>
<p>Add all your vehicles and manage them all at once through your free account at Vahanom. We will keep a service record of each of your wheels, be it two-wheelers or four-wheelers.
</p>
</div>
</div>
</div>
</div>
 
<div class="row">
<div class="col-xl-4 col-lg-4 col-md-6">
<div class="services-caption text-center mb-30">
<div class="service-icon">
<span><img class="manImg" width="40" height="40" src="<?php echo base_url(); ?>assets/img/Website_icons/Easy_to_Customize.png"></span>
</div>
<div class="service-cap">
<h4><a href="#">Get 24/7 access to your service records</a></h4>
<p>Don't juggle with messy paperwork. Collect all your vehicle data in one place. Once we service your wheels, we send you a digital copy of the receipt. You can access it anytime from anywhere by logging in to your account.</p>
</div>
</div>
</div>

</div>
</div>
</section>-->



<div class="container my-5 p-5 z-depth-1">


  <!--Section: Content-->
  <section class="dark-grey-text">

    <!-- Section heading -->
    <h2 class="text-center font-weight-bold mb-4 pb-2">We warranty what we offer</h2>
    <!-- Section description -->

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-4">

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-flag-checkered deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Guaranteed peace of mind</h5>
            <p class="grey-text">With Vahanom, you get the expert mechanic and transparent, right at your doorstep. Get your vehicle repaired on-site, professionally, and with fair pricing. </p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-flask deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Extensive inspection</h5>
            <p class="grey-text">We only hire the best to serve the best. At Vahanom, we hire only experts and qualified professionals to help our clients with their requirements.
                  </p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-md-0 mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-glass-martini deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Industry-standard insurance</h5>
            <p class="grey-text mb-md-0">Our mechanics carry industry-standard insurance as liability insurance for your vehicle. You get 101% security when trusting your wheels with us.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 text-center">
        <img class="img-fluid" src="http://localhost/vahanom/assets/img/gallery/app1.png"
          alt="Sample image">
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4">

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="far fa-2x fa-heart deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">Service warranty</h5>
            <p class="grey-text">We offer our customers with 3-month/ 1500 Kms (whichever comes first) service warranty. Subject to certain conditions, if you feel any defects, we will work with you to resolve them within time.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-bolt deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3">It's A Win-Win For Everyone</h5>
            <p class="grey-text">When you work directly with vehicle owners in your neighborhood, you can make more money with no commute. Plus, Vehicle owners enjoy the convenience. You get an online business. Everyone wins.</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->


      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </section>
  <!--Section: Content-->


</div>

<div class="our-customer sky-blue section-padd-top30" style="padding-bottom:5%;padding-top:5%;">
<div class="container-fluid">
<div class="our-customer-wrapper">

<div class="row d-flex justify-content-center">
<div class="col-xl-8">
<div class="section-tittle text-center">
<h2>What <br>happy customers says </h2>
</div>
</div>
</div>
<div class="row">
<div class="col-12">
 <div class="customar-active dot-style d-flex dot-style">
<div class="single-customer mb-100">
<div class="what-img">
<img src="<?php echo base_url(); ?>assets/img/shape/man1.png" alt="">
</div>
<div class="what-cap">
<h4><a href="#">They help you on time, every time</a></h4>
<p>Vahanom gave me exceptional experience servicing my vehicle. I used to get a leave from the office whenever I had an upcoming servicing for my commute. Vahanom has filled the gap of providing a convenient way to keep my bike up and running. 
Vinod Makhija

.</p>
</div>
</div>
<div class="single-customer mb-100">
<div class="what-img">
<img src="<?php echo base_url(); ?>assets/img/shape/man2.png" alt="">
</div>
<div class="what-cap">
<h4><a href="#">Vahanom is bringing revolution to India</a></h4>
<p>I believe they are revolutionizing the way we service our wheels. The professionals of Vahanom inspected my vehicle and even reduced redundant services to help me cut the cost of service. They are professional, punctual, and genuinely try to help. 
Vishal Kumar

.</p>
</div>
</div>
<div class="single-customer mb-100">
<div class="what-img">
<img src="<?php echo base_url(); ?>assets/img/shape/man3.png" alt="">
</div>
<div class="what-cap">
<h4><a href="#">I would definitely recommend Vahanom to everyone</a></h4>
<p>I had an issue with my car and that too at the office it won't start. Instead of towing my vehicle, I search for a convenient way to get it resolved, and that's when I came to know about Vahanom. The mechanic came on time, repaired it within minutes, and saved much of my time and money.
Vyankatesh M.

</p>
</div>
</div>
<div class="single-customer mb-100">
<div class="what-img">
<img src="<?php echo base_url(); ?>assets/img/shape/man2.png" alt="">
</div>
<div class="what-cap">
<h4><a href="#">They got the job done with no extra charges</a></h4>
<p>I had a great experience with Vahanom. I had battery issues in my hometown, and they helped me get to the office on time. The mechanics of Vahanom are very friendly, knowledgeable, and efficient. They got the job done with no extra charges and surprises. 
Nawaz Sayyed

</p>
</div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>


<div class="available-app-area" style="padding-top:30px;">
<div class="container">
<div class="row d-flex justify-content-between">
<div class="col-xl-5 col-lg-6">
<div class="app-caption">
<div class="section-tittle section-tittle3">
<h2>Our app is available at your service</h2>
<p>Download from any device and take good care of your vehicle now. </p>
<div class="app-btn">
<a href="#" class="app-btn1"><img src="<?php echo base_url(); ?>assets/img/shape/app_btn1.png" alt=""></a>
<a href="#" class="app-btn2"><img src="<?php echo base_url(); ?>assets/img/shape/app_btn2.png" alt=""></a>
</div>
</div>
</div>
</div>
 <div class="col-xl-6 col-lg-6">
<div class="app-img">
<img src="<?php echo base_url(); ?>assets/img/shape/available-app1.png" alt="">
</div>
</div>
</div>
</div>

<div class="app-shape">
<img src="<?php echo base_url(); ?>assets/img/shape/app-shape-top.png" alt="" class="app-shape-top heartbeat d-none d-lg-block">
<img src="<?php echo base_url(); ?>assets/img/shape/app-shape-left.png" alt="" class="app-shape-left d-none d-xl-block">

</div>
</div>


<!-- <div class="say-something-aera pt-90 pb-90 fix">
<div class="container">
<div class="row justify-content-between align-items-center">
<div class="offset-xl-1 offset-lg-1 col-xl-5 col-lg-5">
<div class="say-something-cap">
<h2>Say Hello To The Collaboration Hub.</h2>
</div>
</div>
<div class="col-xl-2 col-lg-3">
<div class="say-btn">
<a href="#" class="btn radius-btn">Contact Us</a>
</div>
</div>
</div>
</div>

<div class="say-shape">
<img src="assets/img/shape/say-shape-left.png" alt="" class="say-shape1 rotateme d-none d-xl-block">
<img src="assets/img/shape/say-shape-right.png" alt="" class="say-shape2 d-none d-lg-block">
</div>
</div> -->

</main>
<?php include('footer.php');?>



<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
</body>

</html>