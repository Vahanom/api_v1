<!doctype html>
<html class="no-js" lang="zxx">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Privacy Policy</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="manifest" href="site.html">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.ico">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slicknav.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/privacy-policy.css">
</head>
<body>

<!--<div id="preloader-active">
<div class="preloader d-flex align-items-center justify-content-center">
<div class="preloader-inner position-relative">
<div class="preloader-circle"></div>
<div class="preloader-img pere-text">
<img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt="">
</div>
</div>
</div>
</div>

<style type="text/css">
	.header-area{
		padding: 39px 16px;
	}

	.sticky-bar{
		padding: 20px 16px;
	}
</style>

<header>

<div class="header-area header-transparrent ">
	<div class="main-header header-sticky">
		<div class="container">
			<div class="row align-items-center" >

				<div class="col-xl-2 col-lg-2 col-md-2">
					<div class="logo">
						<a href="index-2.html"><img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt=""></a>
					</div>
				</div>
				<div class="col-12">
					<div class="mobile_menu d-block d-lg-none"></div>
				</div>
			</div>
		</div>
	</div>
</div>

</header>-->
<main> 



<div class="applic-apps section-padding2">
	<div class="container" >

		<div class="row justify-content-center">
			<div class="col-12">
				<h1>Privacy Policy</h1>
			</div>

			<div class="col-12 privacy-policy-content" >
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

			</div>
		</div>
	</div>
</div>




</main>
<!-- <footer>

	<div class="footer-main">
	<div class="footer-area footer-padding">
	<div class="container">
	<div class="row  justify-content-between">
	<div class="col-lg-3 col-md-4 col-sm-8">
	<div class="single-footer-caption mb-10">

	<div class="footer-logo">
	<a href="index-2.html"><img src="assets/img/logo/logo2.png" alt=""></a>
	</div>
	</div>
	</div>
	<div class="col-lg-2 col-md-4 col-sm-5">
	
	</div>
	<div class="col-lg-2 col-md-4 col-sm-7">
	
	</div>
	<div class="col-lg-3 col-md-6 col-sm-8">
	
	</div>
	</div>

	<div class="row align-items-center">
	<div class="col-xl-12 ">
	<div class="footer-copy-right">
	<p>
	Designed & Developed by Qset | <a href="https://qset.in/" target="_blank">www.qset.in</a> 
	</p>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>

</footer>
 -->

<script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-3.5.0.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/gijgo.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/animated.headline.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.scrollUp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nice-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sticky.js"></script>

<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
  

	$('#submit').click(function(){
	 	alert("Thank you for your registration, our team will get back to you shortly.")
	});
	$(document).ready(function(){
 $("#Feature").hide();
 $("#Contact").hide();
});

</script>
</body>
</html>