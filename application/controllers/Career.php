<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

      function __construct()
    {     
            parent::__construct();              
            $this->load->library('session');
            $this->load->model('CareerModal');
            $this->load->helper('form'); 
      }

    public function index()
    {
    $this->load->view('layout/header');
        $this->load->view('layout/navbar');
         $data['openings'] = $this->CareerModal->view(); 
        $this->load->view('career/career',$data);
        //$this->load->view('layout/footer');
        
    }

    public function add(){
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone_number = $this->input->post('phone_number');
        $message = $this->input->post('message');
        $openings = $this->input->post('openings');
        // print_r($_FILES['resume']['name']);exit;
        if(!empty($_FILES['resume']['name']))
        {
            $config['upload_path'] = './assets/resume/career';
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['file_name'] = $_FILES['resume']['name'];
        
            //Load upload library and initialize configuration
            // print_r($config);exit;
            $this->load->library('upload',$config);

            // $this->upload->initialize($config);
            if($this->upload->do_upload('resume'))
            {       
                $uploadData = $this->upload->data();
                // print_r($uploadData);exit;

                $resume = $uploadData['file_name'];
            }else
            {
                // print_r('test');exit;

                $resume = '';
            }
        }else
        {
            $resume = '';
        }

 $result = $this->CareerModal->add($name,$email,$phone_number,$message,$openings,$resume);
        if ($result == 'true') {
          echo "<script>alert('Application Submit Successfully!!!!!');
            window.location.href = '/career';
            </script>";
           // redirect("Dashboard");
          }else{
            redirect("/");
        }
    }
    
}

