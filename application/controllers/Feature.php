<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feature extends CI_Controller {

	  function __construct()
    {     
		    parent::__construct();				
            $this->load->library('session');
            $this->load->model('WebTechnicianModel');
            $this->load->helper('form'); 
	  }

	public function index()
	{
    $this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('feature/web_feature');
		//$this->load->view('layout/footer');
		
	}

	
}

