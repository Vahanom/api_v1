<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	  function __construct()
    {     
		    parent::__construct();				
            $this->load->library('session');
            $this->load->model('SupplierModel');
            $this->load->helper('form'); 
	  }

	public function index()
	{
    $this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('supplier/supplier_registration');
	//$this->load->view('layout/footer');
		
	}
	public function add()
	{
        $supplier_name = $this->input->post('supplier_name');
        $supplier_email = $this->input->post('supplier_email');
        $supplier_aadhar = $this->input->post('supplier_aadhar');
        $supplier_pan = $this->input->post('supplier_pan');
        $supplier_address = $this->input->post('supplier_address');
        $supplier_pincode = $this->input->post('supplier_pincode');
        $supllier_phonenumber = $this->input->post('supllier_phonenumber');
        $status = $this->input->post('status');
        $latlong = $this->input->post('latlong');
        $long = $this->input->post('long');
        $supllier_bankacc = $this->input->post('supllier_bankacc');
        $ifscnumber = $this->input->post('ifscnumber');
        $companyregnumber = $this->input->post('companyregnumber');
        $gstnumber = $this->input->post('gstnumber');
        $mondaystarttime = $this->input->post('mondaystarttime');
        $mondayendtime = $this->input->post('mondayendtime');
        $Tuesdaystarttime = $this->input->post('Tuesdaystarttime');
        $Tuesdayendtime = $this->input->post('Tuesdayendtime');
        $Wednesdaystarttime = $this->input->post('Wednesdaystarttime');
        $Wednesdayendtime = $this->input->post('Wednesdayendtime');
        $Thursdaystarttime = $this->input->post('Thursdaystarttime');
        $Thursdayendtime = $this->input->post('Thursdayendtime');
        $Fridaystarttime = $this->input->post('Fridaystarttime');

        $Fridayendtime = $this->input->post('Fridayendtime');
        $Saturdaystarttime = $this->input->post('Saturdaystarttime');
        $Saturdayendtime = $this->input->post('Saturdayendtime');
        $Sundaystarttime = $this->input->post('Sundaystarttime');
        $Sundayendtime = $this->input->post('Sundayendtime');

        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/supplier';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }
      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/supplier';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = $_FILES['imagetwo']['name'];
        // var_dump($imagetwo);die;
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
    }else{
        $imagetwo = '';
    }
    // var_dump($imagetwo);die;
 $result = $this->SupplierModel->add($supplier_name,$supplier_email,$supplier_aadhar,$supplier_pan,$supplier_address,$supplier_pincode,$supllier_phonenumber,$status,$latlong,$supllier_bankacc,$ifscnumber,$companyregnumber,$gstnumber,$imageone,$mondaystarttime,$mondayendtime,$Tuesdaystarttime,$Tuesdayendtime,$Wednesdaystarttime,$Wednesdayendtime,$Thursdaystarttime,$Thursdayendtime,$Fridaystarttime,$Fridayendtime,$Saturdaystarttime,$Saturdayendtime,$Sundaystarttime,$Sundayendtime,$long,$imagetwo);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Supplier Added Successfully');
            
            </script>";
             redirect("Dashboard");
          }else{
            redirect("admin/Supplier");
        }
      
		
	}
}
