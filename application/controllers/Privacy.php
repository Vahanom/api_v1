<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy extends CI_Controller {

	  function __construct()
    {     
		    parent::__construct();				
            $this->load->library('session');
            //$this->load->model('SupplierModel');
            $this->load->helper('form'); 
	  }

	public function index()
	{

    $this->load->view('layout/header');
    $this->load->view('layout/navbar');
    $this->load->view('privacy/privacy');
    //$this->load->view('layout/footer');
		
	}
	
}
