<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Getaquote extends CI_Controller {

	  function __construct()
    {     
		    parent::__construct();				
            $this->load->library('session');
            $this->load->model('GetaquoteModel');
            $this->load->helper('form'); 
	  }

	public function index()
	{
            $this->load->view('layout/header');
        $this->load->view('layout/navbar');
        $this->load->view('layout/footer');
	
	}

	public function add(){
        // print_r($this->input->post());exit;
		$name = $this->input->post('full_name');
        $email = $this->input->post('email');
        $phone_number = $this->input->post('contact');
        $type_vehicle = $this->input->post('type_vehicle');
        $veh_name = $this->input->post('veh_name');
        $veh_model = $this->input->post('veh_model');
        $message = $this->input->post('message');
 $result = $this->GetaquoteModel->add($name,$email,$phone_number,$type_vehicle,$veh_name,$veh_model,$message);
        if ($result == 'true') {
          echo "<script>alert('Query Submit Successfully !!! We connect You soon.');
            window.location.href = '/';
            </script>";
           // redirect("Dashboard");
          }else{
            redirect("https://vahanom.com");
        }
	}
	
}

