<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutUs extends CI_Controller {

	  function __construct()
    {     
		    parent::__construct();				
            $this->load->library('session');
            $this->load->helper('form'); 
	  }

	public function index()
	{
    $this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('about/about');
		//$this->load->view('layout/footer');
		
	}

	
}

