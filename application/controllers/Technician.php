<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technician extends CI_Controller {

	  function __construct()
    {     
		    parent::__construct();				
            $this->load->library('session');
            $this->load->model('TechnicianModel');
            $this->load->helper('form'); 
	  }

	public function index()
	{
     $this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('technician/technician_registration');
		//$this->load->view('layout/footer');
		
	}
	public function add()
	{
        $technician_name = $this->input->post('technician_name');
        $technician_email = $this->input->post('technician_email');
        $technician_aadhar = $this->input->post('technician_aadhar');
        $technician_pan = $this->input->post('technician_pan');
        $technician_address = $this->input->post('technician_address');
        $technician_pincode = $this->input->post('technician_pincode');
        $technician_phonenumber = $this->input->post('technician_phonenumber');
        $technician_alternatephonenumber = $this->input->post('technician_alternatephonenumber');
        $latlong = $this->input->post('latlong');
        $technician_bankacc = $this->input->post('technician_bankacc');
        $ifscnumber = $this->input->post('ifscnumber');
        $gstnumber = $this->input->post('gstnumber');
        $status = $this->input->post('status');
        $working_type = $this->input->post('working_type');
        $technician_order_distance = $this->input->post('technician_order_distance');
        $weekly_available_hours = $this->input->post('weekly_available_hours');
        if(isset($_POST['technician_certificate']))
         {
             $technician_certificate = array();
               foreach($_POST['technician_certificate'] as $val)
                {
                   $technician_certificate[] = $val;
                }
             $technician_certificate = implode(',', $technician_certificate); 
         }
         $data =$technician_certificate; 
        if(!empty($_FILES['imageone']['name'])){
          $config['upload_path'] = './assets/images/technician';
          $config['allowed_types'] = 'jpg|jpeg|png|gif';
          $config['file_name'] = $_FILES['imageone']['name'];
          
          //Load upload library and initialize configuration
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if($this->upload->do_upload('imageone')){
              $uploadData = $this->upload->data();
              $imageone = $uploadData['file_name'];
          }else{
              $imageone = '';
          }
      }else{
          $imageone = '';
      }
      if(!empty($_FILES['imagetwo']['name'])){
        $config['upload_path'] = './assets/images/technician';
        $config['allowed_types'] = 'pdf|doc|docx';
         $config['file_name'] = $_FILES['imagetwo']['name'];
        
        //Load upload library and initialize configuration
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if($this->upload->do_upload('imagetwo')){
            $uploadData = $this->upload->data();
            $imagetwo = $uploadData['file_name'];
        }else{
            $imagetwo = '';
        }
    }else{
        $imagetwo = '';
    }
    // var_dump($imagetwo);die;
    $long = $this->input->post('long');
 $result = $this->TechnicianModel->add($technician_name,$technician_email,$technician_aadhar,$technician_pan,$technician_address,$technician_pincode,$technician_phonenumber,$latlong,$technician_bankacc,$ifscnumber,$gstnumber,$status,$imageone,$imagetwo,$technician_alternatephonenumber,$long,$working_type,$technician_order_distance,$data,$weekly_available_hours);
//  die;
        if ($result == 'true') {
          echo "<script>alert('Technician Added Successfully');
           
            </script>";
            redirect("Dashboard");
          }else{
            redirect("admin/Technician");
        }
      



		
	}
}

