<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebTechnician extends CI_Controller {

	  function __construct()
    {     
		    parent::__construct();				
            $this->load->library('session');
            $this->load->model('WebTechnicianModel');
            $this->load->helper('form'); 
	  }

	public function index()
	{
    $this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('webtechnician/webtechnician_registration');
		//$this->load->view('layout/footer');
		
	}

	public function add(){
		$name = $this->input->post('name');
        $email = $this->input->post('email');
        $phone_number = $this->input->post('phone_number');
        $address = $this->input->post('address');
        $pincode = $this->input->post('pincode');
        $reference = $this->input->post('reference');
 $result = $this->WebTechnicianModel->add($name,$email,$phone_number,$address,$pincode,$reference);
        if ($result == 'true') {
          echo "<script>alert('Technician Added Successfully');
            window.location.href = 'index';
            </script>";
           // redirect("Dashboard");
          }else{
            redirect("admin/WebTechnician");
        }
	}
	
}

