<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class TechnicianModel extends CI_Model {


function add($technician_name,$technician_email,$technician_aadhar,$technician_pan,$technician_address,$technician_pincode,$technician_phonenumber,$latlong,$technician_bankacc,$ifscnumber,$gstnumber,$status,$imageone,$imagetwo,$technician_alternatephonenumber,$long,$working_type,$technician_order_distance,$data,$weekly_available_hours){
	$sql = "INSERT INTO `tbl_p_users`(`puser_name`, `puser_address`,`puser_pincode`, `puser_lat_long`, `puser_phno`, `puser_email`, `puser_adhar_number`, `puser_pan_number`, `puser_GST_number`, `puser_bankacc_number`, `puser_bankacc_ifsc`, `puser_profile_image`, `puser_account_status`, `puser_document_url`, `puser_created_date`, `puser_type`,`technician_alternatephonenumber`,`puser_long`, `working_type`, `technician_order_distance`, `technician_certificate`, `weekly_available_hours`) VALUES (".$this->db->escape($technician_name).",".$this->db->escape($technician_address).",".$this->db->escape($technician_pincode).",".$this->db->escape($latlong).",".$this->db->escape($technician_phonenumber).",".$this->db->escape($technician_email).",".$this->db->escape($technician_aadhar).",".$this->db->escape($technician_pan).",".$this->db->escape($gstnumber).",".$this->db->escape($technician_bankacc).",".$this->db->escape($ifscnumber).",".$this->db->escape($imageone).",".$this->db->escape($status).",".$this->db->escape($imagetwo).",CURRENT_TIMESTAMP,".$this->db->escape('Technician').",".$this->db->escape($technician_alternatephonenumber).",".$this->db->escape($long).",".$this->db->escape($working_type).",".$this->db->escape($technician_order_distance).",".$this->db->escape($data).",".$this->db->escape($weekly_available_hours).")";
	//echo $sql;die;
	$query=$this->db->query($sql);
	if($query==1){
		return true;
	}
	else
	{
		return false;
	}
}




}