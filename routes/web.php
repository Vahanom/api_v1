<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Headers:access-control-allow-origin, content-type"); 
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    
//===================================Partner Application API===============================================

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/categoryquestion', 'partner\APIController@categoryquestion');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/getsupplierspares', 'partner\APIController@getsupplierspares');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/subcategoryquestion', 'partner\APIController@subcategoryquestion');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/questionlist', 'partner\APIController@questionlist');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/submitquestion', 'partner\APIController@submitquestion');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/odometerreading', 'partner\APIController@odometerreading');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getpartnernotification', 'partner\APIController@getpartnernotification');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
$router->post('/completeorder', 'partner\APIController@completeorder');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
$router->post('/add_partner_review', 'partner\APIController@add_partner_review');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/getpartner_review', 'partner\APIController@getpartner_review');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/uploadedimages', 'partner\APIController@uploadedimages');
});

$router->post('/partnerregisteration', 'partner\APIController@partnerregisteration');
    $router->get('/appversion', 'partner\APIController@appversion');
     
$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/orderdetails', 'partner\APIController@orderdetails');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/categoryspare', 'partner\APIController@categoryspare');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/categorylistspare', 'partner\APIController@categorylistspare');
});
$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/getspare', 'partner\APIController@getspare');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/submitspare', 'partner\APIController@submitspare');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/listviewspares', 'partner\APIController@listviewspares');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/detailsviewspares', 'partner\APIController@detailsviewspares');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/removespare', 'partner\APIController@removespare');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/updatespare', 'partner\APIController@updatespare');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/supplierorder', 'partner\APIController@supplierorder');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/earnings', 'partner\APIController@earnings');
});
//==================================without route===============
$router->post('/locationupdate', 'partner\APIController@locationupdate');

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/technicianorder', 'partner\APIController@technicianorder');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/partnerdutystatus', 'partner\APIController@partnerdutystatus');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/partnerdutystatusupdate', 'partner\APIController@partnerdutystatusupdate');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/getpartnerprofile', 'partner\APIController@getpartnerprofile');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/alltools', 'partner\APIController@alltools');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/removetool', 'partner\APIController@removetool');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/detailsviewtools', 'partner\APIController@detailsviewtools');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/listviewtools', 'partner\APIController@listviewtools');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/submittools', 'partner\APIController@submittools');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/suppliertechorderlist', 'partner\APIController@suppliertechorderlist');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/allservice', 'partner\APIController@allservice');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/submitservice', 'partner\APIController@submitservice');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/removeservice', 'partner\APIController@removeservice');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/techniservice', 'partner\APIController@techniservice');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/alltechnicianservice', 'partner\APIController@alltechnicianservice');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/technicianorderaccept', 'partner\APIController@technicianorderaccept');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/partnerorderotpverify', 'partner\APIController@partnerorderotpverify');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/technicianorderuploadimg', 'partner\APIController@technicianorderuploadimg');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/showordercount', 'partner\APIController@showordercount');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/partnerprofileupdate', 'partner\APIController@partnerprofileupdate');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/savestandardsparecart', 'partner\APIController@savestandardsparecart');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getPartnerReview', 'partner\APIController@getPartnerReview');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getSupplierDetail', 'partner\APIController@getSupplierDetail');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getTerms', 'partner\APIController@getTerms');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getFaq', 'partner\APIController@getFaq');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getLandingPage', 'partner\APIController@getLandingPage');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/bankDetails', 'partner\APIController@bankDetails');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->Post('/bankDetailsUpdate', 'partner\APIController@bankDetailsUpdate');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getpartneraddress', 'partner\APIController@getpartneraddress');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/partnerupdateaddress', 'partner\APIController@partnerupdateaddress');
});
//=================================== Partner Application APIS ENDS HERE ===========================================






//=================================== User Application API STARTS HERE===============================================





// ======================LOGIN APIS STARTS HERE=====================================

$router->post('/userlogin', 'User\LoginController@userlogin');

$router->post('/userotpverify', 'User\LoginController@userotpverify');

$router->post('/userresendotp', 'User\LoginController@userresendotp');

$router->post('/userregistration', 'User\LoginController@userregistration');
// ======================LOGIN APIS ENDS HERE=====================================


// ======================User APIS Starts HERE=====================================


$router->get('/userAppversion', 'User\APIController@userAppversion');

$router->post('/vehiclereg', 'User\APIController@vehiclereg');
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/profileimageupdate', 'User\APIController@profileimageupdate');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/aboutus', 'User\APIController@aboutus');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/uservehicle', 'User\APIController@uservehicle');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/alluserservice', 'User\APIController@alluserservice');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/quickservicecart', 'User\APIController@quickservicecart');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getusernotification', 'User\APIController@getusernotification');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/banner', 'User\APIController@banner');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getmake', 'User\APIController@getmake');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getmodel', 'User\APIController@getmodel');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/gettrim', 'User\APIController@gettrim');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/popup', 'User\APIController@popup');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/terms', 'User\APIController@terms');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/support', 'User\APIController@support');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/support', 'User\APIController@support');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/insuarancelist', 'User\APIController@insuarancelist');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/insuaranceget', 'User\APIController@insuaranceget');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/insuaranceinquiry', 'User\APIController@insuaranceinquiry');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/userget', 'User\APIController@userget');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/userupdate', 'User\APIController@userupdate');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/pollutionlist', 'User\APIController@pollutionlist');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/pollutioneget', 'User\APIController@pollutioneget');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/pollutionsinquiry', 'User\APIController@pollutionsinquiry');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/orderlist', 'User\APIController@orderlist');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/orderinfo', 'User\APIController@orderinfo');
});

// $router->group(['middleware' => 'authuser'], function () use ($router) {
//     $router->post('/profileimageupdate', 'User\APIController@profileimageupdate');
// });
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/alltechnician', 'User\APIController@alltechnician');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/allsupplier', 'User\APIController@allsupplier');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/applycoupon', 'User\APIController@applycoupon');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/placeorder', 'User\APIController@placeorder');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/voiceupload', 'User\APIController@voiceupload');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/supplierspares', 'User\APIController@supplierspares');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/showtechnicianscreen', 'User\APIController@showtechnicianscreen');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/standardsparecart', 'User\APIController@standardsparecart');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getstandardcart', 'User\APIController@getstandardcart');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/categoryuserspare', 'User\APIController@categoryuserspare');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getquickcart', 'User\APIController@getquickcart');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/deletestandardcart', 'User\APIController@deletestandardcart');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/deletequickcart', 'User\APIController@deletequickcart');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/deletestandardspare', 'User\APIController@deletestandardspare');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/deletequickservice', 'User\APIController@deletequickservice');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/quickinspection', 'User\APIController@quickinspection');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/prepurchaseinspection', 'User\APIController@prepurchaseinspection');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/deliverydates', 'User\APIController@deliverydates');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/deletevehicle', 'User\APIController@deletevehicle');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getinspectiontype', 'User\APIController@getinspectiontype');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/serviceget', 'User\APIController@serviceget');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getaddress', 'User\APIController@getaddress');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/saveaddress', 'User\APIController@saveaddress');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/updateaddress', 'User\APIController@updateaddress');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/deleteaddress', 'User\APIController@deleteaddress');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getpartaddress', 'User\APIController@getpartaddress');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/saveenquiry', 'User\APIController@saveenquiry');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/voiceget', 'User\APIController@voiceget');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getservicecart', 'User\APIController@getservicecart');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/addcontactdetail', 'User\APIController@addcontactdetail');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getcontactdetail', 'User\APIController@getcontactdetail');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getsparecart', 'User\APIController@getsparecart');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/deletesparecart', 'User\APIController@deletesparecart');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getquote', 'User\APIController@getquote');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getquestionorder', 'User\APIController@getquestionorder');
});
$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/add_cust_review', 'User\APIController@add_cust_review');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getreview', 'User\APIController@getreview');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/submitquote', 'User\APIController@submitquote');
});

$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->post('/getCustReview', 'User\APIController@getCustReview');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/questionlist_percentage', 'User\APIController@questionlist_percentage');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/alltechnicianvehicle', 'User\APIController@alltechnicianvehicle');
});


$router->group(['middleware' => 'authuser'], function () use ($router) {
    $router->get('/getshowtechnician', 'User\APIController@getshowtechnician');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/getinsuaranceinquiry', 'User\APIController@getinsuaranceinquiry');
});
$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/getpollutionsinquiry', 'User\APIController@getpollutionsinquiry');
});
$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/getBanners', 'User\APIController@getBanners');
});
$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/technicianRatingCalculation', 'User\APIController@technicianRatingCalculation');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/supplierRatingCalculation', 'User\APIController@supplierRatingCalculation');
});

$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->post('/technicianDetails', 'User\APIController@technicianDetails');
});
$router->group(['middleware' => 'authpuser'], function () use ($router) {
    $router->get('/getcontact', 'User\APIController@getcontact');
});
// ======================User APIS ENDS HERE=====================================




